.. HAT documentation master file, created by
   sphinx-quickstart on Tue Apr 20 19:34:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dali USB
========
Class to interface with Dali USB devices.

.. automodule:: hat.comms.DaliUsb
   :members:

