.. HAT documentation master file, created by
   sphinx-quickstart on Tue Apr 20 19:34:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

P7CM FCT
========
Class to interface with P7CM in FCT mode devices.

.. automodule:: hat.comms.P7CM_FCT
   :members:

