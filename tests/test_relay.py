import unittest
from hat.drivers import RelayInterface

relay_address = "COM141"
relay_config = r"..\scripts\robot_lib\relay_config.json"


class MyTestCase(unittest.TestCase):
    def test_relay_interface_open(self):
        relay = RelayInterface.RelayInterface()
        relay.open(relay_address)

    def test_relay_load_config(self):
        relay = RelayInterface.RelayInterface()
        relay.read_config(relay_config)

    def test_relay_load_config(self):
        relay = RelayInterface.RelayInterface(relay_address, relay_config)
        relay.open()

        relay.set_position_by_label("uart")
        # relay.set_relay(0, False)

if __name__ == '__main__':
    unittest.main()
