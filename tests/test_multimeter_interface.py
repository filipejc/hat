import unittest
from hat.drivers import MultimeterInstrument

# Multimeter address
mult_addr = "USB0::0x05E6::0x6510::04472337::INSTR"
confif_file = "mult_config.json"

class MyTestCase(unittest.TestCase):
    def test_multimeter_idn(self):
        mult = MultimeterInstrument.MultimeterInstrument(mult_addr, confif_file)
        mult.open()
        mult.get_IDN()
        mult.get_voltage_ac_by_name("LineOutToNeutral")

    def test_multimeter_dic(self):
        mult = MultimeterInstrument.MultimeterInstrument(mult_addr, confif_file)
        mult.get_voltage_ac_by_name("LineOutToNeutral")



if __name__ == '__main__':
    unittest.main()
