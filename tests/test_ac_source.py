import unittest
from hat.drivers.AcSourceInstrument import AcSourceInstrument

ac_source_address = "ASRL51::INSTR"


class MyTestCase(unittest.TestCase):
    def test_ac_sorce_connection(self):
        ac_source = AcSourceInstrument(ac_source_address)

        ac_source.open()

    def test_ac_source_phase(self):
        ac_source = AcSourceInstrument(ac_source_address)
        ac_source.open()

        ac_source.set_phase_on(0)
        angle = ac_source.get_phase_on()

        self.assertEqual(0, angle, "Phase not equal")

        ac_source.set_phase_on(90)

        angle = ac_source.get_phase_on()

        self.assertEqual(90, angle, "Phase not equal")

        ac_source.set_phase_off(0)
        angle = ac_source.get_phase_off()

        self.assertEqual(0, angle, "Phase not equal")

        ac_source.set_phase_off(90)

        angle = ac_source.get_phase_off()

        self.assertEqual(90, angle, "Phase not equal")


if __name__ == '__main__':
    unittest.main()
