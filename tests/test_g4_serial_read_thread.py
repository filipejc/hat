from hat.comms import g4_interface_cli
import time
import unittest

# com port
COM_port = "COM29"

class MyTestCase(unittest.TestCase):
    def test_serial_thread(self):
        serial_thread = g4_interface_cli.SerialInterfaceThread(COM_port)
        print("My test")
        time.sleep(10)


if __name__ == '__main__':
    unittest.main()
