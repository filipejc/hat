import unittest
import time
from hat.drivers.RelayInterface import RelayInterface

relay_address = "COM131"
config_file = "../scripts/robot_lib/relay_config.json"


class MyTestCase(unittest.TestCase):
    def test_relay_interface(self):
        relay = RelayInterface(relay_address, config_file)
        relay.open()
        relay.set_position_by_label("uart")
        relay.close()

    def test_relay_interface_no_open_to_uart(self):
        relay = RelayInterface(relay_address, config_file)
        relay.set_position_by_label("uart")
        relay.close()

    def test_relay_interface_1_10v(self):
        relay = RelayInterface(relay_address, config_file)
        relay.set_position_by_label("1-10v")
        relay.set_position_by_label("1-10v_led")
        relay.set_position_by_label("1-10v_power")

    def test_relay_interface_dali(self):
        relay = RelayInterface(relay_address, config_file)
        relay.set_position_by_label("dali")
        relay.set_position_by_label("dali_led")
        relay.set_position_by_label("dali_power")

    def test_relay_sequence(self):
        relay = RelayInterface(relay_address, config_file)

        print("Starting")

        for i in range(8):
            relay.set_relay(i, False)

        print("Turning on")
        for i in range(8):
            relay.set_relay(i, True)
            time.sleep(1)

        print("Turning off")
        for i in range(7, -1, -1):
            relay.set_relay(i, False)
            time.sleep(1)


if __name__ == '__main__':
    unittest.main()
