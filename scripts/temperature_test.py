"""
# Filename:     temperature_test.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   24/02/2023
# Last Update:  24/02/2023
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
from hat.drivers import MultimeterInstrument
from hat.comms import g4_interface_cli
from hat.utils import DataStorage

import os
import time

if __name__ == '__main__':
    # Multimeter
    multimeter_address = "USB0::0x05E6::0x6510::04472337::INSTR"
    g4_address = "COM234"
    time_between_measurements = 10
    filename = "temp_relay_on_wisun_on_lte_load_at_65_wisun_on.xlsx"
    directory = "results"
    multimeter_conf = "mult_conf_temp.json"

    print("Connecting to instruments")
    # connecting to instruments
    mult = MultimeterInstrument.MultimeterInstrument(multimeter_address, multimeter_conf, execute_IDN=False)

    mult.load_config(multimeter_conf)
    mult.configure_measurement_scan_using_conf("Temp")

    g4 = g4_interface_cli.G4(g4_address)

    data = DataStorage.DataStorage()
    data.open(filename, directory)

    print("Starting measurement!")

    while True:
        try:
            print(f"Starting measuring cycle...")
            data["Time"] = time.asctime()

            print("Multimeter measurements")
            results = mult.make_measurement_scan_using_conf()
            for label, value in results.items():
                data[label] = value

            print("Owlet IV measurements")
            results = g4.flex_get_voltages_temp()
            for label, value in results.items():
                data[label] = value

            results = g4.flex_get_lte_temp()
            for label, value in results.items():
                data[label] = value

            data.start_next_test()

            print(f"Sleeping {time_between_measurements} seconds")
            time.sleep(time_between_measurements)
        except Exception as e:
            print(f"Error: {e}")
            time.sleep(time_between_measurements)
        except KeyboardInterrupt:
            print("Finishing")
            break

    data.save()
    data.close()
    print("End!")
