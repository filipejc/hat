"""
# Filename:     energy_meter.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   14/02/2023
# Last Update:  14/02/2023
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from pymodbus.client import ModbusSerialClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian

REGISTERS = {
    "Voltage": {"address": 0, "units": "V"},
    "Current": {"address": 6, "units": "A"},
    "Active_Power": {"address": 12, "units": "W"},
    "Apparent_Power": {"address": 18, "units": "VA"},
    "Reactive_Power": {"address": 24, "units": "VAr"},
    "Power_Factor": {"address": 30, "units": ""},
    "Phase_Angle": {"address": 36, "units": "Degrees"},
    "Frequency": {"address": 70, "units": "Hz"},
    "Import_Active_Energy": {"address": 72, "units": "kWh"},
    "Export_Active_Energy": {"address": 74, "units": "kWh"},
    "Import_Reactive_Energy": {"address": 76, "units": "kVAr"},
    "Export_Reactive_Energy": {"address": 78, "units": "kVAr"},
    "Total_Active_Energy": {"address": 342, "units": "kWh"},
    "Total_Reactive_Energy": {"address": 344, "units": "kVAr"},
}


class EnergyMonitorModbus:
    DEFAULT_BAUDRATE = 9600
    DEFAULT_PARITY = "N"

    def __init__(self, serial_port: str, slave_id: int):
        self._client = ModbusSerialClient(serial_port, baudrate=self.DEFAULT_BAUDRATE, parity=self.DEFAULT_PARITY)
        self._client.connect()
        self._slave_id = slave_id

        # self._create_get_functions()

    def get_input_register_float(self, address: int) -> float:
        response = self._client.read_input_registers(address, count=2, slave=self._slave_id)
        # print(f"response: {response}")

        value = BinaryPayloadDecoder.fromRegisters(response.registers,
                                                   byteorder=Endian.Big,
                                                   wordorder=Endian.Big).decode_32bit_float()
        return value

    def get_all_register(self):
        result = {}
        for description, parameter in REGISTERS.items():
            value = self.get_input_register_float(parameter["address"])
            result[description] = value
            print(f"Value: {description} = {value:.03} {parameter['units']}")
        return result

    def _create_get_functions(self):
        for description, parameter in REGISTERS.items():
            func = lambda: self.get_input_register_float(parameter["address"])
            func.__doc__ = f"Get {description} value with units {parameter['units']}"
            setattr(self, f"get_{description}", func)

    def __getattr__(self, item):
        if item in REGISTERS:
            return self.get_input_register_float(REGISTERS[item]["address"])
        else:
            raise AttributeError(f"item {item} not found")

    def __del__(self):
        self._client.close()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    import time
    import csv
    import numpy as np
    import matplotlib.pyplot as plt

    output_file = time.strftime("results_%d%m%Y_%H%M.csv")

    serial_port = "COM3"
    slave_id = 1

    em = EnergyMonitorModbus(serial_port, 1)

    f = open(output_file, "w", newline='')
    csv_writer = csv.DictWriter(f, delimiter=",", fieldnames=["Time", "Voltage", "Current", "Power"])
    csv_writer.writeheader()

    start_time = time.time()
    SLEEP_TIME = 0.8
    try:
       while True:
           voltage = em.Voltage
           current = em.Current
           power = em.Active_Power

           csv_writer.writerow({"Time":time.time() - start_time,
                                "Voltage": voltage,
                                "Current": current,
                                "Power": power})
           f.flush()
           time.sleep(SLEEP_TIME)

    except KeyboardInterrupt:
       print("Finishing!")
    f.close()

    # voltage = em.get_input_register_float(0)
    #
    # print(f"Voltage = {voltage}V")
    # start_time = time.time()
    #
    # data = np.zeros(100)
    #
    # for i in range(100):
    #     data[i] = em.Active_Power
    #
    # elapsed_time = (time.time() - start_time) / 100.0
    # print(f"Elapsed time: {elapsed_time}s")
    #
    # plt.plot(data)
    # plt.show()
    # print(f"Power: {em.Active_Power}")
    #
    # em.get_all_register()

    print("end!")
    # client = ModbusSerialClient("COM3", baudrate=9600, parity="N")
    # client.connect()
    #
    # resp = client.read_input_registers(70, 2)
    # print(f"response: {resp}")
    #
    # response = BinaryPayloadDecoder.fromRegisters(resp.registers,
    #                                               byteorder=Endian.Big,
    #                                               wordorder=Endian.Big)
    # print(f"results: {response.decode_32bit_float()}V")
    # client.close()
