"""
# Filename:     measure_photocell.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   06/12/2022
# Last Update:  06/12/2022
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms.g4_interface_cli import G4
from hat.utils.DataStorage import DataStorage
from hat.drivers.iluminance_driver import IlluminanceDriver
import os
import time

if __name__ == '__main__':
    # variables
    COM_port_production = "/dev/ttyUSB1"
    COM_port_aus = "/dev/ttyAMA1"
    COM_port_luxmeter = "/dev/ttyUSB0"
    use_luxmeter = True
    photocell_gain = 4
    directory = "results"
    return_all_data = True

    time_between_measurements = 10  # seconds between measurements

    # open files
    if not os.path.exists(directory):
        os.makedirs(directory)

    data = DataStorage()
    data.open(f"photocell_gain_{photocell_gain}.xlsx", directory)

    if use_luxmeter:
        print("Connection to lux meter")
        luxmeter = IlluminanceDriver(COM_port_luxmeter)
        luxmeter.get_iluminance()

    # connect to controller
    print("Connecting to controllers....")
    g4_p = G4(COM_port_production, interface_type="SERIAL")
    # g4_p = G4(COM_port_production, interface_type="SERIAL_THREAD")
    # g4_p._interface._intf.inter_byte_timeout = 0.01 # set timeout between bytes
    # g4_p.write("\n")
    g4_p.set_flex_app_prompt()
    # g4_p._interface.add_log_file("main", os.path.join(
    # directory, "G4_production_%Y%m%d_%H%M.txt"))

    g4_a = G4(COM_port_aus, interface_type="SERIAL")
    # g4_a = G4(COM_port_aus, interface_type="SERIAL_THREAD")
    g4_a.set_flex_app_prompt()
    # g4_a._interface.add_log_file("main", os.path.join(
    # directory, "G4_australia_%Y%m%d_%H%M.txt"))

    print("Turn off led!")
    g4_a.flex_set_led_status(False)
    g4_p.flex_set_led_status(False)

    # increase gain
    # stop photocell
    print("set photocell gain")
    g4_p.flex_stop_photocell()
    g4_a.flex_stop_photocell()
    time.sleep(3)

    g4_p.flex_start_photocell(photocell_gain)
    g4_a.flex_start_photocell(photocell_gain)

    cycle = 1

    while True:
        try:
            print(f"measuring cycle {cycle}...")

            try:
                photocell_p, als_data_p, gain_p, int_p = g4_p.flex_get_photocell(
                    return_all_data=return_all_data)
                print(f"Production photocell: {photocell_p} lux")
            except Exception as e:
                print(f"Error reading photocell from production device: {e}")
                photocell_p = 0
                als_data_p = 0
                gain_p = 0
                int_p = 0

            try:
                photocell_a, als_data_a, gain_a, int_a = g4_a.flex_get_photocell(
                    return_all_data=return_all_data)
                print(f"AUS photocell: {photocell_a} lux")
            except Exception as e:
                print(f"Error reading photocell from australia device: {e}")
                photocell_a = 0
                als_data_a = 0
                gain_a = 0
                int_a = 0

            data["Time"] = time.asctime()
            data["G4 production [lux]"] = photocell_p
            data["G4 aus [lux]"] = photocell_a

            if use_luxmeter:
                print("Read lux meter")
                try:
                    lux = luxmeter.get_iluminance()
                except Exception as e:
                    print(f"Could not read from photocell!, {e}")
                    lux = 0

                data["Luxmeter [lux]"] = lux

            data["Als_data p"] = als_data_p
            data["Als data a"] = als_data_a
            data["Als gain p"] = gain_p
            data["Als gain a"] = gain_a
            data["Als int p"] = int_p
            data["Als int a"] = int_a

            # data["G4 p to a difference [lux]"] = photocell_p - photocell_a
            # data["G4 p to a difference [%]"] = (
            #     photocell_p - photocell_a) / photocell_p * 100.0

            # if use_luxmeter:
            #     data["G4 p to l difference [lux]"] = photocell_p - lux
            #     data["G4 p to l difference [%]"] = (
            #         photocell_p - lux) / lux * 100.0

            #     data["G4 a to l difference [lux]"] = photocell_a - lux
            #     data["G4 a to l difference [%]"] = (
            #         photocell_a - lux) / lux * 100.0

            data.start_next_test()

            time.sleep(time_between_measurements)
        except Exception as e:
            print(
                f"Error reading, exception: {e}, waiting 5 seconds to continue...")
            time.sleep(5)

        except KeyboardInterrupt:
            print("Finishing!")
            break

    data.close()
    print("end!")
