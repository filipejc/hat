"""
# Filename:     Lte_test.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   17/01/2023
# Last Update:  17/01/2023
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms import g4_interface_cli
import time
from hat.utils import DataStorage
from hat.drivers import DC_source
from hat.drivers import AcSourceInstrument


class G4_tester:
    def __init__(self, port, filename, directory):
        print(f"Connecting to device on port {port}")
        self.g4 = g4_interface_cli.G4(port, "SERIAL")
        self.data = DataStorage.DataStorage()
        self.data.open(filename, directory)

    def start_next_test(self):
        self.data.start_next_test()

    def start(self):
        self.g4.set_prompt(flex_prompt)
        self.g4.execute_command("quit")

    def execute_test(self, test_type, label, command, expected_text, time_after, cmd_timeout=1, timeout=None,
                     stop_test=True):
        print(f"Starting test: {label}")

        test_start_time = time.time()
        test_elapsed_time = 0
        while True:
            test_elapsed_time = time.time() - test_start_time
            if test_type == "standard":
                response, elapsed_time = self._execute_test_standard(command)
            elif test_type == "lte":
                response, elapsed_time = self._execute_test_flex(command, cmd_timeout)
            else:
                raise Exception(f"Test type {test_type} not recognized")

            # check if module is enabled
            if expected_text not in response:
                if stop_test and timeout is None:
                    raise Exception(f"Expected test: {expected_text} not present on response: {response}")

                    print(f"Expected test: {expected_text} not present on response: {response}")
                test_result = False
            else:
                test_result = True

            # if timeout is nto defined or test_result is positive
            if timeout is None or test_result:
                break

            if time.time() - test_start_time > timeout:
                break

        if not test_result and stop_test:
            raise Exception(f"Expected test: {expected_text} not present on response: {response}")

        print(f"Sleep for {time_after}s")
        time.sleep(time_after)

        self.data["Label"] = label
        self.data["Command"] = command
        self.data["Expected"] = expected_text
        self.data["time after"] = time_after
        self.data["Test result"] = test_result
        self.data["Run time"] = test_elapsed_time + elapsed_time
        self.data["Response"] = response
        self.data.start_next_test()

        return test_result, elapsed_time

    def _execute_test_flex(self, command, lte_command_timeout):
        response, elapsed_time = self.g4.execute_command_lte_shell(command, lte_command_timeout)

        return response, elapsed_time

    def _execute_test_standard(self, command):
        start_time = time.time()
        self.g4.set_prompt(flex_prompt)
        response = self.g4.execute_command(command)
        print(f"response: {response}")

        elapsed_time = time.time() - start_time
        return response, elapsed_time


if __name__ == '__main__':
    # variables
    COM_port = "COM6"
    lte_prompt = "lte $ \r\nlte $ "
    flex_prompt = "$"
    device_imi = "901288006248681"
    use_AC_PSU = False
    use_DC_PSU = True

    dc_power_supply_addr = "USB0::0x05E6::0x2230::802895020757510003::INSTR"

    ac_power_supply_addr = "COM5"

    # timings
    time_after_service_cell = 0.5
    time_after_status = 0.5
    time_after_simstat = 0.5
    time_after_imsi = 0.5
    time_after_nwscan = 0.5
    time_after_service_cell_found = 0.5
    time_after_nvwrite = 0.5
    time_after_nvread = 0.5
    lte_command_timeout = 1  # timeout
    stop_after_failure = True

    # channel
    dc_channel = 1
    time_power_off = 20  # time to discharge
    startup_time = 5
    lte_startup_time = 30
    lte_search_time = 40

    # devices
    directory = "lte_debug"
    filename = "test_run.xlsx"
    tester = G4_tester(COM_port, filename, directory)
    tester.start()

    # dc power supply
    if use_DC_PSU:
        dc = DC_source.DCSource(dc_power_supply_addr)

        def power_turn_on():
            dc.set_output_state(dc_channel, True)

        def power_turn_off():
            dc.set_output_state(dc_channel, False)

    if use_AC_PSU:
        ac = AcSourceInstrument.AcSourceInstrument(ac_power_supply_addr)

        def power_turn_on():
            ac.set_output_state(True)

        def power_turn_off():
            ac.set_output_state(False)

    for cycle in range(100):
        print(f"staring cycle {cycle}, disconnecting DC power supply")
        power_turn_off()

        print(f"Waiting {time_power_off} seconds")
        time.sleep(time_power_off)

        print(f"Turn on..., waiting {startup_time} seconds for startup")
        power_turn_on()
        time.sleep(startup_time)

        print("Starting measurement")
        tester.g4.set_prompt(flex_prompt)
        # get status
        status_result, status_time = tester.execute_test("standard",
                                                         "lte status", "lte-status", "LTE STATUS Pin: High",
                                                         time_after_status, cmd_timeout=lte_command_timeout,
                                                         timeout=lte_startup_time,
                                                         stop_test=stop_after_failure)

        tester.g4.set_prompt(lte_prompt)
        tester.g4.execute_command("lte-shell")

        # SIM STAT
        SIMSTAT, simstat_time = tester.execute_test("lte", "QSIMSTAT", "AT+QSIMSTAT?", "+QSIMSTAT: 0,1",
                                                    time_after_simstat,
                                                    lte_command_timeout)

        # get imi
        IMI, imi_delay_time = tester.execute_test("lte", "IMSI", "AT+CIMI", device_imi, time_after_imsi,
                                                  lte_command_timeout)

        # set nwscanmode
        scan_mode, scan_delay_time = tester.execute_test("lte", "Network scan on", 'AT+QCFG="nwscanmode",1,1', "OK",
                                                         time_after_nwscan, lte_command_timeout)

        # service cell test
        servcell, delay_time = tester.execute_test("lte", "SERVINGCELL", 'AT+QENG="SERVINGCELL"',
                                                   'QENG: "servingcell","NOCONN"',
                                                   time_after_service_cell, lte_command_timeout,
                                                   timeout=lte_search_time, stop_test=False)

        # non volatile writing
        nv_write, delay_time = tester.execute_test("lte", "NV write 2509", 'AT+QNVW=2509,0,"08"', "OK",
                                                   time_after_nvwrite, lte_command_timeout)

        # reading NV
        nv_read, delay_time = tester.execute_test("lte", 'NV RD 1', 'AT+QNVR=2509,0', '+QNVR: "08"', time_after_nvread,
                                                  lte_command_timeout)

        # non volatile writing
        nv_write, delay_time = tester.execute_test("lte", 'NV write 911', 'AT+QNVW=911,0,"08"', "OK",
                                                   time_after_nvwrite, lte_command_timeout)

        # reading NV
        nv_read, delay_time = tester.execute_test("lte", 'NV RD 2', 'AT+QNVR=2509,0', '+QNVR: "08"', time_after_nvread,
                                                  lte_command_timeout)

        # set nwscanmode
        scan_mode, scan_delay_time = tester.execute_test("lte", "Network scan on", 'AT+QCFG="nwscanmode",0,1', "OK",
                                                         time_after_nwscan, lte_command_timeout)

        print("Exit LTE shell")
        tester.g4.set_prompt(flex_prompt)
        tester.g4.execute_command("quit")

    tester.data.close()

    print("end!")
