"""
# Filename:     parse_1_10v_logs.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   18/04/2023
# Last Update:  18/04/2023
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import re
import pandas as pd
import os

if __name__ == '__main__':
    directory = r"C:\Users\fcoelho\OneDrive - Schréder SA\Bureau\1-10v\DALI_0-10V\DALI_0-10V"
    data = pd.DataFrame()

    idx = 0

    for filename in os.listdir(directory):
        print(f"Parsing filename: {filename}")

        if not filename.startswith("NLT"):
            continue

        with open(os.path.join(directory, filename)) as f:
            text = f.read()
            matchs = re.findall(r"voltage: (\d+)\smVOK\$\$(\d+\.\d+)\,(\w+)", text)
            print(matchs)

            for match in matchs:
                data.loc[idx, "file"] = filename
                data.loc[idx, "Get voltage [V]"] = float(match[0]) / 1000.0
                data.loc[idx, "Multimeter [V]"] = float(match[1])
                data.loc[idx, "Pass/Fail"] = match[2]
                idx += 1

    data.to_excel("results_1_10v.xlsx")
    print("end!")

