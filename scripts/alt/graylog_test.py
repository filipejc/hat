import logging
import logging
import graypy
import time

class UsernameFilter(logging.Filter):
    def __init__(self):
        # In an actual use case would dynamically get this
        # (e.g. from memcache)
        self.username = 'Filipe'

    def filter(self, record):
        record.username = self.username
        return True

my_logger = logging.getLogger('test_logger')
my_logger.setLevel(logging.DEBUG)

handler = graypy.GELFTCPHandler('localhost', 12201)
my_logger.addHandler(handler)
my_logger.addFilter(UsernameFilter())

for i in range(10):
    my_logger.debug(f'Hello Graylog: {i}')
    time.sleep(0.1)