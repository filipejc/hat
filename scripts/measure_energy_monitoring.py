import sys

sys.path.append('..\hat')
sys.path.append('C:\code')
sys.path.append('C:\code\hat')
import os
import numpy as np
import time
import logging
import matplotlib.pyplot as plt
import hat.utils.DataStorage
from hat.comms import g4_interface_cli
from hat.drivers import AcPowerMeterInstrument
from hat.drivers import AcSourceInstrument
from hat.drivers import ACDC_load
from datetime import datetime
from genericpath import exists


log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
if not exists("./log"):
    os.mkdir("./log")
logging.basicConfig(filename = './log/{:%Y-%m-%d}.log'.format(datetime.now()),
                    filemode = "w+",
                    format = '%(asctime)s %(levelname)-8s %(message)s', 
                    level = logging.ERROR)
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)

# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")

def test_method():
    log.info("Energy Start")
    ac = AcSourceInstrument.AcSourceInstrument("COM3", reset=False)
    log.info("Energy End")


if __name__ == "__main__":

    # Device parameters
    DEVICE = 'DATALIFT'
    VOLTAGE = 120
    I_MAX = 7.5
    TA = 2.5
    I_MIN = 0.15
    I_ST = 0.01
    DEFAULT_FREQUENCY = 60
    DEFAULT_POWER_FACTOR = 1

    # test parameters
    TEST_CASE = 'T2'
    TEST_CASE_CONDITION = 'C3'
    PROCESSING_TIME = 0.9               # time to be reduced from Test time setting delay due to processing
    use_ac_load = True                  # to use AC load
    plot = True                         # make plot
    compensate_negative = False
    power_on_time = 10                  # number of second it takes to start
    source_stability_time = 5           # time to the source stability
    load_stability_time = 10            # time to the load stability
    use_flex_app = True

    # connection strings
    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    g4_com_port = "COM3"
    ac_com_port = "ASRL5::INSTR"
    load_com_port = "ASRL4::INSTR"

    test_dict = {
        'T2': {
            'C1': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 15,
                'interval': 12-PROCESSING_TIME
                # 'interval': 5.19-PROCESSING_TIME
            },
            'C2': {
                'voltage': VOLTAGE,
                'current': I_MIN,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 200-PROCESSING_TIME
                # 'interval': 86.64-PROCESSING_TIME
            },
            'C3': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            },
            'C4': {
                'voltage': VOLTAGE,
                'current': 0.2*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 60-PROCESSING_TIME
                # 'interval': 25.99-PROCESSING_TIME
            },
            'C5': {
                'voltage': VOLTAGE,
                'current': 0.5*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 15,
                'interval': 24-PROCESSING_TIME
                # 'interval': 10.39-PROCESSING_TIME
            },
            'C6': {
                'voltage': VOLTAGE,
                'current': 1.5*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor':DEFAULT_POWER_FACTOR,
                'trials': 15,
                'interval': 8-PROCESSING_TIME
                # 'interval': 3.46-PROCESSING_TIME
            },
            'C7': {
                'voltage': VOLTAGE,
                'current': I_MAX,
                # 'current': 5,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 20,
                # 'interval': 3-PROCESSING_TIME
                'interval': 2.6-PROCESSING_TIME
            },
        },
        'T3': {
            'C1': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': 1,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            },
            'C2': {
                'voltage': VOLTAGE,
                'current': 0.2*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': -0.5,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            },
            'C3': {
                'voltage': VOLTAGE,
                'current': 0.2*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': +0.5,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            },
            'C4': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': 1,
                'trials': 12,
                'interval': 12-PROCESSING_TIME
                # 'interval': 5.20-PROCESSING_TIME
            },
            'C5': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': -0.5,
                'trials': 12,
                'interval': 24-PROCESSING_TIME
                # 'interval': 10.40-PROCESSING_TIME
            },
            'C6': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': +0.5,
                'trials': 12,
                'interval': 24-PROCESSING_TIME
                # 'interval': 10.40-PROCESSING_TIME
            },
            'C7': {
                'voltage': VOLTAGE,
                'current': I_MAX,
                # 'current': 5,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': 1,
                'trials': 12,
                'interval': 4-PROCESSING_TIME
                # 'interval': 2.60-PROCESSING_TIME
            },
            'C8': {
                'voltage': VOLTAGE,
                'current': I_MAX,
                # 'current': 5,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': -0.5,
                'trials': 12,
                'interval': 8-PROCESSING_TIME
                # 'interval': 5.20-PROCESSING_TIME
            },
            'C9': {
                'voltage': VOLTAGE,
                'current': I_MAX,
                # 'current': 5,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': +0.5,
                'trials': 12,
                'interval': 8-PROCESSING_TIME
                # 'interval': 5.20-PROCESSING_TIME
            }
        },
        'T4': {
            'C1': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 120-PROCESSING_TIME
                'interval': 51.99-PROCESSING_TIME
            },
            'C2': {
                'voltage': 0.9*VOLTAGE,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 133.33-PROCESSING_TIME
                'interval': 57.76-PROCESSING_TIME
            },
            'C3': {
                # 'voltage': 1.1*VOLTAGE,
                'voltage': 300,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 109.09-PROCESSING_TIME
                'interval': 47.26-PROCESSING_TIME
            },
            'C4': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 12-PROCESSING_TIME
                'interval': 5.20-PROCESSING_TIME
            },
            'C5': {
                'voltage': 0.9*VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 13.33-PROCESSING_TIME
                'interval': 5.78-PROCESSING_TIME
            },
            'C6': {
                # 'voltage': 1.1*VOLTAGE,
                'voltage': 300,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 10.91-PROCESSING_TIME
                'interval': 4.73-PROCESSING_TIME
            }
        },
        'T5': {
            'C1': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            },
            'C2': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': 0.98*DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            },
            'C3': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': 1.02*DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            },
            'C4': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 12-PROCESSING_TIME
                # 'interval': 5.20-PROCESSING_TIME
            },
            'C5': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': 0.98*DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 12-PROCESSING_TIME
                # 'interval': 5.20-PROCESSING_TIME
            },
            'C6': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': 1.02*DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 12-PROCESSING_TIME
                # 'interval': 5.20-PROCESSING_TIME
            }
        },
        'T7': {
            'C1': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 12-PROCESSING_TIME
                # 'interval': 5.20-PROCESSING_TIME
            },
            'C2': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': -0.5,
                'trials': 12,
                'interval': 24-PROCESSING_TIME
                # 'interval': 10.40-PROCESSING_TIME
            },
            'C3': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': 0.5,
                'trials': 12,
                'interval': 24-PROCESSING_TIME
                # 'interval': 10.40-PROCESSING_TIME
            },
            'C4': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                'interval': 120-PROCESSING_TIME
                # 'interval': 51.99-PROCESSING_TIME
            }
        }
    }

    power_on_time = 5  # number of second it takes to start

    ### voltage under test
    voltages = np.arange(230, 300+0.1, 10)
    currents = np.arange(0, 1 + 0.1, 0.1)

    ### current under test
    # currents = np.arange(10.5, 15 + 0.5, 0.5)   # Current saturation test
    # currents = [2.50]                           # Current for Accuracy test at 120V
    # currents = [1.00]                           # Current for Accuracy test at 277V
    # currents = [0.15]                           # Current for Load Performance test of ANSIS
    currents = [test_dict[TEST_CASE][TEST_CASE_CONDITION]['current']]
    power_factor = test_dict[TEST_CASE][TEST_CASE_CONDITION]['power_factor']
    frequency = test_dict[TEST_CASE][TEST_CASE_CONDITION]['frequency']
    trials = test_dict[TEST_CASE][TEST_CASE_CONDITION]['trials']
    trials_interval = test_dict[TEST_CASE][TEST_CASE_CONDITION]['interval']

    max_current_peak = max(currents) * 2.0  # needs to account with crest factor
    filename = DEVICE+"_ANSI_C136_50_"+str(VOLTAGE)+"V_"+TEST_CASE+TEST_CASE_CONDITION+".xls" 

    ### test parameters
    time_sleep = 5
    filename = "Schreder_FW.xls"
    repetitions = 4
    use_flex_app = False

    #### connection strings
    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    g4_com_port = "COM6"
    ac_com_port = "ASRL4::INSTR"
    load_com_port = "ASRL5::INSTR"

    log.info("Starting test")

    ### connect to instruments and G4
    log.info("Connecting to AC Source ...")
    ac = AcSourceInstrument.AcSourceInstrument(ac_com_port, reset=False)
    log.info("AC source connected")

    if use_ac_load:
        log.info("Connecting to ACDC load ...")
        load = ACDC_load.ACDCLoad(load_com_port, reset=False)
        log.info("ACDC load connected")

    pm = AcPowerMeterInstrument.AcPowerMeterInstrument(USB_connection_string)
    pm.set_integrator_mode()
    pm.add_meas_active_power()
    pm.add_meas_integration_time()
    pm.conf_integrator_manual_mode()
    pm.reset_integrator()

    g4 = g4_interface_cli.G4(g4_com_port, interface_type="SERIAL_THREAD")
    if use_flex_app:
        g4.set_prompt("$")
        g4.flex_set_relay(True)
    else:
        g4.set_global_loglevel(4)

    ## create data structure
    data = hat.utils.DataStorage.DataStorage()
    data.open(filename)

    #### turn on ac
    ac.set_voltage_AC(voltages[0])
    time.sleep(3)
    ac.set_output_state(True)

    print(f"Waiting {power_on_time} seconds, for device to start")
    time.sleep(power_on_time)

    ### configure load
    if use_ac_load:
        load.set_load_mode("CURR")
        load.clear_protection_status()
        load.set_current_AC(0)
        load.set_peak_current_AC(max_current_peak)
        load.set_power_factor(power_factor)
        load.set_crest_factor(1.414)
        load.get_power_factor()
        load.get_crest_factor()
        time.sleep(3)
        load.set_output_state(True)

    for voltage in voltages:
        for current in currents:
            log.info(f"Testing voltage {voltage} V, current {current} A")

            ac.set_voltage_AC(voltage)
            time.sleep(source_stability_time)

            if use_ac_load:
                load.set_current_AC(current)
                time.sleep(load_stability_time)

            time.sleep(time_sleep)
            for i in range(repetitions):
                data["repetition"] = i + 1
                log.info(f"Staring repetition {i + 1}")
                # read from G4
                if use_flex_app:
                    measuring_data = g4.flex_get_meter_data()
                    g4_voltage = measuring_data["voltage"]
                else:
                    g4_voltage = g4.lwm2m_get_supply_voltage()

                if use_ac_load:
                    if use_flex_app:
                        g4_current = measuring_data["current"]
                    else:
                        g4_current = g4.lwm2m_get_supply_current()

                if use_flex_app:
                    g4_active_power = measuring_data["power"]
                else:
                    g4_active_power = g4.lwm2m_get_active_power()
                # g4_freq = g4.lwm2m_get_frequency()
                # g4_pf = g4.lwm2m_get_power_factor()

                # PM1000
                meas = pm.get_measurement()

                data["Set Voltage [V]"] = voltage
                data["G4 voltage [V]"] = g4_voltage

                if use_ac_load:
                    data["Set Current [A]"] = current
                    data["G4 current [A]"] = g4_current

                data["G4 active power [W]"] = g4_active_power
                # data["G4 frequency [Hz]"] = g4_freq
                # data["G4 power factor"] = g4_pf

                for key, value in meas.items():
                    data[f"PM {key}"] = np.abs(value) if compensate_negative else value

                data.start_next_test()

    data.save()

    # teardown
    if use_ac_load:
        load.set_output_state(False)
    
    ac.set_output_state(False)
    pm.stop_integrator()

    if plot:
        # plt.plot(data["PM Arms"], data["G4 current [A]"], "-go")
        # plt.legend("Current")
        # plt.xlabel("Reference Current [A]")
        # plt.ylabel("G4 Current [A]")
        # plt.title("G4 Current test vs Reference Current Meter[A]")
        plt.plot(data["PM Whr"], data["G4 active energy [Wh]"], "-go", label="Active Energy")
        plt.legend("Active Energy")
        plt.xlabel("Reference Active Energy [Wh]")
        plt.ylabel("G4 Active Energy [Wh]")
        # plt.title("G4 Active Energy vs Reference Active Energy Meter [Wh]")
        plt.grid(True)
        plt.show()

    log.info("End!")