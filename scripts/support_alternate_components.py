"""
# Filename:     support_alternate_components.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   30/06/2021
# Last Update:  30/06/2021
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import pandas as pd
import os
import numpy as np


def is_empty(item):
    return isinstance(item, float) and np.isnan(item)


def load_all_bom(xlsx):
    data = pd.read_excel(xlsx, sheet_name=None)
    res = None

    # bom pre-processing
    for key, item in data.items():
        item["board"] = key
        if res is None:
            res = item
        else:
            res = res.append(item, ignore_index=False)

    res = res.set_index(np.arange(res.shape[0]))

    # fill empty lines
    for idx, row in res.iterrows():
        if not is_empty(row["Level"]):
            last_item_number = row["Item Number"]

            res.loc[idx, "Item Number"] = last_item_number

            last_ref_des = row["Ref Des"]
            res.loc[idx, "Item Number"] = last_item_number
        else:
            res.loc[idx, "Item Number"] = last_item_number
            res.loc[idx, "Ref Des"] = last_ref_des

    return res


def load_critical_components(xlsx):
    data = pd.read_excel(xlsx, sheet_name=None)
    res = None

    # bom pre-processing
    for key, item in data.items():
        item["board"] = key
        if res is None:
            res = item
        else:
            res = pd.concat([res, item])

    return res


if __name__ == "__main__":
    # variables
    # files
    alternate_xlsx = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\08_Support\20210628_HCM-227_alternate_components\Alternate_component_comparison_06.28.xlsx"

    all_bom_xlsx = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\01_HW\01_Schematic\04_PVT\All_BOMs_PVT.xlsx"
    key_part_xlsx = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\06_Factory\08_critical_componentes\Key Parts_simple.xlsx"

    output_dir = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\08_Support\20210628_HCM-227_alternate_components"
    output_file = "analysis.xlsx"

    # processing alternate
    alternate = pd.read_excel(alternate_xlsx, skiprows=7)

    # BOM
    all_bom = load_all_bom(all_bom_xlsx)

    all_bom["Short MPN"] = all_bom["MFR Part Number"]
    all_bom["Short MPN"] = all_bom["Short MPN"].apply(lambda x: x[:-3])

    all_bom.to_excel(os.path.join(output_dir, "all_bom.xlsx"))

    # Key parts
    key_parts = load_critical_components(key_part_xlsx)

    output = pd.DataFrame()

    item_label = "Item Number"

    for idx, row in alternate.iterrows():
        print(f"IDX: {idx}, row: {row}")
        PN = row["Customer P/N"]
        MFG = row["QUOTED MFR"]
        MPN = row["QUOTED MPN"]
        output.loc[idx, "PN"] = PN
        output.loc[idx, "MFG"] = MFG
        output.loc[idx, "MFG PN"] = MPN

        if MPN == 0:
            continue

        # check key components
        keys = key_parts[key_parts["Item Number"] == PN]

        if not keys.empty:
            output.loc[idx, "Key part"] = True
            for key_idx, key in keys.iterrows():
                board = key["board"]
                output.loc[idx, f"Key part {board}"] = True
        else:
            output.loc[idx, "Key part"] = False

        # check if part already in BOM

        # check part number
        # parts = all_bom[(all_bom["MFR Name"] == MFG) & (all_bom["MFR Part Number"] == MPN)]

        parts = all_bom[(all_bom["MFR Part Number"] == MPN)]

        if not parts.empty:
            print(f"Parts: {parts}")
            output.loc[idx, "Already on BOM"] = True
            PNs = []
            equal = []

            for part_idx, part in parts.iterrows():
                PNs.append(part["Item Number"])
                equal.append(part["Item Number"] == PN)

            output.loc[idx, "MPN mapping to PNs"] = ",".join(PNs)
            output.loc[idx, "MPN on same PN"] = ",".join(map(str, equal))
        else:
            output.loc[idx, "Already on BOM"] = False

        almost_same_parts = all_bom[all_bom["Short MPN"] == MPN[:-3]]

        if not almost_same_parts.empty and parts.empty:
            output.loc[idx, "Almost same MPN"] = True
            output.loc[idx, "Similar MPN"] = almost_same_parts.iloc[0]["MFR Part Number"]
        else:
            output.loc[idx, "Almost same MPN"] = False

        # check where used
        used = all_bom[(all_bom["Item Number"] == PN)]
        where_used = []
        for used_idx, used_row in used.iterrows():
            where_used.append(f"{used_row['board']}: {used_row['Position']} - {used_row['Ref Des']}")

        output.loc[idx, "Where used"] = ",".join(where_used)

    output.to_excel(os.path.join(output_dir, output_file))
    print(f"alternate: {alternate}")

    print("end")
