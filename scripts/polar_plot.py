"""
# Filename:     polar_plot.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   07/05/2021
# Last Update:  07/05/2021
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plot
import os

first_form = False
second_form = True
if first_form:

    dirname = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\09_Project\03_NBIOT_DVT\Performance\Antenna\Wisun_pattern_issue"
    filename = r"owlet compare data_20210506.xlsx"

    data_dvt = pd.read_excel(os.path.join(dirname,filename), skiprows=1, usecols=[1, 2]).rename(columns={"Phi.1":"Phi", "Gain . dB":"Gain"})
    data_evt = pd.read_excel(os.path.join(dirname,filename), skiprows=1, usecols=[5, 6]).rename(columns={"Phi.3":"Phi", "Gain . dB.1":"Gain"})

elif second_form:
    dirname = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\09_Project\03_NBIOT_DVT\Performance\Wisun\NB-DVT wisun condutived performance"
    filename = r"G4 Wi-sun 2.4Ghz antenna compared data.xlsx"

    data_evt = pd.read_excel(os.path.join(dirname,filename), usecols=[0, 1]).rename(columns={"Phi":"Phi", "EVT-2":"Gain"})
    data_dvt = pd.read_excel(os.path.join(dirname,filename), usecols=[0, 2]).rename(columns={"Phi":"Phi", "DVT":"Gain"})
    data_dvt_improvement = pd.read_excel(os.path.join(dirname,filename), usecols=[0, 3]).rename(columns={"Phi":"Phi", "DVT-add  one ground pin":"Gain"})

plot.axes(projection='polar')

# Set the title of the polar plot

plot.title('Comparison between DVT and EVT2 in NBIOT wisun radiation pattern')

plot.polar(np.deg2rad(data_evt["Phi"]), data_evt["Gain"], "r")
plot.polar(np.deg2rad(data_dvt["Phi"]), data_dvt["Gain"], "g")
if second_form:
    plot.polar(np.deg2rad(data_dvt_improvement["Phi"]), data_dvt_improvement["Gain"], "b")

plot.ylabel("Gain [dBi]")

#
# for idx, row in data_evt.iterrows():
#     plot.polar(np.deg2rad(row["Phi"]), row["Gain"], "r+")
#     print(row)
#
# for idx, row in data_dvt.iterrows():
#     plot.polar(np.deg2rad(row["Phi"]), row["Gain"], "g+")
#     print(row)

legends = ["EVT2", "DVT"]
if second_form:
    legends.append("DVT improvement")

plot.legend(legends, loc="lower right")
plot.ylim([-15, 5])
plot.savefig(os.path.join(dirname, "EVT_DVT_comparison.jpg"))
plot.show()

plot.cla()
diff_gain = data_dvt["Gain"] - data_evt["Gain"]
plot.plot(data_evt["Phi"], diff_gain)
plot.title("Gain difference DVT to EVT")
plot.xlabel("Phi [degree]")
plot.ylabel("Gain difference [dB]")
plot.grid(True)
plot.savefig(os.path.join(dirname, "DVT_to_EVT2_difference.jpg"))

plot.show()
print("end!")
