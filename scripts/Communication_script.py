from hat.comms.g4_interface_cli import G4
from hat.utils import utils
from hat.utils import DataStorage
import threading
import time

log = utils.create_logger(__name__)


def set_all_to_key(dictionary, label, value):
    """
    set in all child dictionaries, the same value for the required label

    Parameters
    ----------
    dictionary
    label
    value

    Returns
    -------

    """
    for dev, item in dictionary.items():
        dictionary[dev][label] = value


def apply_all(dictionary, label, func, *value):
    """
    Apply a function to all the same label inside the child dictionaries

    Parameters
    ----------
    dictionary
    label
    func
    value

    Returns
    -------

    """
    for dev, item in dictionary.items():
        func(item[label], *value)


class G4_comms_tests(threading.Thread):
    def __init__(self, label: str, g4: G4, data: DataStorage.DataStorage, conf, ncycles):
        super().__init__()
        self.label = label
        self.g4 = g4
        self.data = data
        self.conf = conf
        self.ncycles = ncycles

    def run(self):
        # only to make it easier to run
        label = self.label
        data = self.data
        g4 = self.g4
        g4.set_prompt("$")

        # enable wisun
        g4.flex_lpwn_enable()

        for i in range(self.ncycles):
            try:
                log.info(f"{label}: starting cycle {i}")
                data["Cycle"] = i
                data["Time"] = time.asctime()

                # interfaces to check
                # photocell
                photocell = g4.flex_get_photocell()
                data["Photocell"] = photocell
                # spi-flash
                flash_test = g4.flex_execute_spi_flash_test()
                data["SPI flash"] = flash_test
                # wisun
                wisun = g4.flex_lpwn_txpd()
                data["Wisun"] = wisun
                # Lte, NBIOT in DataLift
                if self.conf["isDataLift"]:
                    # Modem get connection status
                    unsolicited, creg_status = g4.flex_get_creg()
                    unsolicited, cgreg_status = g4.flex_get_cgreg()
                    data["creg"] = creg_status
                    data["creg status"] = creg_status == self.conf["expectedCREG"]
                    data["cgreg"] = cgreg_status
                    data["cgreg status"] = cgreg_status == self.conf["expectedCGREG"]

                    # ESIM in DataLift
                    imsi = g4.flex_get_imsi()
                    data["imsi"] = imsi

                # Metering in Nema
                if self.conf["isNema"]:
                    res = g4.flex_get_meter_data()
                    data["voltage"] = res["voltage"]
                    data["current"] = res["current"]
                    data["power"] = res["power"]

                    # RFID
                    rfid = g4.flex_get_rfid()
                    data["rfid"] = rfid

                # GPS
                gps = g4.flex_gps_dump()
                data["GGA count"] = gps["GGA count"]

                # power voltages
                res = g4.flex_get_voltages_temp()
                data["24V"] = res["24V"]
                data["supercap"] = res["supercap"]
                data["MCU temp"] = res["MCU temp"]

                data["Error"] = False
            except Exception as e:
                log.error("error during execution: ", e)
                data["Error"] = True
                data["Error message"] = str(e)

            data.start_next_test()
            log.info(f"{label}: sleeping for {time_between_cycles}")
            time.sleep(time_between_cycles)  # sleep between cycles


if __name__ == '__main__':
    log.info("Starting!")

    # variables
    n_cycles = 5000
    time_between_cycles = 15

    confs = {
        "DataLiftN": {
            "address": "COM247",
            "datafile": "data/DataLiftN_communication.xlsx",
            "logfile": "data/DataLiftN_%H%M_%Y%m%d.txt",
            "isNema": True,
            "isDataLift": True,
            "expectedCREG": 5,
            "expectedCGREG": 5,
        },
        "DataLiftZ": {
            "address": "COM244",
            "datafile": "data/DataLiftZ_communication.xlsx",
            "logfile": "data/DataLiftZ_%H%M_%Y%m%d.txt",
            "isNema": False,
            "isDataLift": True,
            "expectedCREG": 3,
            "expectedCGREG": 4,

        },
        "MeshNodeN": {
            "address": "COM246",
            "datafile": "data/MeshNodeN_communication.xlsx",
            "logfile": "data/MeshNodeN_%H%M_%Y%m%d.txt",
            "isNema": True,
            "isDataLift": False
        },
        "MeshNodeZ": {
            "address": "COM245",
            "datafile": "data/MeshNodeZ_communication.xlsx",
            "logfile": "data/MeshNodeZ_%H%M_%Y%m%d.txt",
            "isNema": False,
            "isDataLift": False
        }
    }

    threads = []

    # open interfaces and create datafiles
    for label, conf in confs.items():
        g4 = G4(conf["address"], "SERIAL_THREAD", log_file=conf["logfile"])
        data = DataStorage.DataStorage()
        data.open(conf["datafile"])
        threads.append(G4_comms_tests(label, g4, data, conf, n_cycles))

    log.info("Starting threads")
    for thread in threads:
        thread.start()

    log.info("Waiting for thread to end")
    for thread in threads:
        thread.join()

    log.info("end!")
