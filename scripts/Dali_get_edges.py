from hat.drivers.Oscilloscope import Oscilloscope
from hat.utils import utils
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

log = utils.create_logger("Get edges")


def find_edges(wav_time, wav_voltage, low_level, high_level, second_high_level, third_high_level, low_level_perc,
               high_level_perc,
               hysterisis=5, edge_time=10e-6, level_time=200e-6, time_between_frames=1e-3, transition_time_scale=1e-6):
    edges = pd.DataFrame()
    idx = 0

    wav_time = np.array(wav_time)
    wav_voltage = np.array(wav_voltage)

    high_voltage = np.max(wav_voltage)
    low_voltage = np.min(wav_voltage)

    threshold_voltage = (high_voltage + low_voltage) / 2
    threshold_hyterisis = (high_voltage - low_voltage) * hysterisis / 100

    current_state = True if wav_voltage[0] > threshold_voltage else False
    current_threshold = (threshold_voltage - threshold_hyterisis) if current_state else (
            threshold_voltage + threshold_hyterisis)

    current_frame = 1
    last_bit_time = 0

    sample_time = wav_time[1] - wav_time[0]
    samples_to_look = int(edge_time / sample_time)

    point_idx = 0

    log.info("Calculating edges...")

    for wav_time_point, wav_voltage_point in zip(wav_time, wav_voltage):
        if current_state:  # finding falling edges
            if wav_voltage_point < current_threshold:
                edges.loc[idx, "wav_time"] = wav_time_point
                edges.loc[idx, "wav_voltage"] = wav_voltage_point
                edges.loc[idx, "edge_type"] = "Fall"
                edges.loc[idx, "point"] = point_idx

                current_state = False
                current_threshold = threshold_voltage + threshold_hyterisis

                if last_bit_time != 0 and wav_time_point - last_bit_time > time_between_frames:
                    current_frame += 1
                edges.loc[idx, "frame"] = current_frame
                last_bit_time = wav_time_point

                idx += 1

        else:  # finding rising edges
            if wav_voltage_point > current_threshold:
                edges.loc[idx, "wav_time"] = wav_time_point
                edges.loc[idx, "wav_voltage"] = wav_voltage_point
                edges.loc[idx, "edge_type"] = "Rise"

                edges.loc[idx, "point"] = point_idx

                current_state = True
                current_threshold = threshold_voltage - threshold_hyterisis

                if last_bit_time != 0 and wav_time_point - last_bit_time > time_between_frames:
                    current_frame += 1
                edges.loc[idx, "frame"] = current_frame
                last_bit_time = wav_time_point

                idx += 1

        point_idx += 1

    log.info(f"Calculating Level edges based on voltage: {low_voltage}V and {high_level}V...")

    # find level edges
    for idx, edge in edges.iterrows():
        edge_type = edge["edge_type"]

        start_point = int(edge["point"])

        if edge_type == "Rise":  # rising edge:
            direction = 1
        else:
            direction = -1

        # find max voltage
        for deviation, voltage_point in enumerate(wav_voltage[start_point:start_point + direction * samples_to_look:direction]):
            if voltage_point > high_level:
                edges.loc[idx, "level_high_voltage"] = voltage_point
                edges.loc[idx, "level_high_point"] = start_point + direction * deviation
                edges.loc[idx, "level_high_time"] = wav_time[int(edges.loc[idx, "level_high_point"])]
                break

        # find second high voltage
        for deviation, voltage_point in enumerate(wav_voltage[start_point:start_point + direction * samples_to_look:direction]):
            if voltage_point > second_high_level:
                edges.loc[idx, "level_second_high_voltage"] = voltage_point
                edges.loc[idx, "level_second_high_point"] = start_point + direction * deviation
                edges.loc[idx, "level_second_high_time"] = wav_time[int(edges.loc[idx, "level_second_high_point"])]
                break

        # find third high voltage
        for deviation, voltage_point in enumerate(
                wav_voltage[start_point: start_point + direction * samples_to_look:direction]):
            if voltage_point > third_high_level:
                edges.loc[idx, "level_third_high_voltage"] = voltage_point
                edges.loc[idx, "level_third_high_point"] = start_point + direction * deviation
                edges.loc[idx, "level_third_high_time"] = wav_time[int(edges.loc[idx, "level_third_high_point"])]
                break

        # find mim voltage
        for deviation, voltage_point in enumerate(
                wav_voltage[start_point:start_point + - direction * samples_to_look:-direction]):
            if voltage_point < low_level:
                edges.loc[idx, "level_low_voltage"] = voltage_point
                edges.loc[idx, "level_low_point"] = start_point - direction * deviation
                edges.loc[idx, "level_low_time"] = wav_time[int(edges.loc[idx, "level_low_point"])]

                break
        try:
            edges.loc[idx, "level_transition_time"] = abs(
                edges.loc[idx, "level_high_time"] - edges.loc[idx, "level_low_time"]) / transition_time_scale
        except KeyError as e:
            edges.loc[idx, "level_transition_time"] = 0

        try:
            edges.loc[idx, "level_second_transition_time"] = abs(
                edges.loc[idx, "level_second_high_time"] - edges.loc[idx, "level_low_time"]) / transition_time_scale
        except KeyError as e:
            edges.loc[idx, "level_second_transition_time"] = 0

        try:
            edges.loc[idx, "level_third_transition_time"] = abs(
                edges.loc[idx, "level_third_high_time"] - edges.loc[idx, "level_low_time"]) / transition_time_scale
        except KeyError as e:
            edges.loc[idx, "level_third_transition_time"] = 0
        idx += 1

    # calculate percentage edges
    log.info(f"Calculating percentage edges based on {low_level_perc}% and {high_level_perc}%")

    for idx, edge in edges.iterrows():
        edge_type = edge["edge_type"]

        start_point = int(edge["point"])
        before_level = np.average(wav_voltage[int(start_point - edge_time / 2 / sample_time - level_time / sample_time):
                                              int(start_point - edge_time / 2 / sample_time)])

        after_level = np.average(wav_voltage[int(start_point + edge_time / 2 / sample_time):
                                             int(start_point + edge_time / 2 / sample_time + level_time / sample_time)])

        low_level = min(before_level, after_level)
        high_level = max(before_level, after_level)

        interval = abs(before_level - after_level)
        low_level_perc_edge = low_level + low_level_perc / 100 * interval
        high_level_perc_edge = low_level + high_level_perc / 100 * interval
        edges.loc[idx, "perc_low_level_target"] = low_level_perc_edge
        edges.loc[idx, "perc_high_level_target"] = high_level_perc_edge

        if edge_type == "Rise":  # rising edge:
            direction = 1
        else:
            direction = -1

        # find max voltage
        for deviation, voltage_point in enumerate(wav_voltage[start_point::direction]):
            if voltage_point > high_level_perc_edge:
                edges.loc[idx, "perc_high_voltage"] = high_level
                edges.loc[idx, "perc_high_voltage_thresh"] = voltage_point
                edges.loc[idx, "perc_high_point"] = start_point + direction * deviation
                edges.loc[idx, "perc_high_time"] = wav_time[int(edges.loc[idx, "perc_high_point"])]
                break

        # find mim voltage
        for deviation, voltage_point in enumerate(wav_voltage[start_point::-direction]):
            if voltage_point < low_level_perc_edge:
                edges.loc[idx, "perc_low_voltage"] = low_level
                edges.loc[idx, "perc_low_voltage_thresh"] = voltage_point
                edges.loc[idx, "perc_low_point"] = start_point - direction * deviation
                edges.loc[idx, "perc_low_time"] = wav_time[int(edges.loc[idx, "perc_low_point"])]

                break
        edges.loc[idx, "perc_transition_time"] = abs(
            edges.loc[idx, "perc_high_time"] - edges.loc[idx, "perc_low_time"]) / transition_time_scale
        idx += 1

    return edges


if __name__ == '__main__':
    ## variables ###
    log.info("Staring test with oscilloscope")

    ## control plot
    plot_results = False  # if False, no prin will be executed
    plot_level_edges = True
    plot_perc_edges = True

    # values for edges calculations
    low_level_voltage = 4.5
    high_level_voltage = 11.5
    second_high_level = 9.5
    third_high_level = 20

    low_level_perc = 10
    high_level_perc = 90

    dali_channel = 2


    # DON'T USE TCPIP for communication. Will not be able to able to retreive data
    # address = "TCPIP0::192.168.8.131::inst0::INSTR"
    # address = "VICP::192.168.8.161::INSTR"
    # address = "VICP::192.168.8.214::INSTR"
    address = "USB0::0x05FF::0x1023::LCRY3702N13118::INSTR"


    log.info("Connecting to Oscilloscope!")
    osc = Oscilloscope(address)
    osc.open()

    log.info("Getting waveform...")
    wav_time, wav_voltage = osc.get_waveform(f"C{dali_channel}")

    # Oscilloscope.show_waveform(wav_time, wav_voltage)

    edges = find_edges(wav_time, wav_voltage, low_level_voltage, high_level_voltage, second_high_level,
                       third_high_level, low_level_perc,
                       high_level_perc)

    # print("Level times")
    #
    # for idx, edge in edges.iterrows():
    #     print(
    #         f"Edge: #{idx + 1}, Frame: {edge['frame']}, Type: {edge['edge_type']}, transition time: {edge['level_transition_time']:.03}us")

    if plot_results:
        plt.figure()
        plt.title("Edge search resuls")
        plt.xlabel("Time [s]")
        plt.ylabel("Voltage [V]")
        plt.plot(wav_time, wav_voltage)

        if plot_level_edges:
            plt.plot(edges["level_high_time"], edges["level_high_voltage"], 'r+')
            plt.plot(edges["level_low_time"], edges["level_low_voltage"], 'r+')

        if plot_perc_edges:
            plt.plot(edges["perc_high_time"], edges["perc_high_voltage_thresh"], 'g+')
            plt.plot(edges["perc_low_time"], edges["perc_low_voltage_thresh"], 'g+')
        # show results
        plt.show()

    backward_frame_number = np.max(edges["frame"])

    backward_frame_edges = edges[edges["frame"] == backward_frame_number]
    backward_frame_fall = backward_frame_edges[backward_frame_edges["edge_type"] == "Fall"]
    backward_frame_rise = backward_frame_edges[backward_frame_edges["edge_type"] == "Rise"]
    
    # Last high voltage of signal before first edge in V
    print(f"Last high voltage before first edge: {edges.loc[0, 'perc_high_voltage']:.03}V")

    # 11.5V o 4.5V fall time first falling edge in backward frame
    backframe_first_fall_time_level = backward_frame_edges.iloc[0]["level_transition_time"]
    print(f"First backward frame 11.5V to 4.5V transition: {backframe_first_fall_time_level:.03}us")

    # 4.5V  to 11.5V rise time first rise edge in backward frame
    backframe_first_rise_time_level = backward_frame_edges.iloc[1]["level_transition_time"]
    print(f"First backward frame 4.5V to 11.5V  transition: {backframe_first_rise_time_level:.03}us")

    # last high voltage of signal before last edge
    last_high_voltage = edges.iloc[-1]["perc_high_voltage"]
    print(f"Last high voltage of signal before last edge: {last_high_voltage:.03}V")

    # time between 11.5V to 4.5V in last falling edge in backward frame
    backward_last_fall_time_level = edges.iloc[-2]["level_transition_time"]

    print(f"Last backward frame 11.5V to 4.5V transition: {backward_last_fall_time_level:.03}us")

    # 4.5V  to 11.5V rise time first rise edge in backward frame
    backframe_last_rise_time_level = backward_frame_edges.iloc[-1]["level_transition_time"]
    print(f"Last backward frame 4.5V to 11.5V  transition: {backframe_last_rise_time_level:.03}us")

    # time between 9.5 and 4.5V of first falling edge in backward frame in us
    backward_first_fall_time_level_second = backward_frame_edges.iloc[0]["level_second_transition_time"]
    print(f"First backward frame 9.5V to 4.5V transition: {backward_first_fall_time_level_second:.03}us")

    # time between 4.5V to 9.5 of first rising edge in backward frame in us
    backward_first_rise_time_level_second = backward_frame_edges.iloc[1]["level_second_transition_time"]
    print(f"First backward frame 4.5V to 9.5V transition: {backward_first_rise_time_level_second:.03}us")

    # time between 9.5 and 4.5V of last falling edge in backward frame in us
    backward_last_fall_time_level_second = backward_frame_edges.iloc[-2]["level_second_transition_time"]
    print(f"last backward frame 9.5V to 4.5V transition: {backward_last_fall_time_level_second:.03}us")

    # time between 4.5V to 9.5 of last rising edge in backward frame in us
    backward_last_rise_time_level_second = backward_frame_edges.iloc[-1]["level_second_transition_time"]
    print(f"last backward frame 4.5V to 9.5V transition: {backward_last_rise_time_level_second:.03}us")

    # time between 20 and 4.5V of first falling edge in backward frame in us
    backward_first_fall_time_level_third = backward_frame_edges.iloc[0]["level_third_transition_time"]
    print(f"First backward frame 20V to 4.5V transition: {backward_first_fall_time_level_third:.03}us")

    # time between 4.5V to 9.5 of first rising edge in backward frame in us
    backward_first_rise_time_level_third = backward_frame_edges.iloc[1]["level_third_transition_time"]
    print(f"First backward frame 4.5V to 20V transition: {backward_first_rise_time_level_third:.03}us")

    # time between 90% and 10% first falling edges
    backward_first_fall_time_perc = backward_frame_edges.iloc[0]["perc_transition_time"]
    print(f"First backward frame 90% to 10% fall transition: {backward_first_fall_time_perc:.03}us")

    # time between 10% and 90% first rising edges
    backward_first_rise_time_perc = backward_frame_edges.iloc[1]["perc_transition_time"]
    print(f"First backward frame 10% to 90% rise transition: {backward_first_rise_time_perc:.03}us")

    # time between 90% and 10% last falling edges
    backward_last_fall_time_perc = backward_frame_edges.iloc[-2]["perc_transition_time"]
    print(f"last backward frame 90% to 10% fall transition: {backward_last_fall_time_perc:.03}us")

    # time between 10% and 90% last rising edges
    backward_last_rise_time_perc = backward_frame_edges.iloc[-1]["perc_transition_time"]
    print(f"last backward frame 10% to 90% rise transition: {backward_last_rise_time_perc:.03}us")


    # print stats
    log.info("\n\nLevel transition time statistics")
    backward_min_level_fall_time = np.min(backward_frame_fall["level_transition_time"])
    backward_max_level_fall_time = np.max(backward_frame_fall["level_transition_time"])
    backward_avg_level_fall_time = np.average(backward_frame_fall["level_transition_time"])
    
    print(f"Level: Minimum Fall time: {backward_min_level_fall_time:.03}us")
    print(f"Level: Maximum Fall time: {backward_max_level_fall_time:.03}us")
    print(f"Level: Average Fall time: {backward_avg_level_fall_time:.03}us")

    backward_min_level_rise_time = np.min(backward_frame_rise["level_transition_time"])
    backward_max_level_rise_time = np.max(backward_frame_rise["level_transition_time"])
    backward_avg_level_rise_time = np.average(backward_frame_rise["level_transition_time"])

    print(f"Level: Minimum rise time: {backward_min_level_rise_time:.03}us")
    print(f"Level: Maximum rise time: {backward_max_level_rise_time:.03}us")
    print(f"Level: Average rise time: {backward_avg_level_rise_time:.03}us")

    log.info("\n\nPercentage transition time statistics")
    
    backward_min_perc_fall_time = np.min(backward_frame_fall["perc_transition_time"])
    backward_max_perc_fall_time = np.max(backward_frame_fall["perc_transition_time"])
    backward_avg_perc_fall_time = np.average(backward_frame_fall["perc_transition_time"])

    print(f"perc: Minimum Fall time: {backward_min_perc_fall_time:.03}us")
    print(f"perc: Maximum Fall time: {backward_max_perc_fall_time:.03}us")
    print(f"perc: Average Fall time: {backward_avg_perc_fall_time:.03}us")

    backward_min_perc_rise_time = np.min(backward_frame_rise["perc_transition_time"])
    backward_max_perc_rise_time = np.max(backward_frame_rise["perc_transition_time"])
    backward_avg_perc_rise_time = np.average(backward_frame_rise["perc_transition_time"])

    print(f"perc: Minimum rise time: {backward_min_perc_rise_time:.03}us")
    print(f"perc: Maximum rise time: {backward_max_perc_rise_time:.03}us")
    print(f"perc: Average rise time: {backward_avg_perc_rise_time:.03}us")

    print("End!")
