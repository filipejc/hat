import hat.utils.DataStorage
import numpy as np
import time
import logging
from hat.comms import g4_interface_cli
from hat.drivers import AcPowerMeterInstrument
from hat.drivers import AcSourceInstrument
from hat.drivers import ACDC_load
from hat.utils import utils
import matplotlib.pyplot as plt

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")

if __name__ == "__main__":
    log.info("Starting test")

    # variables
    use_ac_load = True  # to use AC load
    plot = False    # make plot
    compensate_negative = True

    power_on_time = 1  # number of second it takes to start

    ### voltage under test
    voltages = np.arange(230, 230+0.1, 10)
    currents = np.arange(0, 1 + 0.1, 0.1)

    # voltages = [230]
    # currents = np.arange(0, 0.350, 0.01)

    max_current_peak = max(currents) * 2.0  # needs to account with crest factor

    ### test parameters
    time_sleep = 5
    filename = "schreder_flex_app.xls"
    repetitions = 4
    use_flex_app = True

    #### connection strings
    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    g4_com_port = "COM72"
    ac_com_port = "ASRL51::INSTR"
    load_com_port = "ASRL209::INSTR"

    #### connect to instruments and G4
    ac = AcSourceInstrument.AcSourceInstrument(ac_com_port, reset=False)

    if use_ac_load:
        load = ACDC_load.ACDCLoad(load_com_port, reset=False)

    pm = AcPowerMeterInstrument.AcPowerMeterInstrument(USB_connection_string)

    g4 = g4_interface_cli.G4(g4_com_port, interface_type="SERIAL_THREAD")
    if use_flex_app:
        g4.set_prompt("$")
        g4.flex_set_relay(True)
    else:
        g4.set_global_loglevel(4)

    ## create data structure
    data = hat.utils.DataStorage.DataStorage()
    data.open(filename)

    #### turn on ac
    ac.set_voltage_AC(voltages[0])
    ac.set_output_state(True)

    print(f"Waiting {power_on_time} seconds, for device to start")
    time.sleep(power_on_time)

    ### configure load
    if use_ac_load:
        load.set_load_mode("CURR")
        load.clear_protection_status()
        load.set_current_AC(0)
        load.set_peak_current_AC(max_current_peak)
        load.set_output_state(True)

    for voltage in voltages:
        log.info(f"Testing voltage {voltage}")
        ac.set_voltage_AC(voltage)
        time.sleep(time_sleep)

        # set current level
        for current in currents:
            log.info(f"Testing voltage {voltage}V, current {current}")

            if use_ac_load:
                load.set_current_AC(current)

            time.sleep(time_sleep)
            for i in range(repetitions):
                data["repetition"] = i + 1
                log.info(f"Staring repetition {i+1}")
                # read from G4
                if use_flex_app:
                    measuring_data = g4.flex_get_meter_data()
                    g4_voltage = measuring_data["voltage"]
                else:
                    g4_voltage = g4.lwm2m_get_supply_voltage()

                if use_ac_load:
                    if use_flex_app:
                        g4_current = measuring_data["current"]
                    else:
                        g4_current = g4.lwm2m_get_supply_current()

                if use_flex_app:
                    g4_active_power = measuring_data["power"]
                else:
                    g4_active_power = g4.lwm2m_get_active_power()
                # g4_freq = g4.lwm2m_get_frequency()
                # g4_pf = g4.lwm2m_get_power_factor()

                # PM1000
                meas = pm.get_measurement()

                data["Set Voltage [V]"] = voltage
                data["G4 voltage [V]"] = g4_voltage

                if use_ac_load:
                    data["Set Current [A]"] = current
                    data["G4 current [A]"] = g4_current

                data["G4 active power [W]"] = g4_active_power
                # data["G4 frequency [Hz]"] = g4_freq
                # data["G4 power factor"] = g4_pf

                for key, value in meas.items():
                    data[f"PM {key}"] = np.abs(value) if compensate_negative else value

                data.start_next_test()

    data.save()

    if plot:
        plt.plot(data["PM Vrms"], data["G4 voltage [V]"], "-go")
        plt.legend("Voltage")
        plt.xlabel("Reference Voltage [V]")
        plt.ylabel("G4 Voltage [V]")
        plt.title("G4 Voltage test vs Reference Voltage Meter[V]")
        plt.show()

    # teardown
    if use_ac_load:
        load.set_output_state(False)
    log.info("End!")