"""
# Filename:     BFT_1-10V_debug.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   07/11/2022
# Last Update:  07/11/2022
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms.g4_interface_cli import G4
from hat.drivers.MultimeterInstrument import MultimeterInstrument
import time
from hat.utils.DataStorage import DataStorage

if __name__ == '__main__':
    # COM address

    Multimeter_address = ""
    COM_address = "COM72"
    test_values = [0, 1000, 5000, 10000]
    delay_time = 100  # ms
    result = f"1-10v_results_{delay_time}ms.xlsx"
    data = DataStorage()
    data.open(result)

    print("Starting...")
    g4 = G4(COM_address, interface_type="SERIAL_THREAD")
    g4.set_prompt("$")

    # start service
    print("Stopping 1_10v service")
    g4.flex_set_relay(True)
    start_time = time.time()
    data["Stop 1_10v_service Time [s]"] = time.time() - start_time
    g4.flex_set_1_10v_stop()
    time.sleep(5)

    print("Start 1_10v service")
    data["Start 1_10v_service Time [s]"] = time.time() - start_time
    g4.flex_set_1_10v_start()
    time.sleep(1)

    for test_value in test_values:
        print(f"Testing value {test_value}mv")
        data["Set 1_10V time [s]"] = time.time() - start_time
        data["Set 1_10V value [mV]"] = test_value
        g4.flex_set_1_10v_voltage(test_value)

        print(f"Waiting {delay_time}ms...")
        time.sleep(delay_time / 1000.0)

        data["get 1_10V time [s]"] = time.time() - start_time
        voltage = g4.flex_get_1_10v_voltage()
        data["get 1_10V value [mV]"] = voltage
        data["difference [mV]"] = test_value - voltage
        print(f"Voltage at  {voltage}")
        data.start_next_test()

    data.close()
    print("end!")
