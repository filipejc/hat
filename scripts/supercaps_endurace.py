from hat.utils import utils, DataStorage
from hat.drivers.AcSourceInstrument import AcSourceInstrument
from hat.drivers.DC_source import DCSource
from hat.drivers.MultimeterInstrument import MultimeterInstrument
from hat.comms.g4_interface_cli import G4
from hat.drivers.RelayInterface import AdvantechInterface
import time

log = utils.create_logger(__name__)


def set_all_to_key(dictionary, label, value):
    """
    set in all child dictionaries, the same value for the required label

    Parameters
    ----------
    dictionary
    label
    value

    Returns
    -------

    """
    for dev, item in dictionary.items():
        dictionary[dev][label] = value


def apply_all(dictionary, label, func, *value):
    """
    Apply a function to all the same label inside the child dictionaries

    Parameters
    ----------
    dictionary
    label
    func
    value

    Returns
    -------

    """
    for dev, item in dictionary.items():
        func(item[label], *value)


def main():
    # addresses
    ac_addr = "EL11"
    dc_addr = "EL22"

    dc_channel_1 = 1
    dc_channel_2 = 2

    mult_addr = "EL18"
    rel_addr = "USB-4761,BID#0"
    mult_config = "supercaps_mult_config.json"
    rel_config = "supercaps_relay_config.json"

    # configuration
    cycles = 365
    ac_voltage = 230
    ac_freq = 50
    dc_voltage = 24

    wait_after_charge_complete = 20  # seconds
    wait_after_discharge_complete = 30  # seconds
    max_charge_time = 180  # seconds
    max_discharge_time = 180  # seconds
    max_complete_discharge_time = 300  # seconds
    minimum_charge_time = 60  # seconds
    mininum_discharge_time = 10  # seconds
    margin_charge = 5  # seconds
    margin_discharge = 5  # seconds

    confs = {
        "DataLiftN": {
            "datafile": "data/DataLiftN.xlsx",
            "charge_threshold": 4.2,  # charge voltage
            "discharge_threshold": 2.5,  # discharge voltage
            "complete_discharge": 0.2
        },
        "DataLiftZ": {
            "datafile": "data/DataLiftZ.xlsx",
            "charge_threshold": 4.2,  # charge voltage
            "discharge_threshold": 2.5,  # discharge voltage
            "complete_discharge": 0.2
        },
        "MeshNodeN": {
            "datafile": "data/MeshNodeN.xlsx",
            "charge_threshold": 4.4,  # charge voltage
            "discharge_threshold": 1,  # discharge voltage
            "complete_discharge": 0.2
        },
        "MeshNodeZ": {
            "datafile": "data/MeshNodeZ_%Y%m_%H%M.xlsx",
            "charge_threshold": 4.4,  # charge voltage
            "discharge_threshold": 1,  # discharge voltage
            "complete_discharge": 0.2
        }
    }

    # connect to device
    log.info("Creating device interfaces")

    dc = DCSource(dc_addr)
    ac = AcSourceInstrument(ac_addr)
    mult = MultimeterInstrument(mult_addr, mult_config)
    mult.open()
    rel = AdvantechInterface(rel_addr, rel_config)

    mult.configure_measurement_scan_using_conf(speed_nplc=1)
    # configure devices
    ac.set_voltage_AC(ac_voltage)
    ac.set_frequency(ac_freq)
    dc.set_voltage_dc(dc_channel_1, dc_voltage)
    dc.set_voltage_dc(dc_channel_2, dc_voltage)

    # open interfaces and create datafiles
    for label, conf in confs.items():
        # g4 = G4(conf["address"], "SERIAL_THREAD", log_file=conf["logfile"])
        data = DataStorage.DataStorage()
        data.open(conf["datafile"])
        # confs[label]["g4"] = g4
        confs[label]["data"] = data

    for i in range(cycles):
        set_all_to_key(confs, "charged", False)
        set_all_to_key(confs, "discharged", False)
        set_all_to_key(confs, "complete_discharged", False)

        log.info(f"Staring cycle {i}")

        apply_all(confs, "data", (lambda x, v: x.__setitem__("Cycle", v)), i)
        apply_all(confs, "data", (lambda x, v: x.__setitem__("Date/Time", v)), time.asctime())

        # Step: turn off additional load
        log.info("Disconnect additional load!")

        for label, conf in confs.items():
            rel.set_position_by_label(f"{label} off")

        # Step: charging cycle
        log.info("Starting charging cycle")
        start_time = time.time()
        ac.set_output_state(True)
        dc.set_output_state(dc_channel_1, True)
        dc.set_output_state(dc_channel_2, True)

        # Step: wait for charging
        log.info(f"Waiting minimum charging cycle: {minimum_charge_time}")
        time.sleep(minimum_charge_time)

        # Step: wait charging cycle to end
        log.info("Waiting for charging cycle to end")
        while time.time() - start_time < max_charge_time:

            supercap_voltages = mult.make_measurement_scan_using_conf()

            all_charged = True  # all devices charged

            for label, conf in confs.items():
                if conf["charged"]:  # if charged continue
                    continue

                supercap_voltage = supercap_voltages[label]
                log.info(f"{label}: charge voltage {supercap_voltage} V")

                # check if charged
                if supercap_voltage > conf["charge_threshold"]:
                    log.info(f"{label} supercaps charged!")
                    conf["data"]["Charge Time [s]"] = time.time() - start_time
                    conf["charged"] = True
                else:
                    log.info(f"{label}: not charged!")
                    all_charged = False

            if all_charged:
                log.info("All devices charged")
                break

        if time.time() - start_time > max_charge_time:
            raise Exception(f"Could not charge in time")

        last_minimum_charge_time = min([conf["data"]["Charge Time [s]"].values[0] for _, conf in confs.items()])
        minimum_charge_time = last_minimum_charge_time - margin_charge

        # Step: wait a bit after charging cycle is complete
        log.info(f"Waiting {wait_after_charge_complete} seconds for charging to be completed!")
        time.sleep(wait_after_charge_complete)

        # Step: measure voltage
        log.info("Measuring charge voltage...")
        for label, conf in confs.items():
            voltage = mult.get_voltage_dc_by_name(label)
            conf["data"]["Charge Voltage [V]"] = voltage

        # Step: set additional load
        log.info("Start additional load!")

        for label, conf in confs.items():
            rel.set_position_by_label(f"{label} on")

        # Step: start discharge cycle
        log.info("Starting discharge cycle")
        start_time = time.time()
        ac.set_output_state(False)
        dc.set_output_state(dc_channel_1, False)
        dc.set_output_state(dc_channel_2, False)

        # Step: wait time before discharge
        log.info(f"Wait minimum {mininum_discharge_time} s")
        time.sleep(mininum_discharge_time)

        # Step: measure time to discharge
        log.info("Measuring discharge time")
        while time.time() - start_time < max_discharge_time:
            supercap_voltages = mult.make_measurement_scan_using_conf()

            all_discharge = True  # all devices charged

            for label, conf in confs.items():
                if conf["discharged"]:  # if charged continue
                    continue

                supercap_voltage = supercap_voltages[label]
                log.info(f"{label}: discharge voltage {supercap_voltage} V")

                # check if charged
                if supercap_voltage < conf["discharge_threshold"]:
                    log.info(f"{label} supercaps discharged!")
                    conf["data"]["Discharge Time [s]"] = time.time() - start_time
                    conf["data"]["Discharge Voltage [V]"] = supercap_voltage
                    conf["discharged"] = True
                else:
                    log.info(f"{label}: not discharged!")
                    all_discharge = False

            if all_discharge:
                log.info("All devices discharged")
                break

        if time.time() - start_time > max_discharge_time:
            raise Exception(f"Could not discharge in time")

        last_minimum_discharge_time = min([conf["data"]["Discharge Time [s]"].values[0] for _, conf in confs.items()])
        mininum_discharge_time = last_minimum_discharge_time - margin_discharge

        # Step: wait time after discharge
        log.info(f"Waiting {wait_after_discharge_complete} seconds after discharge")
        time.sleep(wait_after_discharge_complete)

        while time.time() - start_time < max_complete_discharge_time:
            supercap_voltages = mult.make_measurement_scan_using_conf()

            all_discharge = True  # all devices charged

            for label, conf in confs.items():
                if conf["complete_discharged"]:  # if charged continue
                    continue

                supercap_voltage = supercap_voltages[label]
                log.info(f"{label}: complete discharge voltage {supercap_voltage} V")

                # check if charged
                if supercap_voltage < conf["complete_discharge"]:
                    log.info(f"{label} supercaps discharged!")
                    conf["data"]["Complete discharge Time [s]"] = time.time() - start_time
                    conf["data"]["Complete Discharge Voltage [V]"] = supercap_voltage
                    conf["complete_discharged"] = True
                else:
                    log.info(f"{label}: not completely discharged!")
                    all_discharge = False

            if all_discharge:
                log.info("All devices discharged")
                break

            time.sleep(10)

        # Step: wait a bit between cycles

        log.info(f"Sleeping 30 seconds!")
        time.sleep(30)

        apply_all(confs, "data", lambda x: x.start_next_test())

    # close all files
    apply_all(confs, "data", lambda x: x.close())
    log.info("Test Completed")


if __name__ == '__main__':
    main()
