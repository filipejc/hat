"""
# Filename:     import_data_to_plm.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   25/06/2021
# Last Update:  25/06/2021
#
# Version:      1.0
# Filetype:     
# Description:  Script to import data into the PLM
#               Uses BOM files provided by Flex and follows procedure from Jean-Philippe
# STATUS:       Beta
# Limitation:   None
"""

import pandas as pd
import os
import numpy as np

# define location
PART_FOLDER = "OWLET IV"
PRODUCT = "OWLET"

"""
Step 1: Export current list of Schreder parts already created
Go to folder, folder content "Schreder Part" and use the "action" -> "Export List to File" -> "Export list to XLSX".

Add exported file to directory.
"""
PART_TYPE = "WCTYPE|wt.part.WTPart|net.schreder.SchrederPart"
PART_NUMBER = ""
PART_PHANTOM = False
PART_TRACE_CODE = 0
PART_GENERIC_TYPE = "standard"
PART_LOCATION = f"/Default/Design/{PART_FOLDER}"
PART_ORGANIZATION = "Schreder"
PART_REVISION = ""
PART_VIEW = "Design"
PART_STATE = "INWORK"
PART_Lifecycle = "Schreder Life Cycle"
PART_Gathering_Part = False
PART_Collapsible = False
PART_Classification = ""
PART_COLOR = "None"
PART_PurchaseSegmentNumber = "S42 - Electronics Components - Other components"
PART_ValidatedByPPLPur = False
PART_ValidatedByGrpPur = False
PART_BOMCalculationGroup = "GEN-Generic"
PART_ValidatedByRD = False

""" Step 2: Import BOM
Import BOM with reference to the created schreder part 

"""
BOM_ACTION = "Add"
BOM_ORGANIZATION_ID = "Schreder"
BOM_Container = f"/wt.inf.container.OrgContainer=Schreder/wt.pdmlink.PDMLinkProduct={PRODUCT}"
BOM_REV = ""
BOM_VIEW = "Design"
BOM_REFDES = ""
BOM_TRACECODE = 0
BOM_FINDNUMBER = ""
BOM_LINENUMBER = ""
BOM_COMPREF = ""
BOM_QtyOpt = 1
BOM_InclusionOpt = ""
BOM_TYPE = "WCTYPE|wt.part.WTPartUsageLink"
BOM_COMP_ID = ""

""" Step 3: prepare Schreder Manufacture Part


"""
SMP_TYPE = "WCTYPE|com.ptc.windchill.suma.part.ManufacturerPart|net.schreder.SchrederManufacturerPart"
SMP_ENDITEM = False
SMP_PHANTOM = False
SMP_TRACE_CODE = 0
SMP_Generic_type = "standard"
SMP_AsmMod = "component"
SMP_Location = f"/Default/Design/{PART_FOLDER}"
SMP_REVISION = ""
SMP_View = "Design"
SMP_STATE = "INWORK"
SMP_LIFECYCLE = "Schreder Life Cycle"
SMP_Source = "buy"
SMP_Gathering = False
SMP_collapsile = False
SMP_CLASS = ""

""" Step 4: prepare the AVL matching between the schreder manufacture part and schreder part

"""
AVL_ACTION = "ADD"
AVL_CONTAINER = ""
AVL_OEM_ORGANIZATION = ""
AVL_OEM_PART_REV = ""
AVL_OEM_VIEW = "Design"
AVL_PART_number = ""
AVL_vendor_org_id = ""
AVL_sourcing_status = ""
AVL_sourcing_context = ""

SOURCING_STATUS_MATCH = {
    "": "Preferred",
    "Alternate": "Approved",
    "Primary alternate": "Preferred",
    "Primary Alternate": "Preferred"
}


def is_empty(item):
    return isinstance(item, float) and np.isnan(item)


MAX_SMP_LEN = 40  # maximum number of letter in SMP code


def make_SMP_name(manufacture_name, manufacture_part_number):
    number = f"{manufacture_part_number} ("
    words_manufacturer = manufacture_name.split()

    second_word = False

    for word in words_manufacturer:
        len_number = len(number)
        if len_number + len(word) < 38:
            if second_word:
                number += " "
            else:
                second_word = True
            number += word
        else:
            break
    number += ")"

    # print(f"name too large: {number}")
    return number


class PLM_loader:
    def __init__(self, directory, schreder_parts_xlsx, schreder_manufacture_parts_xlsx):
        self._directory = directory
        self._schreder_parts_xlsx = schreder_parts_xlsx
        self._schreder_manufacture_parts_xlsx = schreder_manufacture_parts_xlsx
        self._parts = []
        self._manufacure_parts = []
        self._bom = None
        self.load_schreder_parts()
        self.load_schreder_manufacture_parts()

    def load_schreder_parts(self):
        full_filename = os.path.join(self._directory, self._schreder_parts_xlsx)
        if not os.path.exists(full_filename):
            raise Exception(f"Filename: {full_filename} does not exists")

        self._parts = pd.read_excel(full_filename)
        return self._parts

    def get_schreder_parts(self):
        return self._parts

    def load_schreder_manufacture_parts(self):
        full_filename = os.path.join(self._directory, self._schreder_manufacture_parts_xlsx)
        if not os.path.exists(full_filename):
            raise Exception(f"Filename: {full_filename} does not exists")

        self._manufacure_parts = pd.read_excel(full_filename)
        return self._manufacure_parts

    def get_schreder_manufacture_parts(self):
        return self._manufacure_parts

    def load_smp(self, smp_xlsx):
        full_filename = os.path.join(self._directory, smp_xlsx)
        if not os.path.exists(full_filename):
            raise Exception(f"Filename: {full_filename} does not exists")

        self._smp = pd.read_excel(full_filename, skiprows=5)
        return self._smp

    def load_bom(self, original_bom_xlsx):
        full_filename = os.path.join(self._directory, original_bom_xlsx)
        if not os.path.exists(full_filename):
            raise Exception(f"Filename: {full_filename} does not exists")

        self._bom = pd.read_excel(full_filename)
        return self._bom

    def get_bom(self):
        return self._bom

    def in_Schreder_Parts(self, part_name):
        present = part_name in self._parts["Name"].values
        return present

    def in_Schreder_Manufacture_Parts(self, part_name):
        present = part_name in self._manufacure_parts["Number"].values
        return present

    def get_SP_from_name(self, part_name):
        SP = self._parts[self._parts["Name"] == part_name]["Number"].values[0]
        return SP

    def prepare_schreder_part_loading(self, original_bom_xlsx, out_schreder_part_xlsx):
        # loading BOM into memory
        BOM = self.load_bom(original_bom_xlsx)

        BOM_SP_total = BOM.iloc[:, 2].count()  # number of items in BOM that should be a SP
        BOM_SP_unique = len(BOM.iloc[:, 2].unique()) - 1  # number of unique element -1 due to nan
        previous_added_SP = 0  # number of items that were already added to list to upload
        already_on_PLM_SP = 0  # number of items already on PLM

        out = pd.DataFrame()
        index = 0

        for idx, row in BOM.iterrows():
            # print(f"idx: {idx}, row: {row}")
            item_number = row.iloc[2]
            if is_empty(item_number):
                continue
            # calculate name
            part_name = item_number + "-" + row["Description"]

            present = self.in_Schreder_Parts(part_name)
            if present:
                print(f"Part already present: {part_name}")
                already_on_PLM_SP += 1
                continue
            # check if part already in previous file, first row needs to be created.
            part_in_previous = out.shape[0] and np.any(out["Name"] == part_name)
            if part_in_previous:
                print(f"Part {part_name} already added")
                previous_added_SP += 1
                continue

            print(f"Adding part: {part_name}")

            level = row["Level"]
            item_type = row["Item Type"]
            unit = row["UOM"]

            out.loc[index, "Type"] = PART_TYPE
            out.loc[index, "Number"] = PART_NUMBER
            out.loc[index, "Name"] = part_name
            out.loc[index, "End Item"] = False  # top level part is already created
            out.loc[index, "Phantom"] = PART_PHANTOM
            out.loc[index, "Trace Code"] = PART_TRACE_CODE
            out.loc[index, "Generic Type"] = PART_GENERIC_TYPE
            out.loc[index, "Assembly Mode"] = "BOM" if item_type == "MANUFACTURED" else "component"
            out.loc[index, "Location"] = PART_LOCATION
            out.loc[index, "Organization ID"] = PART_ORGANIZATION
            out.loc[index, "Revision"] = PART_REVISION
            out.loc[index, "View"] = PART_VIEW
            out.loc[index, "State"] = PART_STATE
            out.loc[index, "Lifecycle"] = PART_Lifecycle
            out.loc[index, "Source"] = "make" if item_type == "MANUFACTURED" else "buy"
            if unit == "EA":
                unit_str = "ea"
            elif unit.lower() == "ml":
                unit_str = "l"
            else:
                raise Exception(f"Not known unit {unit}")

            out.loc[index, "Default Unit"] = unit_str
            out.loc[index, "Gathering Part"] = PART_Gathering_Part
            out.loc[index, "Collapsible"] = PART_Collapsible
            out.loc[index, "Classification"] = PART_Classification
            out.loc[index, "Color"] = PART_COLOR
            out.loc[index, "PurchaseSegmentNumber"] = PART_PurchaseSegmentNumber
            out.loc[index, "net.schreder.ValidatedByPPLPur"] = PART_ValidatedByPPLPur
            out.loc[index, "net.schreder.ValidatedByGrpPur"] = PART_ValidatedByGrpPur
            out.loc[index, "BOMCalculationGroup"] = PART_BOMCalculationGroup
            out.loc[index, "net.schreder.ValidatedByRD"] = PART_ValidatedByRD

            index += 1

        out.to_excel(os.path.join(self._directory, out_schreder_part_xlsx), index=False)

        items_added = index
        # statistics for cross check
        print(f"Total number of items: {BOM_SP_total}")
        print(f"Unique items: {BOM_SP_unique}")
        print(f"SP already in PLM: {already_on_PLM_SP}")
        print(f"SP already on the list: {previous_added_SP}")
        print(f"Item to add: {items_added}")

        return out

    def prepare_BOM_loading(self, original_bom_xlsx, out_BOM_xlsx, schreder_top_part, use_top_part):
        # loading BOM into memory
        BOM = self.load_bom(original_bom_xlsx)

        out = pd.DataFrame()
        index = 0

        # add end item
        if use_top_part:
            out.loc[index, "Action"] = ""
            out.loc[index, "Level"] = 0
            out.loc[index, "Number"] = schreder_top_part
            out.loc[index, "Organization ID"] = BOM_ORGANIZATION_ID
            out.loc[index, "Container"] = BOM_Container
            out.loc[index, "Revision"] = BOM_REV
            out.loc[index, "View"] = BOM_VIEW
            out.loc[index, "Quantity"] = ""
            out.loc[index, "Unit of Measure"] = ""
            out.loc[index, "Reference Designators"] = BOM_REFDES
            out.loc[index, "Trace Code"] = BOM_TRACECODE
            out.loc[index, "Find Number"] = BOM_FINDNUMBER
            out.loc[index, "Line Number"] = BOM_LINENUMBER
            out.loc[index, "Component Reference"] = BOM_COMPREF
            out.loc[index, "Quantity Option"] = ""
            out.loc[index, "Inclusion Option"] = BOM_InclusionOpt
            out.loc[index, "Type"] = ""
            out.loc[index, "Component Id"] = BOM_COMP_ID
            out.loc[index, "BOMLineComment"] = ""

            # increase line
            index += 1

            # Level adder
            level_adder = 1
        else:
            level_adder = 0  # add zero if not adding a

        # add other items
        for idx, row in BOM.iterrows():
            # print(f"idx: {idx}, row: {row}")
            item_number = row.iloc[2]
            if is_empty(item_number):
                continue
            # calculate name
            part_name = item_number + "-" + row["Description"]

            present = self.in_Schreder_Parts(part_name)
            if not present:
                print("Part not create, ignoring just for testing")
                # raise Exception(f"Part not yet create {part_name}")
                # continue

            print(f"Adding part: {part_name}")

            level = int(row["Level"]) + level_adder  # add one extra level if needed
            item_type = row["Item Type"]
            unit = row["UOM"]
            schreder_part = self.get_SP_from_name(part_name)

            out.loc[index, "Action"] = "" if level == 0 else BOM_ACTION
            out.loc[index, "Level"] = level
            out.loc[index, "Number"] = schreder_part
            out.loc[index, "Organization ID"] = BOM_ORGANIZATION_ID
            out.loc[index, "Container"] = BOM_Container
            out.loc[index, "Revision"] = BOM_REV
            out.loc[index, "View"] = BOM_VIEW

            if unit == "EA":
                unit_str = "ea"
            elif unit.lower() == "ml":
                unit_str = "l"
            else:
                raise Exception(f"Not known unit {unit}")

            qty = float(row["Qty"])

            if unit_str == "l":
                qty = f"{qty / 1000:0.4f}"

            out.loc[index, "Quantity"] = 1 if level == level_adder else qty
            out.loc[index, "Unit of Measure"] = "ea" if level == level_adder else unit_str
            out.loc[index, "Reference Designators"] = BOM_REFDES
            out.loc[index, "Trace Code"] = BOM_TRACECODE
            out.loc[index, "Find Number"] = BOM_FINDNUMBER
            out.loc[index, "Line Number"] = BOM_LINENUMBER
            out.loc[index, "Component Reference"] = BOM_COMPREF
            out.loc[index, "Quantity Option"] = "" if level == 0 else BOM_QtyOpt

            out.loc[index, "Inclusion Option"] = BOM_InclusionOpt
            out.loc[index, "Type"] = "" if level == 0 else BOM_TYPE
            out.loc[index, "Component Id"] = BOM_COMP_ID

            if level != level_adder:
                pos = row["Position"]
                refdes = row["Ref Des"]
                bom_line_comment = f"POS = {pos}"
                if not is_empty(refdes):
                    bom_line_comment += f" / RefDes = {refdes}"

            else:
                bom_line_comment = ""
            out.loc[index, "BOMLineComment"] = bom_line_comment

            index += 1
            print(f"Part already present: {part_name}")

        out.to_excel(os.path.join(self._directory, out_BOM_xlsx), index=False)
        return out

    def load_suppliers(self, suppliers_xlsx):
        suppliers = pd.read_excel(os.path.join(self._directory, suppliers_xlsx))
        self._suppliers = suppliers[suppliers["State"] != "Canceled"]
        return suppliers

    def get_suppliers(self, supplier):
        supplier_name = []
        for supplier_idx, supplier_row in self._suppliers.iterrows():
            if supplier.lower() in supplier_row["Name"].lower():
                supplier_name.append(supplier_row["Name"])

        if len(supplier_name) == 1:
            name = supplier_name[0]
        elif len(supplier_name) > 1:
            raise Exception(f"Supplier name: {supplier_name}")
        else:
            name = "OPEN MANUFACTURER"
        return name

    def prepare_SMP_loading(self, original_bom_xlsx, out_SMP_xlsx, suppliers_xlsx, ignore_previous=True):
        self.load_suppliers(suppliers_xlsx)

        # loading BOM into memory
        BOM = self.load_bom(original_bom_xlsx)

        total_smp = BOM.shape[0]
        total_unique_smp = len(BOM["MFR Part Number"].unique())
        already_present_PLM = 0
        already_present_smp = 0
        added_smp = 0

        out = pd.DataFrame()
        index = 0

        for idx, row in BOM.iterrows():
            # print(f"idx: {idx}, row: {row}")
            item_number = row.iloc[2]
            if not is_empty(item_number):
                last_valid_item_number = item_number

            mfg = row["MFR Name"]
            mfg_pn = row["MFR Part Number"]
            # calculate name
            number = make_SMP_name(mfg, mfg_pn)

            present = not ignore_previous and self.in_Schreder_Manufacture_Parts(number)
            if present:
                print(f"Continuing: Schreder Manufacture Part: {number} already present...")
                already_present_PLM += 1
                continue

            if out.shape[0] and np.any(out["Number"] == number):
                print(f"Part is already on list to add: {number}")
                already_present_smp += 1
                continue

            print(f"Creating:   Schreder Manufacture Part: {number} not present...")
            added_smp += 1

            # level = int(row["Level"])
            # item_type = row["Item Type"]
            if not is_empty(item_number):
                last_unit = row["UOM"]
                last_name = row["Description"]

            out.loc[index, "Type"] = SMP_TYPE
            out.loc[index, "Number"] = number
            out.loc[index, "Name"] = last_name
            out.loc[index, "End Item"] = SMP_ENDITEM
            out.loc[index, "Phantom"] = SMP_PHANTOM
            out.loc[index, "Trace Code"] = SMP_TRACE_CODE
            out.loc[index, "Generic Type"] = SMP_Generic_type
            out.loc[index, "Assembly Mode"] = SMP_AsmMod
            out.loc[index, "Location"] = SMP_Location
            out.loc[index, "Organization ID"] = self.get_suppliers(mfg)  # TODO: use list of approved manufactures
            out.loc[index, "Revision"] = SMP_REVISION
            out.loc[index, "View"] = SMP_View
            out.loc[index, "State"] = SMP_STATE
            out.loc[index, "Lifecycle"] = SMP_LIFECYCLE
            out.loc[index, "Source"] = SMP_Source
            if last_unit == "EA":
                unit_str = "ea"
            elif last_unit.lower() == "ml":
                unit_str = "l"
            else:
                raise Exception(f"Not known unit {last_unit}")

            out.loc[index, "Default Unit"] = unit_str
            out.loc[index, "Gathering Part"] = SMP_Gathering
            out.loc[index, "Collapsible"] = SMP_collapsile
            out.loc[index, "Classification"] = SMP_CLASS

            index += 1

        out.to_excel(os.path.join(self._directory, out_SMP_xlsx), index=False)

        print(f"Total SMP {total_smp}")
        print(f"Total Unique SMP {total_unique_smp}")
        print(f"Already present on PLM {already_present_PLM}")
        print(f"Already present SMP {already_present_smp}")
        print(f"Added SMP {added_smp}")

        return out

    def get_SMP_from_name(self, smp):
        return self._smp[self._smp["Number"] == smp]

    def prepare_AVL_loading(self, original_bom_xlsx, out_AVL_xlsx):
        # loading BOM into memory
        BOM = self.load_bom(original_bom_xlsx)

        out = pd.DataFrame()
        index = 0

        for idx, row in BOM.iterrows():
            # print(f"idx: {idx}, row: {row}")
            item_number = row.iloc[2]
            if not is_empty(item_number):
                last_valid_item_number = item_number
                last_unit = row["UOM"]
                last_name = row["Description"]

            mfg = row["MFR Name"]
            mfg_pn = row["MFR Part Number"]
            # calculate name
            number = make_SMP_name(mfg, mfg_pn)

            part_name = last_valid_item_number + "-" + last_name

            present = self.in_Schreder_Parts(part_name)
            if not present:
                print(f"Part not create, ignoring just for testing")
                # raise Exception(f"Part not yet create {part_name}")
                continue

            if out.shape[0] and np.any(number == out["Manufacturer part number"]):
                print(f"Part already present {number}")
                continue

            print(f"Adding part: {part_name}")

            # level = int(row["Level"])
            # item_type = row["Item Type"]
            # unit = row["UOM"]
            schreder_part = self.get_SP_from_name(part_name)

            smp = self.get_SMP_from_name(number)
            if not smp.shape[0]:
                print(f"Part number {number} not found!")
                continue
            # TODO: only export SMP before adding in previous step to know which ones are not associated with SP
            present = self.in_Schreder_Manufacture_Parts(smp["Number"].values[0])
            # if present:
            #     print(f"Continuing: Schreder Manufacture Part: {smp['Number']} already present...")
            #     continue
            # else:
            #     print(f"Creating:   Schreder Manufacture Part: {smp['Number']} not present...")

            out.loc[index, "Action"] = AVL_ACTION
            out.loc[index, "OEM part number"] = schreder_part
            out.loc[index, "OEM container"] = AVL_CONTAINER
            out.loc[index, "OEM Organization ID"] = AVL_OEM_ORGANIZATION
            out.loc[index, "OEM part revision"] = AVL_OEM_PART_REV
            out.loc[index, "OEM part view"] = AVL_OEM_VIEW
            out.loc[index, "Manufacturer part number"] = number
            out.loc[index, "Manufacturer organization ID"] = smp["Organization ID"].iloc[0]

            if is_empty(row["MFG Comments"]):
                sourcing_status = "Preferred"
            else:
                sourcing_status = SOURCING_STATUS_MATCH[row["MFG Comments"]]

            out.loc[index, "Manufacturer sourcing status"] = sourcing_status
            out.loc[index, "Vendor part number"] = AVL_PART_number
            out.loc[index, "Vendor organization ID"] = AVL_vendor_org_id  # TODO: use list of approved manufactures
            out.loc[index, "Vendor sourcing status"] = AVL_sourcing_status
            out.loc[index, "Sourcing context"] = AVL_sourcing_status
            index += 1

        out.to_excel(os.path.join(self._directory, out_AVL_xlsx), index=False)
        return out


if __name__ == "__main__":
    # stage
    CREATE_NEW_SCHREDER_PARTS = False
    BOM_IMPORT = False
    SMP_CREATION = False
    AVL_MATCHING = True

    # Configurations
    use_top_part = False
    schreder_part_top_level = "02-47-237"

    # label = "NEMA_LTE"
    # label = "NEMA_MESH"
    # label = "LUMA_NBIOT"
    label = "LUMA_MESH"

    # variables
    directory = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\06_Factory\09_PLM_import\Files\Reference_files"

    # original_BOM = r"NEMA LTE_TLA0010299R000_V2.xlsx"
    # original_BOM = r"NEMA MESH_TLA0010300R000_V2_eddited.xlsx"
    original_BOM = r"LUMA MESH__TLA0010302R000_V2.xlsx"
    # original_BOM = "LUMA NBIOT_TLA0010301R000_V4.xlsx"

    Schreder_Parts = r"SchrederParts.xlsx"
    suppliers_xlsx = "Suppliers.xlsx"
    Schreder_Manufacture_Parts = r"SchrederManufactureParts.xlsx"

    # output files
    output_new_schreder_parts_xlsx = label + "_10_New_Schreder_Parts.xlsx"
    out_new_bom_xlsx = label + "_40_BOM.xlsx"
    out_new_SMP_xlsx = label + "_60_SMP.xlsx"
    out_new_AVL_xlsx = label + "_80_AVL.xlsx"

    print("Staring processing!")
    loader = PLM_loader(directory, Schreder_Parts, Schreder_Manufacture_Parts)

    parts = loader.get_schreder_parts()
    # print(parts)

    loader.load_bom(original_BOM)
    bom = loader.get_bom()
    # print(bom)

    if CREATE_NEW_SCHREDER_PARTS:
        print("Creating new schreder parts to be loaded!")
        loader.prepare_schreder_part_loading(original_BOM, output_new_schreder_parts_xlsx)
        print("Notice, small processing is necessary in the excel file")

    if BOM_IMPORT:
        print(f"Creating new BOM file to be loaded!")
        loader.prepare_BOM_loading(original_BOM, out_new_bom_xlsx, schreder_part_top_level, use_top_part)
        print("Notice, small processing is necessary in the excel file")

    if SMP_CREATION:
        print(f"Creating new SMP file to be loaded!")
        loader.prepare_SMP_loading(original_BOM, out_new_SMP_xlsx, suppliers_xlsx)
        print("Notice, small processing is necessary in the excel file")

    if AVL_MATCHING:
        print(f"Creating new AVL file to be loaded!")
        loader.load_smp(out_new_SMP_xlsx)
        loader.prepare_AVL_loading(original_BOM, out_new_AVL_xlsx)
        print("Notice, small processing is necessary in the excel file")

    print("end!")
