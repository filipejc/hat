"""
# Filename:     BOM_load.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   17/02/2022
# Last Update:  17/02/2022
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import pickle

import numpy as np
import pandas as pd
import os
from dataclasses import dataclass, field
from enum import Enum, auto
from typing import List, Dict
import copy


class PartType(Enum):
    MANUFACTURED = auto()
    PURCHASED = auto()
    UNKNOWN = auto()


@dataclass
class Manufacturer_Part:
    manufacture: str
    manufacture_part_number: str

    def __eq__(self, other):
        return self.manufacture == other.manufacture and self.manufacture_part_number == other.manufacture_part_number

    def __post_init__(self):
        try:
            self.manufacture = self.manufacture.split(" ")[0].split("|")[0].upper()
        except AttributeError as e:
            self.manufacture = "NA"

        try:
            self.manufacture_part_number = self.manufacture_part_number.split("|")[0]
        except AttributeError as e:
            self.manufacture_part_number = "NA"

    def export(self):
        return pd.Series({
            "Manufacture": self.manufacture,
            "Manufacture Part Number": self.manufacture_part_number
        })


@dataclass
class Part:
    part_number: str
    description: str
    part_type: PartType
    imi_part_number: str = "NA"
    manufacture_parts: List[Manufacturer_Part] = field(default_factory=list)
    parameters: List[str] = field(default_factory=list)

    def __post_init__(self):
        self.parameters = self.description.split(",")

    def add_manufacture_part(self, part: Manufacturer_Part):
        self.manufacture_parts.append(part)

    def __eq__(self, other):
        return self.part_number == other.part_number

    def export(self):
        values = {
            "IMI Part Number": self.imi_part_number,
            "Part Number": self.part_number,
            "Description": self.description
        }
        for i, parameter in enumerate(self.parameters):
            values[f"Parameter {i + 1}"] = parameter
        return values


@dataclass
class BOM_Line:
    part: Part
    qty: int
    reference_designator: List[str]

    def export(self):
        data = self.part.export()
        data["QTY"] = self.qty
        data["Ref des"] = ",".join(self.reference_designator)

        return data


@dataclass
class PCBA:
    name: str
    BOM: List[BOM_Line] = field(default_factory=list)

    def add_bom_line(self, bom_line: BOM_Line):
        self.BOM.append(bom_line)

    def load_altium_bom(self, filename: str):
        print(f"Loading altium BOM: {filename}")
        data = pd.read_csv(filename)

        for idx, row in data.iterrows():
            if not row["Description"] or isinstance(row["Description"], float) and np.isnan(
                    row["Description"]):  # for empty lines
                continue

            # create part
            part = Part(row["Part Number"],
                        row["Description"],
                        PartType.UNKNOWN)

            # create manufacture part
            manufacture_part = Manufacturer_Part(row["MFR_CONSOLIDATION"], row["MPN_CONSOLIDATION"])
            part.add_manufacture_part(manufacture_part)

            # create bom line
            try:
                bom_line = BOM_Line(part, row["Quantity"], list(map(lambda x: x.strip(), row["Designator"].split(","))))
            except Exception as e:
                print(f"error: {e}")
            self.add_bom_line(bom_line)

        print("load completed!")

    def load_altium_bom_new(self, filename: str):
        print(f"Loading altium BOM: {filename}")
        data = pd.read_csv(filename)

        for idx, row in data.iterrows():
            if not row["Description"] or isinstance(row["Description"], float) and np.isnan(
                    row["Description"]):  # for empty lines
                continue

            # create part
            part = Part(row["Comment"],  # comment represents the part number
                        row["Description"],
                        PartType.UNKNOWN)

            # create manufacture part
            for i in range(1, 6):
                if pd.isnull(row[f"MFG_{i}"]) and pd.isnull(row[f"MPN_{i}"]):
                    continue
                manufacture_part = Manufacturer_Part(row[f"MFG_{i}"], row[f"MPN_{i}"])
                part.add_manufacture_part(manufacture_part)

            # create bom line
            try:
                bom_line = BOM_Line(part, row["Quantity"], list(map(lambda x: x.strip(), row["Designator"].split(","))))
            except Exception as e:
                print(f"error: {e}")
            self.add_bom_line(bom_line)

        print("load completed!")

    def compare_by_reference_designator(self, other, filename):
        """ compare two PCBA BOMs """
        """
        Options:
        - present on A but not on B => done
        - present on B but not on A
        - present on A and B but different quantities and/or reference designator => done
        - present on A and B equally => done
        - present on A and B but different manufacture part number => done

        """
        found_ref_des_B = set()

        columns = ['Ref Des', 'Present on A', 'Present on B', 'Present on both',
                   'Part number A', 'Part number B', 'Part number equal', 'Part number equal but last 4 digits',
                   'Description A', 'Description B', 'Description equal',
                   'Manufacture 1 A', 'Manufacture 1 B',
                   'Manufacture 1 PN A', 'Manufacture 1 PN B', 'Manufacture 1 PN equal',
                   'Manufacture 2 A', 'Manufacture 2 B',
                   'Manufacture 2 PN A', 'Manufacture 2 PN B', 'Manufacture 2 PN equal',
                   'Manufacture 3 A', 'Manufacture 3 B',
                   'Manufacture 3 PN A', 'Manufacture 3 PN B', 'Manufacture 3 PN equal',
                   'Manufacture 4 A', 'Manufacture 4 B',
                   'Manufacture 4 PN A', 'Manufacture 4 PN B', 'Manufacture 4 PN equal',
                   'Manufacture 5 A', 'Manufacture 5 B',
                   'Manufacture 5 PN A', 'Manufacture 5 PN B', 'Manufacture 5 PN equal',
                   ]

        data = pd.DataFrame(columns=columns)
        other_pcba = copy.deepcopy(other)  # copy other pcba to be able to modify without affecting object

        idx = 0
        # iterate over self to check if present on A
        for bom_line_A in self.BOM:
            BOM_A_ref_designators = bom_line_A.reference_designator
            for BOM_A_ref_des in BOM_A_ref_designators:
                not_present_on_B = True
                print(f"Processing ref des {BOM_A_ref_des}")

                part_a = bom_line_A.part

                data.loc[idx, "Ref Des"] = BOM_A_ref_des
                data.loc[idx, "Present on A"] = True
                data.loc[idx, "Part number A"] = part_a.part_number
                data.loc[idx, "Description A"] = part_a.description

                for i, manufacture_part in enumerate(part_a.manufacture_parts):
                    data.loc[idx, f"Manufacture {i + 1} A"] = manufacture_part.manufacture
                    data.loc[idx, f"Manufacture {i + 1} PN A"] = manufacture_part.manufacture_part_number

                # search reference designator

                # search by part number on other
                for bom_line_B in other_pcba.BOM:
                    for BOM_B_ref_des in bom_line_B.reference_designator:
                        if BOM_A_ref_des == BOM_B_ref_des:
                            part_b = bom_line_B.part
                            not_present_on_B = False
                            print("Reference designator present on A and B")
                            break
                    if not not_present_on_B:
                        break

                if not_present_on_B:
                    # not present on B
                    data.loc[idx, "Present on B"] = False
                    data.loc[idx, "Present on both"] = False
                else:
                    # present on B
                    data.loc[idx, "Present on B"] = True
                    data.loc[idx, "Present on both"] = True

                    # store found ref des
                    found_ref_des_B.add(BOM_B_ref_des)

                    # part B
                    data.loc[idx, "Part number B"] = part_b.part_number
                    data.loc[idx, "Description B"] = part_b.description

                    # find on B
                    matched_PN_B = set()

                    for i in range(1, 6):  # compare from 1 to 5 manufactures.
                        # we need to compare manufacture components even if not in order.
                        manufacture_A = data.loc[idx, f"Manufacture {i} A"]
                        manufacture_PN_A = data.loc[idx, f"Manufacture {i} PN A"]

                        if pd.isnull(manufacture_A) and pd.isnull(manufacture_PN_A):
                            print("End of manufactures on A")
                            break

                        data.loc[idx, f"Manufacture {i} PN equal"] = False

                        # check if project
                        for j, mfg_B in enumerate(part_b.manufacture_parts):
                            manufacture_B = mfg_B.manufacture
                            manufacture_PN_B = mfg_B.manufacture_part_number

                            if pd.isnull(manufacture_B) and pd.isnull(manufacture_PN_A):
                                print("End of manufactures on B")
                                break

                            if manufacture_A == manufacture_B and manufacture_PN_A == manufacture_PN_B:
                                matched_PN_B.add(j)
                                print(f"Found on manufacture B index: {j}")
                                data.loc[idx, f"Manufacture {i} B"] = manufacture_B
                                data.loc[idx, f"Manufacture {i} PN B"] = manufacture_PN_B
                                data.loc[idx, f"Manufacture {i} PN equal"] = True
                                break

                    i = 1
                    for j, mfg_B in enumerate(part_b.manufacture_parts):
                        if manufacture_B == "NA" and manufacture_PN_B == "NA":
                            continue
                        if j in matched_PN_B:
                            continue

                        manufacture_B = mfg_B.manufacture
                        manufacture_PN_B = mfg_B.manufacture_part_number

                        data.loc[idx, f"Manufacture {i} B not match"] = manufacture_B
                        data.loc[idx, f"Manufacture {i} PN B not match"] = manufacture_PN_B
                        i += 1

                    # Compare part numbers
                    data.loc[idx, "Part number equal"] = part_a.part_number == part_b.part_number
                    data.loc[idx, "Part number equal but last 4 digits"] = part_a.part_number[
                                                                           :-4] == part_b.part_number[:-4]

                    # Compare description
                    data.loc[idx, "Description equal"] = part_a.description == part_b.description

                idx += 1

            # search BOM B
        for bom_line_B in other_pcba.BOM:
            BOM_B_ref_designators = bom_line_B.reference_designator
            for BOM_B_ref_des in BOM_B_ref_designators:
                if BOM_B_ref_des in found_ref_des_B:
                    print(f"Reference designator {BOM_B_ref_des} already found")
                    continue

                # component not present on A
                part_b = bom_line_B.part
                print(f"Reference designator {BOM_B_ref_des} only found on B")

                data.loc[idx, "Ref Des"] = BOM_B_ref_des
                data.loc[idx, "Present on A"] = False
                data.loc[idx, "Present on B"] = True
                data.loc[idx, "Present on both"] = False

                data.loc[idx, "Part number B"] = part_b.part_number
                data.loc[idx, "Description B"] = part_b.description
                data.loc[idx, "Manufacture B"] = part_b.manufacture_parts[0].manufacture
                data.loc[idx, "Manufacture PN B"] = part_b.manufacture_parts[0].manufacture_part_number

                idx += 1

        data_file = pd.ExcelWriter(filename)
        data.to_excel(data_file, "Complete")

        # not present on both board
        res_not_present_on_both = data[data["Present on both"] == False]
        res_not_present_on_both.to_excel(data_file, "Complete")

        # different part number
        res_diff_PN = data[data["Part number equal"] == False]
        res_diff_PN.to_excel(data_file, "Different PN")

        # different part number but last 4
        res_diff_PN_but_4_last = data[data["Part number equal but last 4 digits"] == False]
        res_diff_PN_but_4_last.to_excel(data_file, "Different PN but last 4 digits")

        # description different on same part number
        res_same_PN = data[data["Part number equal"] == True]

        res_diff_description = res_same_PN[res_same_PN["Description equal"] == False]
        res_diff_description.to_excel(data_file, "Description Different")

        # check manufactures
        for i in range(1, 6):
            res_manufacture = res_same_PN[res_same_PN[f"Manufacture {i} PN equal"] == False]
            res_manufacture.to_excel(data_file, f"Manufacture {i} PN different")

        #
        try:
            res_not_match_manufacture = res_same_PN[pd.notnull(res_same_PN["Manufacture 1 B not match"])]
            res_not_match_manufacture.to_excel(data_file, f"Manufacture not match")
        except Exception as e:
            print(f"Error: {e}")

        # save file
        data_file.save()

        return

    def compare_by_partnumber(self, other, filename):
        """ compare two PCBA BOMs """
        """
        Options:
        - present on A but not on B => done
        - present on B but not on A
        - present on A and B but different quantities and/or reference designator => done
        - present on A and B equally => done
        - present on A and B but different manufacture part number => done
        
        """
        data = pd.DataFrame()
        other_pcba = copy.deepcopy(other)  # copy other pcba to be able to modify without affecting object

        idx = 0
        # iterate over self to check if present on A
        for line in self.BOM:
            data.loc[idx, "part number"] = line.part.part_number
            data.loc[idx, "Description"] = line.part.description
            data.loc[idx, "ref des"] = ", ".join(line.reference_designator)

            print(line)
            not_present_on_B = True

            # search by part number on other
            for other_line in other_pcba.BOM:
                if line.part == other_line.part:
                    print("Part number present on A and B")
                    data.loc[idx, "Result"] = f"Part Number {line.part.part_number} present on A and present on B"
                    data.loc[
                        idx, "Quantity"] = f"A and B same quantities" if line.qty == other_line.qty else f"Different quantities: A={line.qty}, B={other_line.qty}"
                    data.loc[
                        idx, "Ref des"] = f"A and B same Ref Des" if line.reference_designator == other_line.reference_designator \
                        else f"Different Ref Des: A={line.reference_designator}, B={other_line.reference_designator}"

                    # check same manufacture
                    not_same_manufacture = True

                    for A_manufacture_part in line.part.manufacture_parts:
                        for B_manufacture_part in other_line.part.manufacture_parts:
                            if A_manufacture_part == B_manufacture_part:
                                print("Same Manufacture")
                                not_same_manufacture = False
                                break

                        if not not_same_manufacture:
                            break

                    if not_same_manufacture:
                        data.loc[idx, "Manufacture"] = "Manufacture A and B are different"
                    else:
                        data.loc[idx, "Manufacture"] = "Manufacture A and B are equal"

                    not_present_on_B = False

                    # remove line
                    other_pcba.BOM.remove(other_line)

                    break

            if not_present_on_B:
                print("Part number not present B")
                data.loc[idx, "Result"] = f"Part Number {line.part.part_number} present on A but not present on B"

            idx += 1

        for other_line in other_pcba.BOM:
            data.loc[idx, "part number"] = other_line.part.part_number
            data.loc[idx, "Description"] = other_line.part.description
            data.loc[idx, "ref des"] = ", ".join(other_line.reference_designator)

            data.loc[idx, "Result"] = f"Part Number {other_line.part.part_number} present on B but not present on A"
            data.loc[idx, "Manufacture"] = f"Manufacture B is {other_line.part.manufacture_parts[0].manufacture}" \
                                           f" with part number {other_line.part.manufacture_parts[0].manufacture_part_number}"
            idx += 1

        data.to_excel(filename)


@dataclass
class SKU:
    sku_name: str
    pcba_list: Dict[str, PCBA] = field(default_factory=dict)

    def add_pcba(self, pcba: PCBA):
        if pcba.name in self.pcba_list.keys():
            print(f"PCBA: {pcba} already in list")
        else:
            self.pcba_list[pcba.name] = pcba


sku_list = ["DataLift Z", "DataLift N", "MeshNode N", "MeshNode Z"]

sku_list_mapping = {
    "DataLift Z Goldcap": ["DataLift Z"],
    "DataLift Z Comm": ["DataLift Z"],
    "NEMA Meter": ["MeshNode N", "DataLift N"],
    "MeshNode Z Comm": ["MeshNode Z"],
    "NEMA Power": ["MeshNode N", "DataLift N"],
    "DataLift N Goldcap": ["DataLift N"],
    "MeshNode N Comm": ["MeshNode N"],
    "DataLift N Comm": ["DataLift N"],
}

sheet_name_mapping = {
    "LUMA_GOLDCAP": "DataLift Z Goldcap",
    "LUMA_NB": "DataLift Z Comm",
    "METER": "NEMA Meter",
    "LUMA_MESH": "MeshNode Z Comm",
    "POWER": "NEMA Power",
    "NEMA_GOLDCAP": "DataLift N Goldcap",
    "NEMA_MESH": "MeshNode N Comm",
    "NEMA_LTE": "DataLift N Comm"
}

MANUFACTURER_COLUMN = "MFR Name"
MANUFACTURER_PART_COLUMN = "MFR Part Number"
PART_COLUMN = "Item\x30Number"  # incorrect
PART_COLUMN_IDX = 2
DESCRIPTION_COLUMN = "Description"
PART_TYPE_COLUMN = "Item Type"
QUANTITY_COLUMN = "Qty"
REFERENCE_DESIGNATOR_COLUMN = "Ref Des"


@dataclass
class OWLET_IV:
    part_list: List[Part] = field(default_factory=list)
    manufacture_part_list: List[Manufacturer_Part] = field(default_factory=list)
    sku_list: Dict[str, SKU] = field(default_factory=dict)
    pcba_list: Dict[str, PCBA] = field(default_factory=dict)

    def get_part(self, part_number: str):
        for part in self.part_list:
            if part.part_number == part_number:
                return part
        return False

    def add_part(self, part: Part):
        if part in self.part_list:
            raise Exception(f"Part: {part} already in list")
        else:
            self.part_list.append(part)

    def add_manufacture_part(self, manufacture_part: Manufacturer_Part):
        if manufacture_part in self.manufacture_part_list:
            print(f"Manufacture Part: {manufacture_part} already in list")
        else:
            self.manufacture_part_list.append(manufacture_part)

    def add_sku(self, sku: SKU):
        if sku.sku_name in self.sku_list.keys():
            print(f"SKU: {sku} already in list")
        else:
            self.sku_list[sku.sku_name] = sku

    def add_pcba_to_sku(self, sku_name: str, pcba: PCBA):
        self.sku_list[sku_name].add_pcba(pcba)

    def load_flex_bom(self, dirname, filename):
        complete_filename = os.path.join(dirname, filename)
        reader = pd.ExcelFile(complete_filename)

        for sku_name in sku_list:
            sku = SKU(sku_name)
            self.add_sku(sku)

        for sheet_name in reader.sheet_names:
            print(f"Sheet Name: {sheet_name}")
            board_name = sheet_name_mapping[sheet_name]

            print(f"Board Name: {board_name}")
            pcba_to_sku_list = sku_list_mapping[board_name]

            # create bom
            pcba = PCBA(board_name)
            self.pcba_list[board_name] = pcba

            print(f"SKU list: {sku_list}")
            for sku_name in pcba_to_sku_list:
                self.add_pcba_to_sku(sku_name, pcba)

            data = reader.parse(sheet_name)
            for idx, row in data.iterrows():
                # print(row)
                part_number = row[PART_COLUMN_IDX]

                if isinstance(part_number, str):  # if string it is a part number
                    # print(f"Creating part for {part_number}")
                    part = self.get_part(part_number)
                    if not part:
                        part = Part(row[PART_COLUMN_IDX], row[DESCRIPTION_COLUMN], PartType[row[PART_TYPE_COLUMN]])
                        self.add_part(part)

                    # bom line calculation
                    ref_des = row[REFERENCE_DESIGNATOR_COLUMN]
                    ref_des_list = ref_des.split(",") if isinstance(ref_des, str) else []
                    bom_line = BOM_Line(part, len(ref_des_list), ref_des_list)
                    pcba.add_bom_line(bom_line)

                manufacture_part = Manufacturer_Part(row[MANUFACTURER_COLUMN], row[MANUFACTURER_PART_COLUMN])

                if manufacture_part not in part.manufacture_parts:
                    part.add_manufacture_part(manufacture_part)

                if manufacture_part not in self.manufacture_part_list:
                    self.add_manufacture_part(manufacture_part)

        return owlet_iv

    def save_to_file(self, filename: str):
        print(f"Loading Owlet IV from file: {filename}")
        pickle.dump(self, open(filename, "wb"))

    @staticmethod
    def load_from_file(filename: str):
        print(f"Load filename: {filename}")
        return pickle.load(open(filename, "rb"))

    def export_parts(self, filename: str):
        print(f"Exporting Parts to filename: {filename}")

        data = pd.DataFrame()

        for part in self.part_list:
            data = data.append(part.export(), ignore_index=True, sort=False)

        data.to_excel(filename)

    def export_manufacture_parts(self, filename: str):
        print(f"Exporting Manufacturing parts")
        data = pd.DataFrame()

        for part in self.part_list:
            for manufacture_part in part.manufacture_parts:
                row = pd.Series(part.export())
                row = row.append(manufacture_part.export())
                data = data.append(row, ignore_index=True)

        data.to_excel(filename)

    def load_imi_part_numbers(self, filename: str):
        print(f"Import IMI part number {filename}")

        data = pd.read_excel(filename, "Materials List", skiprows=22)
        data.rename(columns={"`": "IMI part numbers"}, inplace=True)
        for idx, row in data.iterrows():
            imi_part_number = row["IMI part numbers"]
            flex_part_number = row["CUSTOMER PN"]
            self.set_imi_part_number(imi_part_number, flex_part_number)
        print(data)

    def set_imi_part_number(self, imi_part_number: str, flex_part_number: str):
        for part in self.part_list:
            if part.part_number == flex_part_number:
                part.imi_part_number = imi_part_number
                return
        print(f"IMI Part number not set: {imi_part_number}")

    def export_single_page_BOM(self, filename: str):
        print(f"Export single page BOM {filename}")
        data = pd.DataFrame()

        for sku_name, sku in self.sku_list.items():
            print(f"Exporting SKU: {sku_name}")
            for pcb_name, pcba in sku.pcba_list.items():
                print(f"Exporting PCBA: {pcb_name}")
                for bom_line in pcba.BOM:
                    # print(bom_line)
                    row = {
                        "sku_name": sku_name,
                        "pcb_name": pcb_name,
                    }
                    row.update(bom_line.export())
                    print(row)
                    data = data.append(row, ignore_index=True)
        data.to_excel(filename)

    def export_pcba(self, pcba_name: str):
        """ export PCBA based on name """
        print(f"Exporting pcba with name {pcba_name}")


if __name__ == '__main__':
    print("start")

    # variables:
    load_from_excel = True  # if true, load from source, else try to load pickle file

    dirname = "."
    filename = "BOM.xlsx"
    comparison_dirname = "Comparison"

    if load_from_excel:
        owlet_iv = OWLET_IV()
        owlet_iv.load_flex_bom(dirname, filename)
        owlet_iv.load_imi_part_numbers("Summary of Owlet Materials - rev18.xlsx")
        owlet_iv.save_to_file("Owlet_IV.bin")
    else:
        owlet_iv = OWLET_IV.load_from_file("Owlet_IV.bin")

    # owlet_iv.export_single_page_BOM("Single_bom.xlsx")
    #
    # owlet_iv.export_parts("OWLET_IV_parts.xlsx")
    # owlet_iv.export_manufacture_parts("Owlet_IV_manufacturer.xlsx")
    #
    # pcba_nema_mesh_giga = PCBA("NEMA mesh giga")
    # pcba_nema_mesh_giga.load_altium_bom(os.path.join(comparison_dirname, "Bill of Materials-PCB0003-OWIV_MESH_G(NGD).csv"))
    # owlet_iv.pcba_list["MeshNode N Comm"].compare(pcba_nema_mesh_giga,
    #                                               os.path.join(comparison_dirname, "Nema_mesh_giga.xlsx"))
    #
    # pcba_nema_mesh_giga = PCBA("NEMA mesh st")
    # pcba_nema_mesh_giga.load_altium_bom(os.path.join(comparison_dirname, "Bill of Materials-PCB0003-OWIV_MESH_G(NST).csv"))
    # owlet_iv.pcba_list["MeshNode N Comm"].compare(pcba_nema_mesh_giga,
    #                                               os.path.join(comparison_dirname, "comparison_Nema_mesh_st.xlsx"))
    #
    # pcba_luma_mesh_giga = PCBA("LUMA mesh giga")
    # pcba_luma_mesh_giga.load_altium_bom(os.path.join(comparison_dirname, "Bill of Materials-PCB0003-OWIV_MESH_G(ZGD).csv"))
    # owlet_iv.pcba_list["MeshNode Z Comm"].compare(pcba_luma_mesh_giga,
    #                                               os.path.join(comparison_dirname, "comparison_Luma_mesh_giga.xlsx"))
    #
    # pcba_luma_mesh_giga = PCBA("LUMA mesh st")
    # pcba_luma_mesh_giga.load_altium_bom(os.path.join(comparison_dirname, "MESH_NZST.csv"))
    # owlet_iv.pcba_list["MeshNode Z Comm"].compare(pcba_luma_mesh_giga,
    #                                               os.path.join(comparison_dirname, "comparison_Luma_mesh_st.xlsx"))

    # pcba_nema_lte_giga = PCBA("Nema LTE st")
    # pcba_nema_lte_giga.load_altium_bom(os.path.join(comparison_dirname, "LTE_NNST.csv"))
    # owlet_iv.pcba_list["DataLift N Comm"].compare(pcba_nema_lte_giga,
    #                                               os.path.join(comparison_dirname, "comparison_Nema_lte_st.xlsx"))
    #
    # pcba_nema_mesh_giga = PCBA("MESH ")
    # pcba_nema_mesh_giga.load_altium_bom(os.path.join(comparison_dirname, "MESH_NNST.csv"))
    # owlet_iv.pcba_list["MeshNode N Comm"].compare(pcba_nema_mesh_giga,
    #                                               os.path.join(comparison_dirname, "comparison_MESH_NNST.xlsx"))

    # pcba_nema_nbiot_giga = PCBA("Luma NBIOT giga")
    # pcba_nema_nbiot_giga.load_altium_bom(os.path.join(comparison_dirname, "Bill of Materials-PCB0006-OWIV_NBIOT_G(ZGD).csv"))
    # owlet_iv.pcba_list["DataLift Z Comm"].compare(pcba_nema_nbiot_giga,
    #                                               os.path.join(comparison_dirname, "comparison_luma_NBIOT_giga.xlsx"))
    #
    # pcba_nema_nbiot_st = PCBA("Luma NBIOT giga")
    # pcba_nema_nbiot_st.load_altium_bom(os.path.join(comparison_dirname, "Bill of Materials-PCB0006-OWIV_NBIOT_G(ZST).csv"))
    # owlet_iv.pcba_list["DataLift Z Comm"].compare(pcba_nema_nbiot_st,
    #                                               os.path.join(comparison_dirname, "comparison_LUMA_NBIOT_st.xlsx"))

    # pcba_nema_nbiot_st = PCBA("NEMA Power")
    # pcba_nema_nbiot_st.load_altium_bom(os.path.join(comparison_dirname, "PCB0008-OWIV_PWR_2.csv"))
    # owlet_iv.pcba_list["NEMA Power"].compare_by_partnumber(pcba_nema_nbiot_st,
    #                                                        os.path.join(comparison_dirname, "comparison_Power.xlsx"))
    #

    # pcba_nema_nbiot_st = PCBA("NEMA Power")
    # pcba_nema_nbiot_st.load_altium_bom(os.path.join(comparison_dirname, "PCB0008-OWIV_PWR_2.csv"))
    # owlet_iv.pcba_list["NEMA Power"].compare_by_reference_designator(pcba_nema_nbiot_st,
    #                                                                  os.path.join(comparison_dirname,
    #                                                                               "comparison_Power.xlsx"))

    # pcba_nema_lte_giga = PCBA("NEMA LTE ST")
    # pcba_nema_lte_giga.load_altium_bom_new(
    #     os.path.join(comparison_dirname, "Bill of Materials-PCB0002-OWIV_LTE_G(NST)_231122.csv"))
    # owlet_iv.pcba_list["DataLift N Comm"].compare_by_reference_designator(pcba_nema_lte_giga,
    #                                                                       os.path.join(comparison_dirname,
    #                                                                                    "Bill of Materials-PCB0002-OWIV_LTE_G(NST)_231122.xlsx"))

    # pcba_nema_mesh_giga = PCBA("NEMA mesh st")
    # pcba_nema_mesh_giga.load_altium_bom_new(os.path.join(comparison_dirname, "EBOM_PCB0009_OWIV_MESH_GNS2_03-62-875-1.9_DFM.csv"))
    # owlet_iv.pcba_list["MeshNode N Comm"].compare_by_reference_designator(pcba_nema_mesh_giga,
    #                                               os.path.join(comparison_dirname, "comparison_Nema_mesh_st_1.9_DFM.xlsx"))

    # pcba_nema_lte_st = PCBA("NEMA MESH NA st")
    # pcba_nema_lte_st.load_altium_bom_new(
    #     os.path.join(comparison_dirname, "EBOM_PCB0007_OWIV_LTE_SNS2_03-62-835-1.9_DFM.csv"))
    # owlet_iv.pcba_list["DataLift N Comm"].compare_by_reference_designator(pcba_nema_lte_st,
    #                                                                       os.path.join(comparison_dirname,
    #                                                                                    "comparison_LTE_NA_migrated_1.9_DFM.xlsx"))

    # pcba_power = PCBA("NEMA LTE NA st")
    # pcba_power.load_altium_bom_new(os.path.join(comparison_dirname, "EBOM_PCB0008_OWIV_PWR_N6_03-63-099-0.9_DFM.csv"))
    # owlet_iv.pcba_list["NEMA Power"].compare_by_reference_designator(pcba_power,
    #                                                                       os.path.join(comparison_dirname, "comparison_Power_migrated.xlsx"))
    #
    # pcba_nema_lte_st = PCBA("NEMA LTE NA st")
    # pcba_nema_lte_st.load_altium_bom_new(
    #     os.path.join(comparison_dirname, "EBOM_PCB0007_OWIV_LTE_SNS2_03-62-835-1.9_DFM.csv"))
    #
    # pcba_nema_lte_g = PCBA("NEMA LTE NA st G")
    # pcba_nema_lte_g.load_altium_bom_new(
    #     os.path.join(comparison_dirname, "EBOM_PCB0002_03-54-261_A.2_ENS1.csv"))
    #
    # pcba_nema_lte_g.compare_by_reference_designator(pcba_nema_lte_st, os.path.join(comparison_dirname,
    #                                                                                    "comparison_2_LTE_NA_migrated_1.9_DFM.xlsx"))

    # pcba_mesh = PCBA("NEMA MESH")
    # pcba_mesh.load_altium_bom_new(os.path.join(comparison_dirname,"EBOM_03-67-873_PCB0013_OWIV_MESH_ANS2_1.csv"))
    # owlet_iv.pcba_list["MeshNode N Comm"].compare_by_reference_designator(pcba_mesh, os.path.join(comparison_dirname,
    #                                                                                     "MESH_ZD4i_comparison.xlsx"))
    # #
    # pcb_mesh_aus = PCBA("MESH AUS")
    # pcb_mesh_aus.load_altium_bom_new(os.path.join(comparison_dirname,"EBOM_03-67-873_PCB0013_OWIV_MESH_ANS2_1.csv"))

    # pcb_mesh_aus_zd4i =PCBA("MESH AUS ZD4i")
    # pcb_mesh_aus_zd4i.load_altium_bom_new(os.path.join(comparison_dirname,"EBOM_03-67-869_PCB0013_OWIV_MESH_AAS2_1.csv"))
    # pcb_mesh_aus.compare_by_reference_designator(pcb_mesh_aus_zd4i, os.path.join(comparison_dirname,
    #                                                                                     "MESH_ZD4i_comparison.xlsx"))
    #
    # pcb_mesh_nam =PCBA("MESH NAM")
    # pcb_mesh_nam.load_altium_bom_new(os.path.join(comparison_dirname,"EBOM_03-67-877_PCB0013_OWIV_MESH_GNS2_1.csv"))
    # pcb_mesh_aus.compare_by_reference_designator(pcb_mesh_nam, os.path.join(comparison_dirname,
    #                                                                                     "MESH_NAM_AUS_comparison.xlsx"))

    # pcba_lte = PCBA("NEMA MESH")
    # pcba_lte.load_altium_bom(os.path.join(comparison_dirname, "EBOM_PCB0015485R001_OWIV_LTE_EAS1_03-64-589-1.0.csv"))
    # owlet_iv.pcba_list["DataLift N Comm"].compare_by_reference_designator(pcba_lte, os.path.join(comparison_dirname,
    #                                                                                     "LTE_ausgrid_comparison.xlsx"))
    #

    # pcba_power = PCBA("NEMA POWER")
    # pcba_power.load_altium_bom(os.path.join(comparison_dirname, "EBOM_PCB0015507R001_OWIV_POWER_A4_03-64-586-1.0.csv"))
    # owlet_iv.pcba_list["NEMA Power"].compare_by_reference_designator(pcba_power, os.path.join(comparison_dirname,
    #                                                                                     "Power_ausgrid_comparison.xlsx"))

    # pcb_lte_nam = PCBA("lte NAM")
    # pcb_lte_nam.load_altium_bom_new(os.path.join(comparison_dirname, "EBOM_03-67-866_PCB0012_OWIV_LTE_NNS2_1.csv"))
    #
    # pcb_lte_aus = PCBA("lte AUS")
    # pcb_lte_aus.load_altium_bom_new(os.path.join(comparison_dirname, "EBOM_03-67-881_PCB0012_OWIV_LTE_ANS2_1.csv"))
    # # pcb_lte_nam.compare_by_reference_designator(pcb_lte_aus, os.path.join(comparison_dirname,
    # #                                                                        "LTE_NAM_AUS_comparison.xlsx"))
    #
    # pcb_lte_aus_d4i = PCBA("lte AUS d4i")
    # pcb_lte_aus_d4i.load_altium_bom_new(os.path.join(comparison_dirname, "EBOM_03-67-868_PCB0012_OWIV_LTE_AAS2_1.csv"))
    # pcb_lte_aus.compare_by_reference_designator(pcb_lte_aus_d4i, os.path.join(comparison_dirname,
    #                                                                        "LTE_AUS_AUS_ZD4i_comparison.xlsx"))

    pcb_power = PCBA("pwoer 6kv")
    pcb_power.load_altium_bom_new(os.path.join(comparison_dirname, "EBOM_03-63-099_PCB0008_OWIV_PWR_N.csv"))
    owlet_iv.pcba_list["NEMA Power"].compare_by_reference_designator(pcb_power, os.path.join(comparison_dirname,
                                                                              "6kV_Power_comparison.xlsx"))

    pcb_power_d4i = PCBA("pwoer 6kv d4i")
    pcb_power_d4i.load_altium_bom_new(os.path.join(comparison_dirname, "EBOM_03-59-914_PCB0008_OWIV_PWR_A6.csv"))
    pcb_power.compare_by_reference_designator(pcb_power_d4i, os.path.join(comparison_dirname,
                                                                              "6kV_D4i_Power_comparison.xlsx"))

    # owlet_iv.pcba_list["DataLift N Comm"].compare_by_reference_designator(pcb_lte_nam, os.path.join(comparison_dirname,
    #                                                                                     "LTE_AUS_comparison.xlsx"))

    # pcb_lte_aus_zd4i =PCBA("lte AUS ZD4i")
    # pcb_lte_aus_zd4i.load_altium_bom_new(os.path.join(comparison_dirname,"EBOM_03-67-869_PCB0013_OWIV_lte_AAS2_1.csv"))
    # pcb_lte_aus.compare_by_reference_designator(pcb_lte_aus_zd4i, os.path.join(comparison_dirname,
    #                                                                                     "lte_ZD4i_comparison.xlsx"))

    # "EBOM_PCB0015485R001_OWIV_LTE_EAS1_03-64-589-1.0.csv"
    # "EBOM_PCB0015507R001_OWIV_POWER_A4_03-64-586-1.0.csv"
    print("end")
