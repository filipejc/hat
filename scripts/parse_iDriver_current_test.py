"""
# Filename:     parse_iDriver_current_test.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   02/02/2023
# Last Update:  02/02/2023
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import os
import re
import pandas as pd
import time

under_protect_on_str = "UNDER VOLTAGE protected"
under_protect_off_str = "UNDER VOLTAGE recovered"
current_error_on_str = "CURRENT FEEDBACK ERROR protected"
overvoltage_protect_on_str = "OVER VOLTAGE protected"
overvoltage_protect_off_str = "OVER VOLTAGE recovered"
power_startup_current_re = re.compile(r"PowerCtrl startup (\w+) - (\d+)mA")


def parse_log(directory, filename, labels):
    under_protect_state = None
    current_protect_state = None
    overvoltage_protect_state = None

    startup_current = None
    startup_confirmed = None

    idx = 0
    data = pd.DataFrame()

    print(f"Parsing log file: {filename}")

    with open(os.path.join(directory, filename), errors="ignore") as f:
        for line_idx, line in enumerate(f):
            match = re.search(power_startup_current_re, line)

            if under_protect_on_str in line:
                if not under_protect_state:  # if previous state was not in protection, clear the measurement current
                    startup_current = None
                under_protect_state = True
            elif under_protect_off_str in line:
                under_protect_state = False
                # change previous line to not in protection
                # data.loc[idx - 1, "Protect state"] = False
            elif match:
                startup_current = int(match.group(2))
                startup_confirmed = True if "confirmed" in match.group(1) else False
            elif current_error_on_str in line:
                current_protect_state = True
            elif overvoltage_protect_on_str in line:
                overvoltage_protect_state = True
            elif overvoltage_protect_off_str in line:
                overvoltage_protect_state = False
            else:
                continue

            # labels
            for key, label in labels.items():
                data.loc[idx, key] = label

            # a match was found
            data.loc[idx, "Line Number"] = line_idx
            data.loc[idx, "Line"] = line
            data.loc[idx, "Protect state"] = under_protect_state
            data.loc[idx, "Startup confirmed"] = startup_confirmed
            data.loc[idx, "Startup current"] = startup_current
            data.loc[idx, "Current feedback"] = current_protect_state
            data.loc[idx, "overvoltage"] = overvoltage_protect_state

            idx += 1

    return data


if __name__ == '__main__':
    logs_directory = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\iDriver\Project Folder\08_Support\20230202_debug_startup_current_on_short_circuit\Short_circuit_debug_010223\Threshold_-30"
    results_directory = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\iDriver\Project Folder\08_Support\20230202_debug_startup_current_on_short_circuit"
    output_file = time.strftime("log_threshold_-30_%H%M.xlsx")

    all_data = None

    file_labeling = {
        "DIM20W_079_2_threshold_30mA.txt": {"Driver": "D20A", "SN": "079"},
        "DIM40W_889_#1_threshold_30mA.txt": {"Driver": "D40A", "SN": "889"},
        "DIM75WA_002_threshold_30mA.txt": {"Driver": "D75A", "SN": "002"},
        "DIM75WB_005_#1_threshold_30mA.txt": {"Driver": "D75B", "SN": "005"},
        "DIM75WB_005_#2_threshold_30mA.txt": {"Driver": "D75B", "SN": "005"},
        "DIM120W_002_#1_threshold_30mA.txt": {"Driver": "D120B", "SN": "889"},
        "SMART20_001_#1_threshold_30mA.txt": {"Driver": "S20A", "SN": "320"},
        "SMART40W_010_#1_threshold_30mA.txt": {"Driver": "S40A", "SN": "010"},
        "SMART75WA_004_threshold_30mA.txt": {"Driver": "S75A", "SN": "004"},
        "SMART75WB_422_#1_threshold_30mA.txt": {"Driver": "S75B", "SN": "422"},
        "SMART120_007_#1_theshold_30mA.txt": {"Driver": "S120A", "SN": "007"},
    }

    print("Start parsing!")
    excel = pd.ExcelWriter(os.path.join(results_directory, output_file))

    for filename, labels in file_labeling.items():
        data = parse_log(logs_directory, filename, labels)
        data.to_excel(excel, sheet_name=os.path.splitext(filename)[0][:29])

        if all_data is None:
            all_data = data
        else:
            all_data = pd.concat([all_data, data])

    all_data.to_excel(excel, sheet_name="All_data")

    excel.save()
    excel.close()
    print("end!")
