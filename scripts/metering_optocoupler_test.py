"""
# Filename:     metering_optocoupler_test.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   02/03/2022
# Last Update:  02/03/2022
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms.g4_interface_cli import G4
from hat.utils.DataStorage import DataStorage
import time

if __name__ == '__main__':
    print("starting")

    # variables
    time_between_cycles = 10
    relay_switch_interval = 120
    relay_status = True
    relay_change_time = 0

    data = DataStorage()
    data.open("results.xlsx")

    g4 = G4("COM72", interface_type="SERIAL_THREAD", log_file="%H%M_metering.txt")

    while True:
        try:
            # get measurements
            voltage = g4.lwm2m_get_supply_voltage()
            current = g4.lwm2m_get_supply_current()
            power = g4.lwm2m_get_active_power()

            # check if time to perform relay change
            if time.time() - relay_change_time > relay_switch_interval:
                print("Time to change relay")
                g4.set_relay_state_cli(not relay_status)

                relay_status = not relay_status
                relay_change_time = time.time()

            data["voltage"] = voltage
            data["current"] = current
            data["power"] = power

            data.start_next_test()
            print(f"Sleeping {time_between_cycles} seconds")
            time.sleep(time_between_cycles)
        except Exception as e:
            print(f"Error: {e}, continuing")
            time.sleep(10)

    data.close()