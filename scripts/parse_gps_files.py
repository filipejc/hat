"""
# Filename:     parse_gps_files.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   06/01/2022
# Last Update:  06/01/2022
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import re
import numpy as np
import matplotlib.pyplot as plt
from hat.utils.utils import create_logger
from hat.utils import DataStorage
import os
import pynmea2

log = create_logger(__name__)


def degrees_to_meters(degree, latitude=39):
    degrees_arc_fact = np.cos(np.radians(latitude)) * 1852 / 60  # arc to meter
    meters = degree * 3600 * degrees_arc_fact

    return meters


def calculate_distances(lat_dist, lon_dist):
    return np.sqrt(np.power(lat_dist, 2) + np.power(lon_dist, 2))


def plot_map(latitude, longitude, directory, name):
    plt.figure()

    avg_lat, avg_lon = average_position(latitude, longitude)

    latitude = np.array(latitude)
    longitude = np.array(longitude)

    diff_lat = latitude - avg_lat
    diff_lon = longitude - avg_lon

    diff_lat_meter = [degrees_to_meters(x) for x in diff_lat]
    diff_lon_meter = [degrees_to_meters(x) for x in diff_lon]

    distances = calculate_distances(diff_lat_meter, diff_lon_meter)
    ax = plt.gca()
    ax.cla()  # clear things for fresh plot

    max_distance = np.max(distances)

    circle1 = plt.Circle((0, 0), max_distance, color='g', fill=False)

    ax.add_patch(circle1)

    plt.scatter(diff_lon_meter, diff_lat_meter)
    plt.plot([0], [0], "r+")

    plt.xlabel("longitude")
    plt.ylabel("latitude")
    plt.legend(["average position", f"radius = {max_distance:.1f}m", "positions"])
    plt.savefig(os.path.join(directory, f"{name}.png"))
    # plt.show()
    return max_distance


def average_position(latitude, longitude):
    return np.average(latitude), np.average(longitude)


def process_gps(directory, filename):
    complete_filename = os.path.join(directory, filename)

    log.info(f"Parsing files: {complete_filename}")
    count_rmc = 0
    position_found = False
    latitude = []
    longitude = []

    with open(complete_filename) as f:
        for line in f:
            # log.info(f"line: {line}")
            gps = pynmea2.parse(line)
            if not position_found and gps.sentence_type == "RMC":
                count_rmc += 1
            if gps.sentence_type == "RMC" and gps.is_valid:
                position_found = True
                latitude.append(gps.latitude)
                longitude.append(gps.longitude)

    radius = plot_map(latitude, longitude, directory, filename)
    return count_rmc, radius


def process_dir(dirname):
    data = DataStorage.DataStorage()
    data.open("gps.xlsx", dirname)

    filenames = os.listdir(dirname)

    for filename in filenames:
        if os.path.splitext(filename)[-1] != ".ubx":
            continue

        print(f"filename: {filename}")
        match = re.search("(?P<time>\d+_\d+)_(?P<model>.+)_gps_(?P<reset>\w+)_(?P<hard>\w+)\.ubx", filename)
        if match:
            ttff, radius = process_gps(dirname, filename)
            data["Time"] = match.group("time")
            data["Model"] = match.group("model")
            data["reset"] = match.group("reset")
            data["hard"] = match.group("hard")
            data["Time for first Fix"] = ttff
            data["Radius"] = radius
            data.start_next_test()


if __name__ == '__main__':
    log.info("Test started ")
    directory = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\04_Tests\07_PVT\GPS_tests\Raw_data_short"
    filename = "20220102_1257_DataLift N_gps_cold_soft.ubx"

    # degrees
    distance = degrees_to_meters(1.635E-05)
    print(f"Distance: {distance}")

    # process_gps(directory, filename)
    process_dir(directory)
    log.info("Test end")
