"""
# Filename:     eeprom_decoder.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   01/09/2022
# Last Update:  01/09/2022
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
import re
import numpy as np
from dataclasses import dataclass
import pandas as pd


def load_memory_from_file(filename):
    data = np.zeros(1024, dtype=np.ubyte)

    with open(filename, encoding="utf8") as f:
        for line in f:
            match = re.match(r"DB\s+(\d+):\s+(\w+)\s+(\w+)\s+(\w+)\s+(\w+)", line)
            if match:
                start_address = int(match.group(1)) * 4
                data[start_address] = int(match.group(2), 16)
                data[start_address + 1] = int(match.group(3), 16)
                data[start_address + 2] = int(match.group(4), 16)
                data[start_address + 3] = int(match.group(5), 16)
            else:
                print("No match, please check")

    return data

@dataclass
class MemoryLoc:
    name: str
    start_address: int
    end_address: int
    length: int
    # value: int = 0


def load_eeprom_mapping(filename):
    data = pd.read_excel(filename, skiprows=3)
    mapping = {}
    for idx, row in data.iterrows():
        try:
            mapping[row["Bank Location Name"]] = MemoryLoc(
                row["Bank Location Name"],
                int(row["NFC EEPROM ADDRESS\n(0x000 - 0x1FF)"], 16),
                int(row["NFC EEPROM ADDRESS END(hex)-auto"], 16),
                int(row["LENGTH"])
            )
        except Exception as e:
            print(f"Problem in row: {row}")

    # print(data)
    return mapping


def convert_raw_data(data, mapping):
    res = pd.DataFrame()
    idx = 0
    for key, item in mapping.items():
        print(f"Key: {key}")
        res.loc[idx, "Name"] = item.name
        memory = bytearray(data[item.start_address:item.end_address+1])
        res.loc[idx, "Value"] = int.from_bytes(memory, "big")

        idx += 1

    return res

if __name__ == '__main__':
    # variables
    act_filename = "D20_memory.txt"
    schreder_filename = "D20_schreder_memory.txt"
    mapping_filename = "Schreder_iDriver_NFC_EEPROM_Memory_Allocation_And_Layout_20210208.xlsx"

    mapping = load_eeprom_mapping(mapping_filename)

    # ACT driver processing
    data = load_memory_from_file(act_filename)
    res = convert_raw_data(data, mapping)
    # res.to_excel("D20_act_memory.xlsx")

    # Schreder driver processing

    data = load_memory_from_file(schreder_filename)
    res = convert_raw_data(data, mapping)
    res.to_excel("D20_schreder_memory.xlsx")

    print("end!")
