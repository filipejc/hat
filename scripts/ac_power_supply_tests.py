import numpy as np
import os
from hat.comms.g4_interface_cli import G4
from hat.drivers.AcSourceInstrument import AcSourceInstrument
from hat.utils.DataStorage import DataStorage
from hat.drivers.AcPowerMeterInstrument import AcPowerMeterInstrument
from hat.drivers.Oscilloscope import Oscilloscope
from hat.utils.utils import create_logger
import time
import asyncio
import itertools

log = create_logger(__name__)


async def initialize(label: str, conf: dict):
    log.info(f"{label}: Trying to initiatlize")

    g4 = conf["g4"]

    # set prompt for flex app
    g4.set_prompt("$")

    log.info(f"{label}: Initialization completed:")


def frequency_test(frequency_range, voltage_range, ac: AcSourceInstrument, g4, pm: AcPowerMeterInstrument,
                   data: DataStorage, wait_time=20):
    # set configuration on ac source
    log.info(f"Turning on controller")
    ac.set_voltage_AC(230)
    ac.set_frequency(50)
    ac.set_output_state(True)
    log.info("Waiting for node to startup")

    time.sleep(startup_time)
    g4.set_global_loglevel(4)
    g4.execute_command("cm-suspend 3600")  # suspends connection manaer

    # start device
    log.info("Configure measurements on power meter")
    pm.clear_measurements()
    pm.add_meas_voltage_rms()
    pm.add_meas_active_power()
    pm.add_meas_frequency()

    for voltage in voltage_range:
        log.info(f"Test voltage: {voltage} V")
        ac.set_voltage_AC(voltage)
        for freq in frequency_range:
            log.info(f"Testing frequency: {freq} Hz")
            ac.set_frequency(freq)

            log.info(f"Wait time {wait_time}")
            time.sleep(wait_time)
            for meas, value in pm.get_measurement().items():
                data[f"PM {meas}"] = np.abs(value)

            # get value from G4
            volt = g4.lwm2m_get_supply_voltage()
            power = g4.lwm2m_get_active_power()
            freq = g4.lwm2m_get_frequency()

            data[f"G4 volt"] = volt
            data[f"G4 power"] = power
            data[f"G4 freq"] = freq

            data.start_next_test()


async def measure_start_up(label: str, conf: dict, dict_for_data: dict):
    g4 = conf["g4"]
    data = conf["data"]
    isNema = conf["isNema"]
    isDataLift = conf["isDataLift"]

    data["Time"] = time.asctime()
    for key, value in dict_for_data.items():
        data[key] = value

    try:
        # interfaces to check
        # photocell
        photocell = g4.flex_get_photocell()
        data["Photocell"] = photocell
        # spi-flash
        flash_test = g4.flex_execute_spi_flash_test()
        data["SPI flash"] = flash_test
        # wisun
        # enable wisun
        g4.flex_lpwn_enable()

        wisun = g4.flex_lpwn_txpd()
        data["Wisun"] = wisun
        # Lte, NBIOT in DataLift
        if isDataLift:
            # Modem get connection status
            unsolicited, creg_status = g4.flex_get_creg()
            unsolicited, cgreg_status = g4.flex_get_cgreg()
            data["creg"] = creg_status
            data["creg status"] = creg_status == conf["expectedCREG"]
            data["cgreg"] = cgreg_status
            data["cgreg status"] = cgreg_status == conf["expectedCGREG"]

            # ESIM in DataLift
            imsi = g4.flex_get_imsi()
            data["imsi"] = imsi

        # Metering in Nema
        if isNema:
            res = g4.flex_get_meter_data()
            data["AC voltage"] = res["voltage"]
            data["current"] = res["current"]
            data["power"] = res["power"]

            # RFID
            for j in range(5):
                try:
                    rfid = g4.flex_get_rfid()
                    data["RFID"] = rfid
                    data["RFID count"] = j
                    data["RFID first try"] = j == 0
                    break
                except Exception as e:
                    log.warning(f"Could not read RFID tag", e)

                    if j == 4:
                        data["RFID"] = "FAILED"

        # GPS
        gps = g4.flex_gps_dump()
        data["GGA count"] = gps["GGA count"]
        data["GGA count ok"] = data["GGA count"] != 0
        # power voltages
        res = g4.flex_get_voltages_temp()
        data["24V"] = res["24V"]
        data["supercap"] = res["supercap"]
        data["MCU temp"] = res["MCU temp"]

        data["Error"] = False

    except Exception as e:
        log.error("error during execution: ", e)
        data["Error"] = True
        data["Error message"] = str(e)

    data.start_next_test()
    return dict(data._data.iloc[-1])


async def voltage_start_test(confs, ac: AcSourceInstrument, voltages_to_test, frequency_to_test, angles_to_test):
    log.info("Staring voltage start test function")
    # start test
    # L = await asyncio.gather(*[initialize(label, conf["g4"]) for label, conf in confs.items()])
    # print(L)
    startup_time = 20
    discharge_time = 60

    points_to_test = itertools.product(voltages_to_test, frequency_to_test, angles_to_test)
    cycle = 0
    for voltage, frequency, angle in points_to_test:
        log.info(
            f"Starting cycle: {cycle + 1} at voltage {voltage} V at frequency {frequency} Hz at angle {angle} degree")
        ac.set_voltage_AC(voltage)
        ac.set_frequency(frequency)
        ac.set_phase_on(angle)
        time.sleep(3)
        ac.set_output_state(True)

        log.info(f"Waiting {startup_time} seconds to start")
        time.sleep(startup_time)

        if cycle == 0:  # first cycle
            log.info("Initializing...")
            L = await asyncio.gather(*[initialize(label, conf) for label, conf in confs.items()])

        await asyncio.gather(
            *[measure_start_up(label, conf, {"Set voltage": voltage,
                                             "Set frequency": frequency,
                                             "Set angle": angle,
                                             }) for label, conf in confs.items()])

        log.info("Measurement cycle completed, turning off power source")
        ac.set_output_state(False)
        log.info(f"Waiting {discharge_time} seconds to discharge!")
        time.sleep(discharge_time)
        cycle += 1

    log.info("End of main!")


async def waveform_check(confs, ac: AcSourceInstrument, osc: Oscilloscope, voltages_to_test, waveforms):
    points_to_test = itertools.product(voltages_to_test, waveforms)

    await asyncio.gather(*[initialize(label, conf) for label, conf in confs.items()])

    cycle = 0
    for voltage, waveform in points_to_test:
        log.info(f"Starting cycle: {cycle + 1} at voltage {voltage} V with waveform {waveform}")
        ac.set_source_waveform("A", waveform)

        start_time = time.time()
        while time.time() - start_time < 60:
            await asyncio.gather(
                *[measure_start_up(label, conf, {"Cycle": cycle,
                                                 "Voltage": voltage,
                                                 "Waveform": waveform
                                                 }) for label, conf in confs.items()])

        # save picture
        for label, conf in confs.items():
            osc.get_screen_capture(f"{waveform}_{voltage}V.jpg", conf["test_directory"])

        cycle += 1


async def continuous_check(confs):
    await asyncio.gather(*[initialize(label, conf) for label, conf in confs.items()])

    cycle = 0
    while True:
        await asyncio.gather(
            *[measure_start_up(label, conf, {}) for label, conf in confs.items()])
        cycle += 1


def random_generator(cut_times: list, random: bool):
    if random:
        while True:
            yield np.random.randint(cut_times[0], cut_times[1])
    else:
        for cut_time in cut_times:
            yield cut_time


async def cut_startup(confs, ac: AcSourceInstrument, cut_times: list, time_after_cut: int, random: bool = False):
    log.info("Starting initilization")
    await asyncio.gather(*[initialize(label, conf) for label, conf in confs.items()])

    log.info("Starting cycles")
    cycle = 0
    for cut_time in random_generator(cut_times, random):
        log.info(f"Staring cycle {cycle} with {cut_time} ms")

        ac.config_pulse_mode(0, 0, 50, 1, 100, cut_time)
        log.info("Making the cut")
        ac.set_trigger(True)
        log.info(f"waiting {time_after_cut} seconds after the cut")
        time.sleep(time_after_cut)

        log.info("Checking if survive cut")
        await asyncio.gather(
            *[measure_start_up(label, conf, {"Cycle": cycle,
                                             "Cut time": cut_time}
                               ) for label, conf in confs.items()])

        cycle += 1

    log.info("End of cut startup check")


async def slow_voltage(label, conf, ac, slew_rate, start_voltage):
    log.info(f"testing {label}")

    await initialize(label, conf)

    log.info(f"Setting slew rate")
    ac.set_voltage_slew_rate_AC(slew_rate / 1000.0)  # set slew rate
    for repetition in range(10):
        log.info("Starting to decrease voltage")
        ac.set_voltage_AC(0)

        waiting_shutdown = True

        cycle = 0
        while True:
            voltage = ac.get_voltage_rms()
            res = await measure_start_up(label, conf, {
                "Cycle": cycle,
                "Repetition": repetition,
                "voltage": voltage})

            volt_24V = res["24V"]
            log.info(f"24V = {volt_24V}V")
            if waiting_shutdown and volt_24V < 20:
                log.info(f"24V drop, starting going up")
                ac.set_voltage_AC(start_voltage)
                waiting_shutdown = False

            ac_voltage = res.get("AC voltage", 0)
            log.info(f"AC voltage: {ac_voltage} V")
            if not waiting_shutdown and ac_voltage > start_voltage - 5:
                log.info("Voltage back to normal, stopping cycle")
                break

            cycle += 1


if __name__ == "__main__":
    # variables
    #
    ac_source_addr = "ASRL40::INSTR"
    pm_addr = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    osc_addr = "VICP::192.168.8.163"

    exec_frequency_test = False
    exec_start_voltage = False
    exec_start_angle = False
    exec_multiple_startup = False
    exec_waveform_check = False
    exec_continuous_check = False
    exec_cut_startup = False  # check if 24V and metering can recover after a cut in the power supply
    exec_cut_random = False  # check if 24V and metering can recover after a random cut in the AC power supply

    exec_slow_voltage_drop = True  # check if device survive a slow voltage drop and recover

    if exec_frequency_test:
        # frequency range
        start_freq = 47.5  # Hz
        stop_freq = 63  # Hz
        step_freq = 0.5  # Hz
        frequency_range = np.arange(start_freq, stop_freq + step_freq, step_freq)

        # voltage range
        voltage_range = [110, 230, 277]

    if exec_start_voltage:
        # variables for startup voltage
        start_voltages = np.concatenate((np.array([99]), np.arange(110, 301, 10)))  # voltages to test
        frequency_to_test = [50]
        angles_to_test = [0]
        test_name = "startup"

    if exec_start_angle:
        # variables for startup voltage
        start_voltages = [110, 230, 277]  # voltages to test
        frequency_to_test = [50]
        angles_to_test = np.arange(0, 360, 15)
        test_name = "start_angle"

    if exec_multiple_startup:
        n_tests = 48
        nominal_voltage = 230
        start_voltages = [nominal_voltage] * n_tests
        frequency_to_test = [50]
        angles_to_test = [0]
        test_name = "multiple_startup"

    if exec_waveform_check:
        start_voltages = [230]
        waveforms = [f"DST{x:02d}" for x in range(1, 31)]
        test_name = "continuous_check"

    if exec_continuous_check:
        test_name = "continuous_check"

    if exec_cut_startup:
        test_name = "cut_start"
        start_voltages = [230]  # voltage to test
        cut_times = np.arange(2550, 3001, 10)  # times to test in ms
        time_after_cut = 5  # number of seconds to wait after cut

    if exec_cut_random:
        test_name = "cut_random"
        start_voltages = [230]  # voltage to test
        cut_times = [100, 2500]  # minimum and maximum cut times
        time_after_cut = 2  # number of seconds to wait after cut

    if exec_slow_voltage_drop:
        test_name = "slow_voltage_change"
        n_repetitions = 10
        start_voltage = 99
        slew_rate = 1  # volts per seconds

    # variables
    confs = {
        "NEMA MESH": {
            "com_port": "COM29",
            "logfile": f"%Y%m%d_%H%M_{test_name}_NEMA_MESH.txt",
            "datafile": f"data_{test_name}_NEMA_MESH.xlsx",
            "test_directory": r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\04_Tests\07_PVT\MeshNode N\AC\Slow_voltage",
            "isNema": True,
            "isDataLift": False
        },
        "NEMA LTE": {
            "com_port": "COM225",
            "logfile": f"%Y%m%d_%H%M_{test_name}_NEMA_LTE.txt",
            "datafile": f"data_{test_name}_NEMA_LTE.xlsx",
            "test_directory": r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\04_Tests\07_PVT\DataLift N\AC\Slow_voltage",
            "isNema": True,
            "isDataLift": True,
            "expectedCREG": 5,
            "expectedCGREG": 5
        }
    }
    # timmings
    startup_time = 60
    wait_time = 20  # time between each

    log.info("Start")

    # connection to instruments
    ac = AcSourceInstrument(ac_source_addr)
    osc = Oscilloscope(osc_addr)
    # pm = AcPowerMeterInstrument(pm_addr)

    # data
    for label, conf in confs.items():
        data = DataStorage()
        data.open(os.path.join(conf["test_directory"], conf["datafile"]))
        g4 = G4(conf["com_port"], "SERIAL_THREAD", log_file=os.path.join(conf["test_directory"], conf["logfile"]))
        conf["g4"] = g4
        conf["data"] = data

    # start frequency test
    if exec_frequency_test:
        log.info("Executing frequency test")
        frequency_test(frequency_range, voltage_range, ac, g4, pm, data, wait_time)

    # start voltage startup test
    if exec_start_voltage:
        log.info("Staring voltage startup test")
        asyncio.run(voltage_start_test(confs, ac, start_voltages, frequency_to_test, angles_to_test))

    if exec_start_angle:
        log.info("Starting angle startup test")
        asyncio.run(voltage_start_test(confs, ac, start_voltages, frequency_to_test, angles_to_test))

    if exec_multiple_startup:
        log.info("Starting multiple startup test")
        asyncio.run(voltage_start_test(confs, ac, start_voltages, frequency_to_test, angles_to_test))

    if exec_waveform_check:
        log.info("Starting continuous check")
        osc.open()
        ac.set_voltage_AC(230)
        ac.set_frequency(50)
        ac.set_output_state(True)
        log.info("Waiting 20 seconds for startup")
        time.sleep(20)
        asyncio.run(waveform_check(confs, ac, osc, start_voltages, waveforms))

    if exec_waveform_check:
        log.info("Starting continuous check")
        osc.open()
        ac.set_voltage_AC(230)
        ac.set_output_state(True)
        log.info("Waiting 20 seconds for startup")
        time.sleep(20)
        asyncio.run(waveform_check(confs, ac, osc, start_voltages, waveforms))

    if exec_continuous_check:
        asyncio.run(continuous_check(confs))

    if exec_cut_startup:
        log.info("Check cut startup")
        # osc.open()
        ac.set_voltage_AC(230)
        ac.set_frequency(50)
        ac.set_output_state(True)
        log.info("Waiting 20 seconds for startup")
        time.sleep(20)
        asyncio.run(cut_startup(confs, ac, cut_times, time_after_cut, random=False))

    if exec_cut_random:
        log.info("Check cut random")
        # osc.open()
        ac.set_voltage_AC(230)
        ac.set_frequency(50)
        ac.set_output_state(True)
        log.info("Waiting 20 seconds for startup")
        time.sleep(20)
        asyncio.run(cut_startup(confs, ac, cut_times, time_after_cut, random=True))

    if exec_slow_voltage_drop:
        log.info("Slow voltage drop")

        for label, conf in confs.items():
            log.info(f"Testing {label} at {start_voltage} V ")
            ac.set_voltage_slew_rate_AC(0)
            ac.set_ac_configuration(start_voltage, 50, True)

            log.info(f"Waiting {wait_time} seconds")
            time.sleep(wait_time)

            asyncio.run(slow_voltage(label, conf, ac, slew_rate, start_voltage))

    log.info("end")
