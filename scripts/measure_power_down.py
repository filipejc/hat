from genericpath import exists
import sys

sys.path.append('C:\code')
sys.path.append('C:\code\hat')

import os
import hat.utils.DataStorage
import numpy as np
import time
import logging
from datetime import datetime
from hat.comms import g4_interface_cli
from hat.drivers import AcPowerMeterInstrument
from hat.drivers import AcSourceInstrument
from hat.drivers import ACDC_load
from hat.utils import utils
import matplotlib.pyplot as plt


if __name__ == "__main__":

    ### voltage under test
    voltages = [100]                            # Voltage for current saturation test
    # voltages = [120]                            # Voltage for Accuracy test at 120V
    # voltages = [277]                            # Voltage for Accuracy test at 277V
    ### current under test
    currents = np.arange(0.5, 7.5 + 0.5, 0.5)   # Current saturation test
    # currents = [2.50]                           # Current for Accuracy test at 120V
    # currents = [1.00]                           # Current for Accuracy test at 277V

    max_current_peak = max(currents) * 2.0  # needs to account with crest factor

    ### test parameters
    use_ac_load = True                  # to use AC load
    plot = True                         # make plot
    compensate_negative = False
    power_on_time = 10                  # number of second it takes to start
    source_stability_time = 5           # time to the source stability
    load_stability_time = 15            # time to the load stability
    filename = "Schreder_FW.xls" 
    trials = 1                          # number of measurements for each voltage/current condition
    # trials = 50                         # number of measurements for each voltage/current condition
    trials_interval = 12                # interval in seconds among the trials
    use_flex_app = True

    #### connection strings
    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    g4_com_port = "COM3"
    ac_com_port = "ASRL5::INSTR"
    load_com_port = "ASRL4::INSTR"

    ### connect to instruments and G4
    ac = AcSourceInstrument.AcSourceInstrument(ac_com_port, reset=False)

    if use_ac_load:
        load = ACDC_load.ACDCLoad(load_com_port, reset=False)

    # teardown
    ac.set_output_state(False)

    if use_ac_load:
        load.set_output_state(False)
    
    quit()