"""
# Filename:     FW_update_over_zigbee.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   12/05/2021
# Last Update:  12/05/2021
#
# Version:      1.0
# Filetype:     
# Description:  Firmware update using AT otap messages send over zigbee
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms import P7CM

if __name__ == "__main__":
    print("Starting firmware update")
    com_port = "COM77"
    eui = "00:13:A2:00:41:76:09:EB"

    zb = P7CM.P7CM(com_port, interface_type="zigbee", eui=eui)

    func = zb.get_at_functionality()

    # set full functionality radio on
    if func != "Full":
        zb.set_at_full()

        func = zb.get_at_functionality()
        if func != "Full":
            print("Not in full functionality")

    if "Registered" in zb.get_at_creg()["status"]:
        print("Device already registered in network")
    else:
        print("Force device network registration")
        zb.force_network_search()

    network = zb.get_at_network()
    print(f"Network: {network}")

    creg = zb.get_at_creg()
    print(f"creg: {creg}")

    # chunk messages don't work
    use_chunk = False

    command = 'AT^SJOTAP="owlet","http://u.owlet-iot.de/3.32.15.xx.jad","A:/","","","gprs","reg.o3iot.fr","ow3@reg-o3iot.fr.m2m","AgPw473h9p","195.177.188.21",,,'

    if use_chunk:
        n = 20  # number of bytes to send
        command_chunks = [command[i:i + n] for i in range(0, len(command), n)]

        for data in command_chunks:
            zb._intf._write_command_to_cli(data)

        zb._intf._write_command_to_cli("\r\n")
        res = zb._intf._read_explicitly_from_remote(10)

    else:
        res = zb.execute_at(command)

    print(f"otap result: {res}")

    otap_configs = zb.execute_at("AT^SJOTAP?")
    print(f"OTAP configs: {otap_configs}")

    print("execute otap message")
    zb.execute_at("AT^SJOTAP")

    print("end!")
