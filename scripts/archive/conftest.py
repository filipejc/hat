import time
import datetime
from py.xml import html
import pytest

def pytest_html_report_title(report):
    report.title = time.strftime("G4 test report %d-%m-%Y")


def pytest_configure(config):
    config._metadata["Device type"] = "G4 NEMA LTE DVT"
    config._metadata["ID"] = "L34"
    config._metadata["git"] = "git_012345678"


def pytest_html_results_summary(prefix, summary, postfix):
    prefix.extend([html.p("This is a test including Hardware stimulus to a G4.")])

