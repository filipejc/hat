import serial
import time

if __name__ == "__main__":
    com_port = "COM115"
    baudrate = 115200
    timeout = 10
    data =""
    connected = False

    while True:
        try:
            dev = serial.Serial(port=com_port, baudrate=baudrate, timeout=timeout)
            connected = True
            print("Connected!")
            while True:
                data += dev.read(1).decode("ascii")
                print(f"Data {data}")
                if "SYSSTART" in data:
                    dev.write('AT^SCFG="Userware/Autostart","","0"\r'.encode("ascii"))
                    data = ""
                    print("Sysstart found, send command")
                # time.sleep(0.1)
        except serial.SerialException as e:
            if connected:
                print("Disconnected")
                connected = False
            time.sleep(0.01)


    print("end!")