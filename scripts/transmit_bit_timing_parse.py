import re
import os
from hat.utils.DataStorage import DataStorage

if __name__ == '__main__':
    # variables
    dirname = r"C:\Users\Public\ProbitBench_4.3.3.0\Sequences\Dali Dongle RX circuit\R4=2.74k"
    filename = "2023-01-27 11-58-33 Sequences 0002 IEC 62386-102 3.7 passed.xml"
    half_to_double = 600

    data = DataStorage()
    data.open("results.xlsx")

    actual_re = re.compile(r"Actual:\s(\d+)\s")

    with open(os.path.join(dirname, filename)) as f:
        for line in f:
            match = actual_re.search(line)
            if match:
                width = int(match.group(1))
                data["width"] = width
                if width > half_to_double:
                    data["double"] = True
                else:
                    data["double"] = False

                data.start_next_test(print_last=False)

    # calculation
    half = data._data[data._data["double"] == False]
    double = data._data[data._data["double"] == True]

    print("Half")
    print(half.describe())

    print("Double")
    print(double.describe())

    data.close()

    print("End")
