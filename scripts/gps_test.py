import asyncio
import os

from hat.comms.g4_interface_cli import G4
from hat.utils import utils

log = utils.create_logger(__name__)


async def make_gps_reset(label: str, conf: dict, wait_time: int, reset_type: str, hard_type: bool):
    log.info(f"Making reset {reset_type}, hard reset {hard_type}")
    g4 = conf["g4"]

    g4.flex_gps_reset(reset_type, hard_type)
    g4.flex_enter_gps_shell()

    log.info("Starting logging")
    g4._interface.add_log_file("gps", os.path.join(conf["test_dir"],
                                                   f"%Y%m%d_%H%M_{label}_gps_{reset_type}_{'hard' if hard_type else 'soft'}.ubx"))

    log.info(f"Waiting {wait_time} seconds to collect logs")
    await asyncio.sleep(wait_time)

    g4._interface.close_log_file("gps")
    log.info("Exiting GPS shell")
    g4.flex_quit_shell()


async def main(confs: dict, reset_types: list, hard_types: list, wait_time: int):
    log.info("Starting main function")

    log.info("Initialization of connection to G4")
    for label, conf in confs.items():
        g4 = G4(conf["COM_ADDR"], "SERIAL_THREAD", log_file=os.path.join(conf["test_dir"], f"%Y%m%d_%H%M_{label}.txt"))
        g4.set_prompt("$")
        g4.execute_command("quit")

        conf["g4"] = g4

    for reset_type, hard in zip(reset_types, hard_types):
        await asyncio.gather(
            *[make_gps_reset(label, conf, wait_time, reset_type, hard) for label, conf in confs.items()])

    log.info("End of main functions")


if __name__ == '__main__':
    log.info("Starting test")

    # variables
    reset_types = ["cold", "cold", "cold", "cold"]
    hard_types = [True, True, False, False]

    time_after_reset = 320  # seconds to wait after reset

    confs = {
        "MeshNode N": {
            "COM_ADDR": "COM72",
            "test_dir": "data"
        },

        "DataLift N": {
            "COM_ADDR": "COM217",
            "test_dir": "data"
        },

        "DataLift Z": {
            "COM_ADDR": "COM218",
            "test_dir": "data"
        },

        "MeshNode Z": {
            "COM_ADDR": "COM216",
            "test_dir": "data"
        }
    }

    asyncio.run(main(confs, reset_types, hard_types, time_after_reset))

    log.info("end of test")
