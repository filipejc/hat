from hat.comms import g4_interface_cli
import time
from MultimeterInstP import MultimeterInst
from hat.utils.DataStorage import DataStorage
from hat.drivers.DC_source import DCSource


def test_1_10v_flex_app(g4, mult):
    data = DataStorage("1_10v.xlsx")

    g4.flex_set_1_10v_start()

    for set_volt in range(0, 10001, 500):
        g4.flex_set_1_10v_voltage(set_volt)
        time.sleep(4)

        get_volt = g4.flex_get_1_10v_voltage()
        mult_volt = mult.get_voltage_dc()

        data["G4 Set voltage [V]"] = set_volt / 1000.0
        data["G4 Get voltage [V]"] = get_volt / 1000.0
        data["G4 Mult voltage [V]"] = mult_volt
        data["Diff Get and Set [V]"] = get_volt / 1000.0 - set_volt / 1000.0
        data["Diff Mult and Set [V]"] = mult_volt - set_volt / 1000.0
        data["Diff Mult and Get [V]"] = mult_volt - get_volt / 1000.0

        data.start_next_test()


def test_dali_on_1_10v(g4, mult, pwr):
    data = DataStorage("Dali_in_1_10v.xlsx")
    # power channel
    pwr_channel = "CH1"
    dali_channel = "CH2"
    pwr.set_voltage_dc(dali_channel, 20.5)
    sleep_time = 10  # seconds
    # case = "0V"
    # case = "10V"
    case = "dali"

    if case == "0V":
        g4.flex_set_1_10v_start()
        g4.flex_set_1_10v_voltage(0)
    elif case == "10V":
        g4.flex_set_1_10v_start()
        g4.flex_set_1_10v_voltage(10000)
    elif case == "default":
        g4.flex_set_1_10v_stop()
    elif case == "dali":
        g4.flex_set_1_10v_stop()
        g4.flex_dali_start()
    else:
        raise Exception(f"Case not supported {case}")

    for curr in range(10, 251, 20):
        print(f"Set current {curr}mA")
        pwr.set_current(dali_channel, curr / 1000.0)
        print(f"Sleeping for {sleep_time} seconds...")
        time.sleep(sleep_time)
        print(f"Get status")
        volt_1_10v = g4.flex_get_1_10v_voltage()
        pwr_volt = pwr.get_voltage_dc(dali_channel)
        # pwr_curr = pwr.get_current(dali_channel)
        pwr_status = pwr.get_operating_condition(dali_channel)
        mult_voltage = mult.get_voltage_dc()
        mult_curr = mult.get_current_dc()
        # print(f"Power supply: {pwr_volt}V, {mult_curr}A, status: {pwr_status}")

        time.sleep(1)

        # store data
        data["Limit current PSU [mA]"] = curr
        data["1-10V voltage PSU [V]"] = pwr_volt
        data["Current limiting"] = pwr_status["CC"]
        data["1_10v feedback G4 [V]"] = volt_1_10v / 1000.0
        data["1-10V Current [mA]"] = mult_curr * 1000.0
        data["Transistor Vce [V]"] = mult_voltage
        data["Transistor Power [mW]"] = mult_voltage * mult_curr / 1000.0

        # new line
        data.start_next_test()


if __name__ == "__main__":
    # connection
    mult_address = "USB0::0x05E6::0x6510::04472337::INSTR"
    pwr_address = "USB0::0x05E6::0x2230::802895020757510003::INSTR"

    address = "COM69"

    # connect instruments
    mult = MultimeterInst(mult_address)

    g4 = g4_interface_cli.G4(address)
    g4.set_flex_app_prompt()

    pwr = DCSource(pwr_address, reset=False)

    test_1_10v_flex_app(g4, mult)
    # test_dali_on_1_10v(g4, mult, pwr)

    print("end!")
