from AC_source import ACSource
from hat.comms.Zigbee_interface import SecoInterfaceTelnet
from hat.utils.DataStorage import DataStorage
from hat.comms.P7CM import P7CM
import pprint
import time

if __name__ == "__main__":
    # variables
    ACDC_serial = "ASRL51::INSTR"

    seco_ip = "192.168.1.74"
    step_time = 120
    sampling_time = 10


    # list of waveforms
    waveforms = [f"DST{x:02d}" for x in range(1, 31)]

    # devices
    devices_address = ["00:13:a2:00:41:78:b9:06"]
    p7cm_dict = []

    # connect
    print("Connect to AC source...")
    ac_source = ACSource(ACDC_serial, reset=False)
    ac_source.clear()

    print("Connecting to seco...")
    seco = SecoInterfaceTelnet(seco_ip)

    nodes = seco.get_nodes()
    print(f"Nodes: {pprint.pprint(nodes)}")

    for address in devices_address:
        execute_command = lambda cmd: seco.execute_command(cmd, address)
        device = P7CM(execute_command)

        aux_dict = {
            "address": address,
            "device": device,
            "data": DataStorage(f"{address.replace(':','')}.xlsx")
        }

        version = device.execute_command("version")
        print(f"Version: {version}")

        p7cm_dict.append(aux_dict)
        # data = p7cm[-1].get_sys03()
        # print(f"Data: {data}")

    for waveform in waveforms:
        start_time = time.time()
        ac_source.set_source_waveform("A", waveform)
        time.sleep(5)
        idx = 0
        while time.time() - start_time < step_time:
            for aux_dict in p7cm_dict:
                sys03 = aux_dict["device"].get_sys03()
                aux_dict["data"]["Index"] = idx
                aux_dict["data"]["Time"] = time.asctime()
                aux_dict["data"]["Waveform"] = waveform

                for key, value in sys03.items():
                    aux_dict["data"][key] = value

                aux_dict["data"].start_next_test()
                time.sleep(1)

            idx += 1
            print(f"Sleeping {sampling_time} seconds...")
            time.sleep(sampling_time)


    for aux_dict in p7cm_dict:
        aux_dict["data"].close()

    print("end!")
