import socket

# SOCK_STREAM is TCP connection
# SOCK_DGRAM is UDP connection
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host_ip = "52.13.98.241"
host_port = 82

server_address = (host_ip, host_port)

s.connect(server_address)

try:

    # Send data
    message = 'This is the message.  It will be repeated.'.encode("ascii")
    print('sending "%s"' % message)
    s.sendall(message)

    # Look for the response
    amount_received = 0
    amount_expected = len(message)

    while amount_received < amount_expected:
        data = s.recv(1024)
        amount_received += len(data)
        print('received "%s"' % data)

except KeyboardInterrupt:
    print("Keyboard interrupt end")
    s.close()

finally:
    print('closing socket')
    s.close()
