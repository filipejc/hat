"""
# Filename:     lwm2m_interface_test.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   10/01/2022
# Last Update:  10/01/2022
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""
from hat.comms.g4_interface_cli import G4


def main():
    url = "http://13.94.133.236:3000"
    dev_eui = "14efcf0111f0003a"
    g4 = G4(url, "LWM2M_SERVER", dev_eui=dev_eui)
    power = g4.lwm2m_get_active_power()
    print(f"Power: {power} W")

    eui = g4.lwm2m_get_eui()
    print(f"EUI: {eui}")

    microseconds = g4.lwm2m_get_microseconds()
    print(f"Microseconds: {microseconds}")

    g4.lwm2m_set_relay_planned_switch_time(microseconds + 1e6)

    microseconds = g4.lwm2m_get_relay_planned_switch_time()
    print(f"Microseconds: {microseconds}")
    # g4.set_relay_state(False, 5)

    relay_state = g4.lwm2m_get_relay_voltage()
    print(f"relay_state: {relay_state}")


def read_all():
    url = "http://13.94.133.236:3000"
    dev_eui = "14efcf0111f0003a"
    g4 = G4(url, "LWM2M_SERVER", dev_eui=dev_eui)

    for key, lwm2m_obj in g4.objs.objects.items():
        for lwm2m_key, lwm2m_resource in lwm2m_obj.resources.items():
            data = g4.lwm2m_read_raw(lwm2m_resource, 0)
            print(data)


if __name__ == '__main__':
    # main()
    read_all()
