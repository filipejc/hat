import sys

sys.path.append('C:\code')
sys.path.append('C:\code\hat')

import os
import hat.utils.DataStorage
import numpy as np
import time
import logging
from datetime import datetime
from hat.comms import g4_interface_cli
from hat.drivers import AcPowerMeterInstrument
from hat.drivers import AcSourceInstrument
from hat.drivers import ACDC_load
from hat.utils import utils
import matplotlib.pyplot as plt
from genericpath import exists
# from test_conditions import T7

def test_method():
    log.info("T7 Start")
    # test_condition = T7()
    ac = AcSourceInstrument.AcSourceInstrument("COM3", reset=False)
    log.info(f'C1 voltage: ')
    log.info("T7 End")

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
if not exists("./log"):
    os.mkdir("./log")
logging.basicConfig(filename = './log/{:%Y-%m-%d}.log'.format(datetime.now()),
                    filemode = "w+",
                    format = '%(asctime)s %(levelname)-8s %(message)s', 
                    level = logging.ERROR)
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)

# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")


if __name__ == "__main__":
    # test constants
    DEVICE = 'DATALIFT'
    VOLTAGE = 120
    MIN_VOLTAGE = 100
    MAX_VOLTAGE = 300
    TEST_CASE = 'T7'
    TEST_CASE_CONDITION = 'C1'

    I_MAX = 7.5
    TA = 2.5
    I_MIN = 0.15
    I_ST = 0.01
    DEFAULT_FREQUENCY = 60
    DEFAULT_POWER_FACTOR = 1
    PROCESSING_TIME = 0.9

    ### test parameters
    use_ac_load = True                  # to use AC load
    plot = True                         # make plot
    compensate_negative = False
    power_on_time = 10                  # number of second it takes to start
    source_stability_time = 5           # time to the source stability
    load_stability_time = 10            # time to the load stability
    use_flex_app = True

    #### connection strings
    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    g4_com_port = "COM3"
    ac_com_port = "ASRL5::INSTR"
    load_com_port = "ASRL4::INSTR"

    test_dict = {
        'T7': {
            'C1': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 12-PROCESSING_TIME
                'interval': 5.20-PROCESSING_TIME
            },
            'C2': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': -0.5,
                'trials': 12,
                # 'interval': 24-PROCESSING_TIME
                'interval': 10.40-PROCESSING_TIME
            },
            'C3': {
                'voltage': VOLTAGE,
                'current': TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': 0.5,
                'trials': 12,
                # 'interval': 24-PROCESSING_TIME
                'interval': 10.40-PROCESSING_TIME
            },
            'C4': {
                'voltage': VOLTAGE,
                'current': 0.1*TA,
                'frequency': DEFAULT_FREQUENCY,
                'power_factor': DEFAULT_POWER_FACTOR,
                'trials': 12,
                # 'interval': 120-PROCESSING_TIME
                'interval': 51.99-PROCESSING_TIME
            }
        }
    }


    ### voltage under test
    voltages = [test_dict[TEST_CASE][TEST_CASE_CONDITION]['voltage']]

    ### current under test
    # currents = np.arange(10.5, 15 + 0.5, 0.5)   # Current saturation test
    # currents = [2.50]                           # Current for Accuracy test at 120V
    # currents = [1.00]                           # Current for Accuracy test at 277V
    # currents = [0.15]                           # Current for Load Performance test of ANSIS
    currents = [test_dict[TEST_CASE][TEST_CASE_CONDITION]['current']]
    power_factor = test_dict[TEST_CASE][TEST_CASE_CONDITION]['power_factor']
    frequency = test_dict[TEST_CASE][TEST_CASE_CONDITION]['frequency']
    trials = test_dict[TEST_CASE][TEST_CASE_CONDITION]['trials']
    trials_interval = test_dict[TEST_CASE][TEST_CASE_CONDITION]['interval']

    max_current_peak = max(currents) * 2.0  # needs to account with crest factor
    filename = DEVICE+"_ANSI_C136_50_"+str(VOLTAGE)+"V_"+TEST_CASE+TEST_CASE_CONDITION+".xls"



    log.info("Starting test")

    ### connect to instruments and G4
    ac = AcSourceInstrument.AcSourceInstrument(ac_com_port, reset=False)

    log.info('Powering up the AC Source to supply the G4')
    ac.set_voltage_AC(voltages[0])
    ac.set_frequency(frequency)
    # ac.set_phase_on(60)
    # ac.set_phase_off(360)
    time.sleep(3)
    ac.set_output_state(True)
    time.sleep(5)


    # setting voltage and current
    for voltage in voltages:
        for current in currents:
            log.info(f"Testing voltage {voltage} V, current {current} A")

            log.info(f'Testing MIN VOLTAGE {MIN_VOLTAGE}')
            ac.set_voltage_AC(MIN_VOLTAGE)
            time.sleep(source_stability_time)

            for i in range(10):
                ac.set_output_state(False)
                time.sleep(0.1)
                ac.set_output_state(True)
                time.sleep(5)

            time.sleep(10)

            log.info(f'Testing MAX VOLTAGE {MAX_VOLTAGE}')
            ac.set_voltage_AC(MAX_VOLTAGE)
            time.sleep(source_stability_time)

            for i in range(10):
                ac.set_output_state(False)
                time.sleep(0.1)
                ac.set_output_state(True)
                time.sleep(5)

    ac.set_output_state(False)
    log.info('Test ended')