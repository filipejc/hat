class T7:
    def __init__(self, voltage:int=110, frequency:int=50, current_max:float=7.5, current_ta:float=2.5, Kh:float=1):
        self._Kh = Kh
        self.test_time = (3600*self._Kh*5)/(voltage*current_ta*power_factor)

class T6:
    def __init__(self, voltage:int=110, frequency:int=50, current_max:float=6, current_ta:float=2.5, Kh:float=1):
        self._Kh = Kh
        self._voltage = voltage
        self._frequency = frequency
        self._current_max = current_max
        self._current_ta = current_ta
        self._conditions_dict = {
            '1': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.1,
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 11
            },
            '2': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*1,
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 11
            },
            '3': {
                'voltage': int(self._voltage),
                'current': float(self._current_max),
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 11
            },
            '4': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.2,
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 11
            },
            '5': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta),
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 11
            },
            '6': {
                'voltage': int(self._voltage),
                'current': 4, # measurement limited by CR on Owlet IV of the load to execute a PF of -0.5
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 11
            },
            '7': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.2,
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 11
            },
            '8': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*1,
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 11
            },
            '9': {
                'voltage': int(self._voltage),
                'current': 4, # measurement limited by CR on Owlet IV of the load to execute a PF of -0.5
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 11
            },
            '10': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.1,
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 16
            },
            '11': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*1,
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 16
            },
            '12': {
                'voltage': int(self._voltage),
                'current': float(self._current_max),
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 16
            },
            '13': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.2,
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 16
            },
            '14': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta),
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 16
            },
            '15': {
                'voltage': int(self._voltage),
                'current': 4,
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 16
            },
            '16': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.2,
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 16
            },
            '17': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*1,
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 16
            },
            '18': {
                'voltage': int(self._voltage),
                'current': 4, #float(self._current_max),
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 16
            },
            '19': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.1,
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 16
            },
            '20': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*1,
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 16
            },
            '21': {
                'voltage': int(self._voltage),
                'current': float(self._current_max),
                'frequency': int(self._frequency),
                'power_factor': 1,
                'trials': 16
            },
            '22': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.2,
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 16
            },
            '23': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta),
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 16
            },
            '24': {
                'voltage': int(self._voltage),
                'current': 4,
                'frequency': int(self._frequency),
                'power_factor': -0.5,
                'trials': 16
            },
            '25': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*0.2,
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 16
            },
            '26': {
                'voltage': int(self._voltage),
                'current': float(self._current_ta)*1,
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 16
            },
            '27': {
                'voltage': int(self._voltage),
                'current': 4, #float(self._current_max),
                'frequency': int(self._frequency),
                'power_factor': +0.5,
                'trials': 16
            }
        }

    def get_condition_voltage(self, condition:str=1):
        try:
            return self._conditions_dict[condition]['voltage']
        except Exception as e:
            print(e)

    def get_condition_current(self, condition:str=1):
        try:
            return self._conditions_dict[condition]['current']
        except Exception as e:
            print(e)  

    def get_condition_frequency(self, condition:str=1):
        try:
            return self._conditions_dict[condition]['frequency']
        except Exception as e:
            print(e)  

    def get_condition_power_factor(self, condition:str=1):
        try:
            return self._conditions_dict[condition]['power_factor']
        except Exception as e:
            print(e)         

    def get_condition_trials(self, condition:str=1):
        try:
            return self._conditions_dict[condition]['trials']
        except Exception as e:
            print(e)      
            
    def get_condition_test_time(self, condition:str=1):
        try:
            voltage = self._conditions_dict[condition]['voltage']
            current = self._conditions_dict[condition]['current']
            power_factor = self._conditions_dict[condition]['power_factor']
            trials = self._conditions_dict[condition]['trials']
            test_time = abs((3600*self._Kh*trials)/(voltage*current*power_factor))
            return test_time
        except Exception as e:
            print(e)


    # test_dict = {
    #     'T7': {
    #         'C1': {
    #             'voltage': VOLTAGE,
    #             'current': TA,
    #             'frequency': DEFAULT_FREQUENCY,
    #             'power_factor': DEFAULT_POWER_FACTOR,
    #             'trials': 12,
    #             # 'interval': 12-PROCESSING_TIME
    #             'interval': 5.20-PROCESSING_TIME
    #         },