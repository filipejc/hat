import sys

# sys.path.append('C:\code')
# sys.path.append('C:\code\hat')

import os
import hat.utils.DataStorage
import numpy as np
import time
import logging
from datetime import datetime
from hat.comms import g4_interface_cli
from hat.drivers import AcPowerMeterInstrument
from hat.drivers import AcSourceInstrument
from hat.drivers import ACDC_load
from hat.utils import utils
import matplotlib.pyplot as plt
from genericpath import exists
from .test_conditions import T6


log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
if not exists("./log"):
    os.mkdir("./log")
logging.basicConfig(filename = './log/{:%Y-%m-%d}.log'.format(datetime.now()),
                    filemode = "w+",
                    format = '%(asctime)s %(levelname)-8s %(message)s', 
                    level = logging.ERROR)
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)

# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")


def test_method(voltage:int):
    log.info("Test #6: Effect of Variation of Ambient Temperature")

    ansi = T6(voltage)

    log.info("Please, specify the Condition (ex. Condition (1) = 1 and so on):")
    test_condition = input()
    log.info(f'Condition selected: C{test_condition}')

    ### condition parameters
    cond_voltage = ansi.get_condition_voltage(test_condition)
    cond_current = ansi.get_condition_current(test_condition)
    cond_frequency = ansi.get_condition_frequency(test_condition)
    cond_power_factor = ansi.get_condition_power_factor(test_condition)
    cond_trials = ansi.get_condition_trials(test_condition)
    cond_test_time = ansi.get_condition_test_time(test_condition)
    cond_trials_interval = cond_test_time/cond_trials

    log.info(f'C{test_condition} voltage: {cond_voltage} VAC')
    log.info(f'C{test_condition} current: {cond_current} A')
    log.info(f'C{test_condition} frequency: {cond_frequency} Hz')
    log.info(f'C{test_condition} power factor: {cond_power_factor}')
    log.info(f'C{test_condition} trials: {cond_trials}')
    log.info(f'C{test_condition} trials interval: {cond_trials_interval}')
    log.info(f'C{test_condition} test time: {cond_test_time} seconds ({cond_test_time/60} minutes)')
    # quit()

    ### test parameters
    DEVICE = 'MESH'
    use_ac_load = True                  # to use AC load
    plot = True                         # make plot
    compensate_negative = False
    processing_time = 0.9
    power_on_time = 3                  # number of second it takes to start
    source_stability_time = 5           # time to the source stability
    load_stability_time = 10            # time to the load stability
    max_current_peak = cond_current * 4.0  # needs to account with crest factor
    filename = DEVICE+"_ANSI_C136_50_"+str(cond_voltage)+"V_"+"T6_C"+test_condition+".xls" 

    ### connection strings
    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"
    g4_com_port = "COM234"
    ac_com_port = "ASRL51::INSTR"
    load_com_port = "ASRL209::INSTR"

    log.info("Starting test")

    ### connect to instruments and G4
    log.info("Connecting to AC Source ...")
    ac = AcSourceInstrument.AcSourceInstrument(ac_com_port, reset=False)
    log.info("AC source connected")

    if use_ac_load:
        log.info("Connecting to ACDC load ...")
        load = ACDC_load.ACDCLoad(load_com_port, reset=False)
        log.info("ACDC load connected")

    pm = AcPowerMeterInstrument.AcPowerMeterInstrument(USB_connection_string)
    pm.set_integrator_mode()
    pm.add_meas_active_power()
    pm.add_meas_integration_time()
    pm.conf_integrator_manual_mode()
    pm.reset_integrator()

    log.debug(f'PM1000+ - mode: {pm.get_current_mode()}')
    log.debug(f"PM1000+ - current measurements: {pm.configured_measurements()}")
    # quit()

    log.info('Powering up the AC Source to supply the G4')
    ac.set_voltage_AC(cond_voltage)
    ac.set_frequency(cond_frequency)
    # ac.set_phase_on(60)
    # ac.set_phase_off(360)
    time.sleep(3)
    ac.set_output_state(True)
    time.sleep(0.8)
    pm.start_integrator()
    time.sleep(5)

    g4 = g4_interface_cli.G4(g4_com_port, interface_type="SERIAL_THREAD")
    g4.set_prompt("$")
    g4.write("system-reset\n")
    time.sleep(5)

    log.info('G4 setting relay true')
    g4.flex_set_relay(True)

    ## create data structure
    data = hat.utils.DataStorage.DataStorage()
    data.open(filename)

    print(f"Waiting {power_on_time} seconds, for load voltage detection")
    time.sleep(power_on_time)

    ### configure load
    if use_ac_load:
        log.info('Configuring AC Load')
        load.set_load_mode("CURR")
        load.clear_protection_status()
        load.set_current_AC(0)
        load.set_peak_current_AC(max_current_peak)
        load.set_power_factor(cond_power_factor)
        load.set_crest_factor(1.414)
        load.get_power_factor()
        load.get_crest_factor()
        load.set_current_AC(cond_current)
        time.sleep(3)
        log.info('Powering Up AC Load')
        load.set_output_state(True)
        time.sleep(load_stability_time)

    ### First sampling for differential calculations
    meas = pm.get_measurement() # read from PM1000
    measuring_data = g4.flex_get_meter_data() # read from G4

    ### parsing first measurement data
    for key, value in meas.items():
        if key == 'Whr':
            first_pm_energy = np.abs(value) if compensate_negative else value

    first_g4_energy = measuring_data["energy"]

    log.debug(f'Initial Energy Measured on G4: {first_g4_energy} Wh')
    log.debug(f'Initial Energy Measured on PM1000: {first_pm_energy} Wh')
    log.debug('Those initial energy measurements will be reduced from the subsequent measurements of the respective devices')

    ### test runs
    for i in range(cond_trials):
        data["trial"] = i + 1
        log.info(f"Starting trial {i + 1}")

        meas = pm.get_measurement() # read from PM1000
        measuring_data = g4.flex_get_meter_data() # read from G4

        ### parsing measurement data
        for key, value in meas.items():
            if key == 'Whr':
                data[f"PM {key}"] = (np.abs(value) - first_pm_energy) if compensate_negative else (value - first_pm_energy)
            else:
                data[f"PM {key}"] = np.abs(value) if compensate_negative else value
        data["PM Sec"] = 3600*data["PM Hrs"]

        data["G4 voltage [V]"] = measuring_data["voltage"]
        data["G4 current [A]"] = measuring_data["current"]
        data["G4 active power [W]"] = measuring_data["power"]
        data["G4 active energy [Wh]"] = measuring_data["energy"] - first_g4_energy
        data["G4 time stamp [s]"] = measuring_data["time_stamp"]

        data["Set Voltage [V]"] = cond_voltage
        data["Set Current [A]"] = cond_current

        data.start_next_test()
        if i < cond_trials-1:
            log.debug(f"Waiting trial interval ({cond_trials_interval} seconds)...")
            time.sleep(cond_trials_interval - processing_time)

    data.save()

    # teardown
    if use_ac_load:
        load.set_output_state(False)
    
    ac.set_output_state(False)
    pm.stop_integrator()

    if plot:
        plt.plot(data["PM Whr"], data["G4 active energy [Wh]"], "-go", label="Active Energy")
        plt.legend("Active Energy")
        plt.xlabel("Reference Active Energy [Wh]")
        plt.ylabel("G4 Active Energy [Wh]")
        # plt.title("G4 Active Energy vs Reference Active Energy Meter [Wh]")
        plt.grid(True)
        plt.show()

    del g4

    log.info("Ending Test")




# if __name__ == "__main__":
    