"""
# Filename:     MyRobotLib.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20/04/2021
# Last Update:  20/04/2021
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""


class MyRobotLib:
    def __init__(self, argument1, argument2):
        print(f"Arg 1: {argument1}, Arg 2: {argument2}")

    def make_test_p7cm(self):
        """
        This is a documentation test
        """
        print("Make testing.")
