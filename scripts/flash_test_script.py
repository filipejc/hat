import os
from hat.drivers.AcSourceInstrument import AcSourceInstrument
from hat.drivers.DCload import DCload
from hat.comms import g4_interface_cli
from hat.utils import utils
import time

log = utils.create_logger(__name__)


def main():
    # voltage
    voltage = 230
    frequency = 50

    # configurations
    wakeup_time = 90  # time to wait after turn on PSU
    max_discharge_time = 600  # maximum time to wait for supercaps to discharge
    off_time = 60  # number of seconds with AC power off after MCU dies
    n_tests = 10000
    # number of test to perform
    accelerate_discharge = False  # if true, DC electronic load will be used to discharge more quickly the supercaps.
    minimum_supercap_voltage = 2.500  # voltage to where the Electronic load is active
    load_current = 0.2

    # address
    ac_source_addr = "ASRL40::INSTR"
    dc_load = "USB0::0x2EC7::0x8800::802197042757210087::INSTR"
    test_dir = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\08_Support\20211130_Flash_problem\Test_logs\20220105_test_logs"
    # variables
    confs = {
        "NEMA LTE 1": {
            "com_port": "COM29",
            "logfile": os.path.join(test_dir, "%Y%m%d_%H%M_NEMA_LTE_1.txt")
        },
        "NEMA LTE 2": {
            "com_port": "COM245",
            "logfile": os.path.join(test_dir, "%Y%m%d_%H%M_NEMA_LTE_2.txt")
        },
        "NEMA LTE 3": {
            "com_port": "COM248",
            "logfile": os.path.join(test_dir,"%Y%m%d_%H%M_NEMA_LTE_3.txt")
        }
    }


    # configurations
    log.info("Connecting to instruments")
    ac = AcSourceInstrument(ac_source_addr)
    dc_load = DCload(dc_load)

    for label, conf in confs.items():
        g4 = g4_interface_cli.G4(conf["com_port"], interface_type="SERIAL_THREAD", log_file=conf["logfile"])
        confs[label]["g4"] = g4

    # configure AC source
    log.info(f"Set voltage {voltage}, frequency {frequency}")
    ac.set_voltage_AC(voltage)
    ac.set_frequency(frequency)

    # # configure DC load
    # dc_load.set_load_mode("CURR")
    # dc_load.set_output_state(False)

    for loop in range(n_tests):
        log.info(f"Staring loop: {loop}")
        # step 1 - turn on
        log.info(f"Turn AC state on")
        ac.set_output_state(True)

        for label, conf in confs.items():
            confs[label]["Startup"] = False
            confs[label]["Dead"] = False

        # step 2 - wait for startup
        log.info(f"Waiting for startup {wakeup_time}s")
        time.sleep(wakeup_time)

        # sys_info = g4.sys_info()
        # print(f"Sys info:\n{sys_info}")
        startup = True
        for label, conf in confs.items():
            g4 = conf["g4"]
            for i in range(10):
                try:
                    version = g4.lwm2m_get_hw_version()
                    log.info(f"{label}: HW version: {version}")
                    confs[label]["Startup"] = True
                    break
                except Exception as e:
                    print(f"error: {e}")
                    time.sleep(5)

            startup = startup and confs[label]["Startup"]

        if startup:
            log.info("All devices startup")
        else:
            log.warning("Some devices didnt start")

        # step 3 - turn off power supply
        log.info(f"Turn off AC power supply")
        ac.set_output_state(False)
        start_time = time.time()

        load_status = True  # store status of DC load
        # check if accelerated discharge
        if accelerate_discharge:
            log.info(f"Configuring accelerated discharge with current {load_current}")
            dc_load.set_current_DC(load_current)
            dc_load.set_output_state(True)


        # step 4 - wait until supercaps are discharged
        while time.time() - start_time < max_discharge_time:
            # check Device status
            all_dead = True
            for label, conf in confs.items():
                if confs[label]["Dead"]:
                    continue
                g4 = conf["g4"]
                try:
                    voltage = g4.get_supercap_voltage()
                    log.info(f"{label} G4: Supercap voltage {voltage}V")
                    confs[label]["Dead"] = False
                    all_dead = False
                except Exception as e:
                    log.info(f"{label}: Assuming device is dead")
                    confs[label]["Dead"] = True

            if all_dead:
                log.info("All devices dead, stopping")
                break
            # check status of DC load
            # dc_load_voltage = dc_load.get_measured_voltage_DC()
            # log.info(f"DC load: supercap voltage: {dc_load_voltage}V")
        #     if accelerate_discharge and dc_load_voltage < minimum_supercap_voltage:
        #         if load_status:
        #             log.info("Below minimum voltage, turning load off...")
        #             dc_load.set_output_state(False)
        #             load_status = False
        #
        # if accelerate_discharge and load_status:
        #     log.info("Below minimum voltage, turning load off...")
        #     dc_load.set_output_state(False)
        #     load_status = False

        # step 5 - wait before starting loop again
        log.info(f"Waiting {off_time} seconds to discharge")
        time.sleep(off_time)

        if accelerate_discharge:
            log.info("End of cycle, turn off load")
            dc_load.set_output_state(False)

    # step 6 - going back to beginning

    log.info("end!")


if __name__ == "__main__":
    main()
    print("end!")
