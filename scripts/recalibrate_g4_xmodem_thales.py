from hat.comms.g4_interface_cli import G4
import re
import os
import pandas as pd
import json
import time
import sys
from hat.utils import programmer
import pygame

# Not used, as each device has their own certificates
CFG_KEY_CLIENT_CERT = '193'
CFG_KEY_CLIENT_PVKEY = '8'
# Thales Certificates (26 Nov 2021)
CFG_KEY_SUBCA_CERT = '221'
CFG_KEY_ROOT_CERT = '171'

ORIGINAL_EUI_COLUMN = 2
CONVERTED_EUI_COLUMN = 3


def helper():
    print("Usage:        ./recalibrate_g4_xmodem_thales.py [socket_type] [controller_type] [production_phase] [operation_type] [configuration_type|optional] [device_id|optional]")
    print("Socket Types:          [NEMA] | [ZHAGA]")
    print("Controller Types:      [LIFT] | [MESH]")
    print("Production Phases:     [eng_run] | [phase_0] | [phase_1]")
    print("Operation Types:       [full] | [recalibrate] | [flash]")
    print("Configuration Types:   [tst_fluvius] | [fluvius] | [tst] | [prod]")
    print("     [full]          - Install Firmwares + Calibrate + Certificates")
    print("     [recalibrate]   - Calibrate + Certificates Only")
    print("Device ID:             [14EFCF2000000000]")
    print("e.g.:")
    print("     ./recalibrate_g4_xmodem_thales.py NEMA LIFT eng_run full")
    print("     ./recalibrate_g4_xmodem_thales.py ZHAGA MESH phase_0 recalibrate tst")
    print("     ./recalibrate_g4_xmodem_thales.py ZHAGA LIFT eng_run recalibrate tst_fluvius 14EFCF18FFFF0001")
    print("     ./recalibrate_g4_xmodem_thales.py ZHAGA LIFT eng_run report fluvius 14EFCF18FFFF0001")


def get_unused_device_id(socket_type, controller_type, production_phase, already_used_EUIs):
    new_device_id = "14efcf01"

    if socket_type == "NEMA":
        new_device_id += '1'
    elif socket_type == "ZHAGA":
        new_device_id += '2'
    else:
        raise Exception (f'Unknown Socket Type: {socket_type}')

    if controller_type == "LIFT":
        new_device_id += '1'
    elif controller_type == "MESH":
        new_device_id += 'a'
    else:
        raise Exception (f'Unknown Controller Type: {controller_type}')

    if production_phase == "eng_run":
        new_device_id += 'f'
    elif production_phase == "phase_0" or production_phase == "phase_1":
        new_device_id += '0'
    else:
        raise Exception(f'Unknown Phase: {production_phase}')

    # Now we need to go through all device IDs from 0 until we find the first that hasn't been used
    for i in range(0, 1000):
        next_device_id = new_device_id + hex(i)[2:].zfill(5)
        if next_device_id not in already_used_EUIs.T.values[CONVERTED_EUI_COLUMN]:
            # Found an EUI which hasn't been used in the EUI_convertion.csv
            new_device_id = next_device_id
            break
        else:
            continue # searching for next EUI that hasn't been used

    if len(new_device_id) != 16:
        raise Exception(f'Newly created Device ID "{new_device_id}" has an invalid Length: {len(new_device_id)}')

    return new_device_id


def update_factory_configuration (factory_config, cal_data, pvt_factory_config=0):
    """
    This function runs through the factory configurations in factory_config and updates them with the values in cal_data
    Returns the updated factory_config, ready to be saved to a json file
    """

    i = 0

    for config in factory_config["configs"]:
        if "CFG_KEY_LIFT" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_LIFT']
        elif "CFG_KEY_NEMA" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_NEMA']
        elif "CFG_KEY_RELAY_OFF_DELAY" in config["args"].values():
            if pvt_factory_config == 0:
                factory_config["configs"][i]["args"]["value"] = str(cal_data['relay_off'])
            else:
                factory_config["configs"][i]["args"]["value"] = pvt_factory_config["configs"][i]["args"]["value"]
        elif "CFG_KEY_RELAY_ON_DELAY" in config["args"].values():
            if pvt_factory_config == 0:
                factory_config["configs"][i]["args"]["value"] = str(cal_data['relay_on'])
            else:
                factory_config["configs"][i]["args"]["value"] = pvt_factory_config["configs"][i]["args"]["value"]
        elif "CFG_KEY_DEV_EUI" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['EUI']
        elif "CFG_KEY_STPM_CORRECT_CURRENT" in config["args"].values():
            if pvt_factory_config == 0:
                factory_config["configs"][i]["args"]["value"] = str(cal_data['current_factor'])
            else:
                factory_config["configs"][i]["args"]["value"] = pvt_factory_config["configs"][i]["args"]["value"]
        elif "CFG_KEY_STPM_CORRECT_VOLTAGE" in config["args"].values():
            if pvt_factory_config == 0:
                factory_config["configs"][i]["args"]["value"] = str(cal_data['volt_factor'])
            else:
                factory_config["configs"][i]["args"]["value"] = pvt_factory_config["configs"][i]["args"]["value"]
        elif "CFG_KEY_HW_VERSION" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['hw_version']

        elif "CFG_KEY_CLIENT_CERT" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_CLIENT_CERT']
        elif "CFG_KEY_CLIENT_PVKEY" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_CLIENT_PVKEY']
        elif "CFG_KEY_SUBCA_CERT" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_SUBCA_CERT']
        elif "CFG_KEY_ROOT_CERT" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_ROOT_CERT']
        elif "CFG_KEY_SELF_TEST_COUNTER" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_SELF_TEST_COUNTER']
        elif "CFG_KEY_PHOTOCELL_USAGE" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_PHOTOCELL_USAGE']
        elif "CFG_KEY_BOOTSTRAP_URI_IPV4" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_BOOTSTRAP_URI_IPV4']
        elif "CFG_KEY_BOOTSTRAP_URI_IPV6" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_BOOTSTRAP_URI_IPV6']
        elif "CFG_KEY_TUNNEL_URI" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_TUNNEL_URI']
        elif "CFG_KEY_NTP_URI_IPV6" in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_NTP_URI_IPV6']
        elif "CFG_KEY_NTP_URI_IPV4" not in config["args"].values():
            factory_config["configs"][i]["args"]["value"] = cal_data['CFG_KEY_NTP_URI_IPV4']

        i += 1

    return factory_config


def verify_configurations(g4, operation_type, device_id, configuration_type, mdm_result="empty"):
    Success = True
    if configuration_type == None:
        filename = f"calibration\\{operation_type}_{device_id}_prod_complete"
    else:
        filename = f"calibration\\{operation_type}_{device_id}_{configuration_type}_complete"


    with open(filename, "w") as completion_file:
        g4.flush_input()
        g4.set_global_loglevel(4)

        file_output = ''

        # Validate certificates in Flash
        #client_cert = g4.lwm2m_read_cert('Client certifcate')
        client_cert = g4.execute_command_slow('lwm2m read factory-cfg 0 client_cert')
        # if CFG_KEY_CLIENT_CERT not in client_cert:
        if ' 0' in client_cert:
            Success = False
            print("Invalid Client Certificate!!!")
        #root_cert = g4.lwm2m_read_cert('Root certifcate')
        root_cert = g4.execute_command_slow('lwm2m read factory-cfg 0 root_cert')
        if CFG_KEY_ROOT_CERT not in root_cert:
            Success = False
            print("Invalid Root Certificate!!!")
        #subca_cert = g4.lwm2m_read_cert('subCA certificate')
        subca_cert = g4.execute_command_slow('lwm2m read factory-cfg 0 subca_cert')
        if CFG_KEY_SUBCA_CERT not in subca_cert:
            Success = False
            print("Invalid SubCA Certificate!!!")
        #client_pvkey = g4.lwm2m_read_cert('Client private key')
        client_pvkey = g4.execute_command_slow('lwm2m read factory-cfg 0 client_pvkey')
        
        self_test = g4.execute_command_slow("lwm2m read factory-cfg 0 self_test_counter")
        photocell = g4.execute_command_slow("lwm2m read factory-cfg 0 photocell_usage")
        bootstrap_ipv4 = g4.execute_command_slow("lwm2m read factory-cfg 0 bootstrap_uri_ipv4")
        tunnel = g4.execute_command_slow("lwm2m read factory-cfg 0 tunnel_uri")
        ntp_ipv4 = g4.execute_command_slow("lwm2m read factory-cfg 0 ntp_uri_ipv4")
        
        # if CFG_KEY_CLIENT_PVKEY not in client_pvkey:
        if ' 0' in client_pvkey:
            Success = False
            print("Invalid Private Key!!!")
        factory_cfg_persisted = g4.execute_command_slow('lwm2m read factory-cfg 0 persisted')
        if 'false' in factory_cfg_persisted:
            Success = False
            print("Factory config not applied")

        file_output += "Client Certificate XOR:" + client_cert
        file_output += "\nRoot Certificate XOR:" + root_cert
        file_output += "\nSubCA Certificate XOR:" + subca_cert
        file_output += "\nPrivate Key XOR:" + client_pvkey
        file_output += "\nSelf Test Counter:" + self_test
        file_output += "\nPhotocell:" + photocell
        file_output += "\nBootstrap:" + bootstrap_ipv4
        file_output += "\nTunnel:" + tunnel
        file_output += "\nNTP:" + ntp_ipv4

        file_output += "\n" + g4.sys_info()

        if mdm_result == "False":
            Success = False
            print('mdm radio 10 not applied!\n')
        elif mdm_result == "True":
            file_output += "\nmdm radio 10 applied!\n"

        if device_id not in file_output:
            Success = False
            print(f'Wrong deviceID, expected: {device_id}, current: {file_output}, FAILED!\n')

        if config["STM_FW"] not in file_output:
            Success = False
            print(f'Wrong MCU FW loaded, expected: { config["STM_FW"] }, current: {file_output}, FAILED!\n')

        if config["EFR_FW"] not in file_output and "EFR32 FW version: NOT AVAILABLE" not in file_output:
            Success = False
            print(f'Wrong EFR32 FW loaded, expected: { config["EFR_FW"] }, current: {file_output}, FAILED!\n')

        if Success == True:
            file_output += '\n-----------------------------'
            file_output += '\nComplete, PASSED!'
            file_output += f'\nProcess Time to {operation_type} : {time.time() - start_time}'
            file_output += '\n-----------------------------\n'
            pygame.init()
            pygame.mixer.init()
            finish_sound = pygame.mixer.Sound('ta_pronto.wav')
            finish_sound.play()
        else:
            file_output += '-----------------------------'
            file_output += 'Complete, FAILED!'
            file_output += f'Process Time to {operation_type} : {time.time() - start_time}'
            file_output += '-----------------------------\n'
            pygame.init()
            pygame.mixer.init()
            finish_sound = pygame.mixer.Sound('ta_pronto.wav')
            finish_sound.play()

        completion_file.write(file_output)
        completion_file.close()

    return Success


if __name__ == "__main__":

    start_time = time.time()

    # This variable shall be set to True whenever a new EUI from the certificates list has ben used
    new_EUI_burned = False
    # In case a new EUI is used from the certificates list, we need to keep track of the original one
    old_device_id = ''
    # Overall success status of the process
    Success = True

    if len(sys.argv) < 6:
        print("Please verify input Parameters")
        helper()
        exit()
    else:
        if "NEMA" not in sys.argv[1] and "ZHAGA" not in sys.argv[1]:
            print("Unknown Socket Type:", sys.argv[1])
            helper()
            exit()
        if "LIFT" not in sys.argv[2] and "MESH" not in sys.argv[2]:
            print("Unknown Controller Type:", sys.argv[2])
            helper()
            exit()
        if "eng_run" not in sys.argv[3] and "phase_0" not in sys.argv[3] and "phase_1" not in sys.argv[3]:
            print("Unknown Production Phase:", sys.argv[3])
            helper()
            exit()
        if "full" not in sys.argv[4] and "recalibrate" not in sys.argv[4] and "flash" not in sys.argv[4] and "report" not in sys.argv[4]:
            print("Unknown Operation Type:", sys.argv[4])
            helper()
            exit()
        

    socket_type = sys.argv[1]
    controller_type = sys.argv[2]
    production_phase = sys.argv[3]
    operation_type = sys.argv[4]
    configuration_type = None
    device_id = None
    
    if len(sys.argv) >= 6:
        if "tst_fluvius" not in sys.argv[5] and "tst" not in sys.argv[5] and "fluvius" not in sys.argv[5] and "prod" not in sys.argv[5]:
            print("Unknown Configuration Type:", sys.argv[5])
            helper()
            exit()
        else:
            configuration_type = sys.argv[5]
    if len(sys.argv) == 7: # Device ID was provided:
        if len(sys.argv[6]) != 16:
            print("Invalid Device ID :", sys.argv[6])
            print("\n Device ID must Have 16 Alphanumeric Characters. e.g. 14EFCF18FFFF0001\n")
            exit()
        else:
            device_id = sys.argv[6]

        # from here onwards, we only use factory_config settings

    if operation_type == "full":
        program_flex_app = False
        install_efr_complete_FW = False
        # install_efr_bootloader = True
        install_stm = True
        factory_wipe = True
        set_data = True
        set_exedra_tst_bootstrap = False
        set_mesh_network = False

    elif operation_type == "full_mesh":
        program_flex_app = False
        install_efr_complete_FW = True
        # install_efr_bootloader = True
        install_stm = True
        factory_wipe = True
        set_data = True
        set_exedra_tst_bootstrap = False
        set_mesh_network = True

    elif operation_type == "flash":
        program_flex_app = False
        install_efr_complete_FW = False
        # install_efr_bootloader = True
        install_stm = True
        factory_wipe = True
        set_data = True
        set_exedra_tst_bootstrap = False
        set_mesh_network = False

    elif operation_type == "recalibrate":
        program_flex_app = False
        install_efr_complete_FW = False
        # install_efr_bootloader = False
        install_stm = False
        factory_wipe = True
        set_data = True
        set_exedra_tst_bootstrap = False
        set_mesh_network = False
    
    elif operation_type == "report":
        program_flex_app = False
        install_efr_complete_FW = False
        # install_efr_bootloader = False
        install_stm = False
        factory_wipe = False
        if len(sys.argv[6]) != 16:
            print("Invalid Device ID :", sys.argv[6])
            print("\n Device ID must Have 16 Alphanumeric Characters. e.g. 14EFCF18FFFF0001\n")
            exit()
        else:
            set_data = True
            device_id = sys.argv[6]
        set_exedra_tst_bootstrap = False
        set_mesh_network = False
    else:
        raise Exception("Invalid operation_type! FAILED!")
        
    #check configuration Type       
    if configuration_type == "fluvius":
        set_tst = False
        set_fluvius = True
        
    elif configuration_type == "tst":
        set_fluvius = False
        set_tst = True
        
    elif configuration_type == "tst_fluvius":
        set_fluvius = True
        set_tst = True

    elif configuration_type == "prod":
        set_fluvius = False
        set_tst = False

    else:
        set_fluvius = False
        set_tst = False
        
    # Open config file with local environment settings
    with open("recalibrate_g4_config.json") as f:
        config = json.load(f)

        #   STM and EFR Firmware Packages:
    # if socket_type == "ZHAGA" and controller_type == "LIFT":
    #     # TCXO files
    #     efr_complete_FW = config["efr_complete_FW"]
    #     stm_hex = config["stm_hex"]
    # else:
    #     # No-TCXO Files
    #     efr_complete_FW = config["efr_complete_FW_NO_TCXO"]
    #     stm_hex = config["stm_hex_NO_TCXO"]
    # efr_complete_FW = config["efr_complete_FW"]
    # stm_hex = config["stm_hex"]

    # Validate files:
    if not os.path.isfile(config["stm_exe"]):
        raise Exception(f'Cannot find {config["stm_exe"]}')
    elif not os.path.isfile(config["efr_exe"]):
        raise Exception(f'Cannot find {config["efr_exe"]}')
    elif not os.path.isfile(config["flex_app_hex"]):
        raise Exception(f'Cannot find {config["flex_app_hex"]}')
    elif not os.path.isfile(config["efr_complete_FW"]):
        raise Exception(f'Cannot find {config["efr_complete_FW"]}')
    # elif not os.path.isfile(config["efr_bootloader_FW"]):
    #     raise Exception(f'Cannot find {config["efr_bootloader_FW"]}')
    elif not os.path.isfile(config["stm_hex"]):
        raise Exception(f'Cannot find {config["stm_hex"]}')

    # Open factory config file with local environment settings
    with open("factory_config_xmodem.json") as f:
        factory_config = json.load(f)

    # Open G4 CLI Port:
    g4 = G4(config["com_port"])

    g4.set_global_loglevel(4)

    factory_config_filename=""

    # Get Data from Device if Device ID is not provided
    if device_id is None:
        # Get EUI, volt_factor, current_factor, relay_on, relay_off, hw_version
        cal_data = g4.get_cal_data()
        if "hw_version" not in cal_data.keys(): # No Hw version successfully read from device
            cal_data["hw_version"] = "4.0"
        elif len(cal_data["hw_version"]) > 3: # Old Hw version successfully read from device
            cal_data["hw_version"] = "4.0"

        # Add socket type configuration to cal_data
        if socket_type == "NEMA":
            cal_data["CFG_KEY_NEMA"] = "true"
        else:
            cal_data["CFG_KEY_NEMA"] = "false"
        # Add controller type configuration to cal_data
        if controller_type == "LIFT":
            cal_data["CFG_KEY_LIFT"] = "true"
        else:
            cal_data["CFG_KEY_LIFT"] = "false"

        if set_fluvius:
            cal_data["CFG_KEY_SELF_TEST_COUNTER"] = "0"
        else:
            cal_data["CFG_KEY_SELF_TEST_COUNTER"] = "5"

        if set_tst and controller_type == "LIFT":
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV4"] = "coap://10.100.99.1:5683"
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV6"] = "coap://[fdf7:a36c:4641:139::f1]:5683"
            cal_data["CFG_KEY_TUNNEL_URI"] = "10.100.193.5"
            cal_data["CFG_KEY_NTP_URI_IPV4"] = "10.100.100.1"
            cal_data["CFG_KEY_NTP_URI_IPV6"] = "fdf7:a36c:4641:139::f1"
        elif set_tst and controller_type == "MESH":
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV4"] = ""
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV6"] = "coap://[fdf7:a36c:4641:139::f1]:5683"
            cal_data["CFG_KEY_TUNNEL_URI"] = "10.100.193.5"
            cal_data["CFG_KEY_NTP_URI_IPV6"] = "fdf7:a36c:4641:139::f1"
            cal_data["CFG_KEY_NTP_URI_IPV4"] = ""
        elif (not set_tst) and controller_type == "LIFT":
            #pass
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV4"] = "coap://10.100.35.1:5683"
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV6"] = "coap://[fdf6:a36c:4641:139::f1]:5683"
            cal_data["CFG_KEY_TUNNEL_URI"] = "10.100.200.20"
            cal_data["CFG_KEY_NTP_URI_IPV4"] = "10.100.35.1"
            cal_data["CFG_KEY_NTP_URI_IPV6"] = "fdf6:a36c:4641:139::f1"
        elif (not set_tst) and controller_type == "MESH":
            #pass
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV4"] = ""
            cal_data["CFG_KEY_BOOTSTRAP_URI_IPV6"] = "coap://[fdf6:a36c:4641:139::f1]:5683"
            cal_data["CFG_KEY_TUNNEL_URI"] = "10.100.200.20"
            cal_data["CFG_KEY_NTP_URI_IPV6"] = "fdf6:a36c:4641:139::f1"
            cal_data["CFG_KEY_NTP_URI_IPV4"] = ""

        cal_data["CFG_KEY_PHOTOCELL_USAGE"] = "false"

        device_id = cal_data["EUI"]

        pvt_factory_config=0
        #Only for PVT Nodes open pvt config file
        #if "14EFCF01110" in device_id or "14EFCF011A0" in device_id:
         #   with open("certificates/NEMA_PVT_Config_JSON/"+device_id+"_device_config.json") as f:
          #      pvt_factory_config = json.load(f)

        # Read SubCA and Root Certificates from files. These are common to all Devices
        with open("certificates/factorySubCA.txt") as f:
            cal_data["CFG_KEY_SUBCA_CERT"] = f.read()

        with open("certificates/factoryRootCA.txt") as f:
            cal_data["CFG_KEY_ROOT_CERT"] = f.read()

        # open the file with the already used EUIs
        already_used_EUIs = pd.read_csv("calibration/EUI_convertion.csv")

        if device_id in already_used_EUIs.T.values[ORIGINAL_EUI_COLUMN]:
            # TODO: This is so ugly! I hope Pandas can do better than this!
            i = list(already_used_EUIs.T.values[ORIGINAL_EUI_COLUMN]).index(device_id)
            device_id = already_used_EUIs.T.values[CONVERTED_EUI_COLUMN][i]
            print(f"Device ID already used! Let's use its new EUI: {device_id}")
        elif device_id in already_used_EUIs.T.values[CONVERTED_EUI_COLUMN]:
            print(f"Device ID already converted previously! Let's keep using this one: {device_id}")
        elif "14EFCF0" in device_id:
            print(f"Device ID OK from the Factory! Let's keep using this one: {device_id}")

        else:
            # Device ID not in Original or Converted EUIs. Pick one from the list
            print("Device ID not in Original or Converted EUIs. Picking one which is not on the list...")
            old_device_id = device_id
            device_id = get_unused_device_id(socket_type, controller_type, production_phase, already_used_EUIs)
            new_EUI_burned = True
            # A net EUI has been used from the certificate files. We need to track this in "calibration/EUI_convertion.csv"
            with open('calibration/EUI_convertion.csv', 'a') as eui_convertion_file:
                eui_convertion_file.write(f"\n{socket_type},{controller_type},{old_device_id},{device_id}")
            print('\033[31m' + '\n----------------------------------------------------------------------------')
            print('\033[31m' + 'A New EUI has been used. Please commit "calibration/EUI_convertion.csv" file')
            print('\033[31m' + '------------------------------------------------------------------------------')

        cal_data["EUI"] = device_id

        list_folder = "certificates/" + production_phase + "_devices/"
        if not os.path.exists(list_folder):
            raise Exception(
                f'Cannot find {list_folder} folder\nPlease request the folder with the device certificates for {production_phase}')

        client_certificate_file = list_folder + device_id.lower() + "_certificate.txt"
        with open(client_certificate_file) as f:
            cal_data["CFG_KEY_CLIENT_CERT"] = f.read()

        private_key_file = list_folder + device_id + "_privateKey.txt"
        with open(private_key_file) as f:
            cal_data["CFG_KEY_CLIENT_PVKEY"] = f.read()

        # Update the default settings loaded from factory_config_xmodem.json (factory_config) with the values read from the device (cal_data)
        factory_config = update_factory_configuration(factory_config, cal_data, pvt_factory_config)

        # Create factory_config_xmodem_DEVICEID.json file with the information read from the device
        factory_config_filename = f"calibration\\factory_config_xmodem_" + cal_data['EUI'] + ".json"
        if not os.path.exists("calibration"):
            os.makedirs("calibration")

        if os.path.exists(factory_config_filename):
            print(f"Data for {cal_data['EUI']} already saved! Not overwriting the file")
        else:
            # Create a new json file for this device, with calibration data and certificates
            with open(factory_config_filename, "w") as f:
                json.dump(factory_config, f, indent=1)
    else:
        # device_id was provided by the user. Open the already existent file with the factory configurations
        factory_config_filename = f"calibration\\factory_config_xmodem_" + device_id + ".json"
        with open(factory_config_filename) as f:
            factory_config = json.load(f)

    if factory_wipe:
        print("Sending factory wipe")
        g4.fcfg_wipe()
        g4.wait_reset(90)

    if program_flex_app:
        print("install Flex app")
        programmer.install_stm32_fw(config["stm_exe"], config["flex_app_hex"], "0x8000000")
        print("Wait flex app start")
        time.sleep(20)
        g4.write("\r\nzigbee-power on\r\n")  # To Power ON the EFR chip
        time.sleep(3)

    if install_efr_complete_FW:
        print("Installing EFR32 complete FW...")
        print(f'\nEFR32 Firmware:    {config["efr_complete_FW"]}\n')
        try:
            programmer.install_efr32_fw(config["efr_exe"], config["efr_complete_FW"])
        except:
            raise Exception("Make sure EFR programmer is connected!")

        print("EFR32 complete FW installation successful!\n")

    # if install_efr_bootloader:
    #     print("Installing EFR32 Bootloader...")
    #     print(f'\nEFR32 Bootloader Firmware:    {config["efr_bootloader_FW"]}\n')
    #     try:
    #         programmer.install_efr32_fw(config["efr_exe"], config["efr_bootloader_FW"])
    #     except:
    #         raise Exception("Make sure EFR programmer is connected!")

        print("EFR32 Bootloader installation successful!\n")
    if install_stm:
        print("Installing STM32 complete FW...")
        print(f'\nSTM32 Firmware:    {config["stm_hex"]}\n')
        programmer.install_stm32_fw(config["stm_exe"], config["stm_hex"], "--erase all")
        print("STM32 complete FW installation successful!\n")
        g4.wait_reset()
        g4.set_global_loglevel(4)
    else:
        print("No firmware STM32 installed!")

    # always set global log level
    g4.set_global_loglevel(4)
    g4.flush_input()

    #Second Factory Wipe, which also performs FSClear from 4.0.11.0 onwards
    if factory_wipe:
        print("Sending factory wipe")
        g4.fcfg_wipe()
        g4.wait_reset(90)

    if set_data:
        persisted = g4.lwm2m_get_fcfg_persisted()
        if persisted:
            if operation_type == "report":

                print("Factory Config Already Applied - READING PARAMETERS TO CONFIRM SUCCESSFULL")
            else:
                raise Exception("Cannot write calibration with persisted, FAILED!")
        if not persisted:
            try:
                g4.upload_xmodem_configurations(factory_config_filename)
            except:
                raise Exception("Failed to write file via xmodem")

            if not g4.apply_factory_config(sleep_time=7):
                raise Exception("Failed to apply factory configurations")

        #if set_tst:
         #   if controller_type == "LIFT":
          #      g4.execute_command_slow("lwm2m write factory-cfg 0 bootstrap_uri coap://10.100.99.1:5683")
          #      g4.execute_command_slow("lwm2m write factory-cfg 0 tunnel_uri 10.100.193.5")
          #      g4.execute_command_slow("lwm2m write factory-cfg 0 ntp_uri 10.100.100.1")
          #  elif controller_type == "MESH":
          #      g4.execute_command_slow("lwm2m write factory-cfg 0 bootstrap_uri coap://[fdf7:a36c:4641:139::f1]:5683")
          #      g4.execute_command_slow("lwm2m write factory-cfg 0 ntp_uri fdf7:a36c:4641:139::f1")
                 
    Success = verify_configurations(g4, operation_type, device_id, configuration_type)

    mdm_result = "empty"
    if socket_type == "ZHAGA" and controller_type == "LIFT" and Success and (operation_type == "full"):
        
        g4.execute_command_slow("reset")
        #if configuration_type == "tst" or configuration_type == "prod":
         #   g4.wait_reset(60)
          #  g4.execute_command_slow("loglevel 4")
           # g4.execute_command_slow("lwm2m write self_test 0 self_test_counter 0")
            #g4.reset()
        print("Applying mdm radio 10!")
        g4.wait_mdm_start(400)
        g4.execute_command_slow("mdm radio 10")
        mdm_result = g4.wait_mdm_applied(30)
        g4.fcfg_wipe()
        g4.wait_reset(90)
        
        if g4.lwm2m_get_fcfg_persisted():
            raise Exception("Cannot write calibration with persisted, FAILED!")

        try:
            g4.upload_xmodem_configurations(factory_config_filename)
        except:
            raise Exception("Failed to write file via xmodem")

        if not g4.apply_factory_config(sleep_time=7):
            raise Exception("Failed to apply factory configurations")
            
        Success = verify_configurations(g4, operation_type, device_id, configuration_type, mdm_result)
    firmware = config["STM_FW"]
    if Success:
        if operation_type == None and socket_type == "ZHAGA" and controller_type == "LIFT" and mdm_result=="True":
            os.rename(f'calibration\\{operation_type}_{device_id}_prod_complete', f'calibration\\{operation_type}_{firmware}_{device_id}_prod_complete_mdm_radio_10_applied_PASS.txt')
        elif operation_type != None and mdm_result=="True" and socket_type == "ZHAGA" and controller_type == "LIFT":
            os.rename(f'calibration\\{operation_type}_{device_id}_{configuration_type}_complete', f'calibration\\{operation_type}_{firmware}_{device_id}_{configuration_type}_complete_mdm_radio_10_applied_PASS.txt')
        else:
            os.rename(f'calibration\\{operation_type}_{device_id}_{configuration_type}_complete', f'calibration\\{operation_type}_{firmware}_{device_id}_{configuration_type}_complete_PASS.txt')
    else:
        os.rename(f'calibration\\{operation_type}_{device_id}_{configuration_type}_complete', f'calibration\\{operation_type}_{firmware}_{device_id}_{configuration_type}_complete_FAIL.txt')

    # TODO: This was moved outside of the "set_data" if condition. Check if this works fine when operation_type == "flash"
    if Success:
        print('\033[32m' + '-----------------------------')
        print('\033[32m' + 'Complete, PASSED!')
        print('\033[32m' + f'Process Time to {operation_type} : {time.time() - start_time}')
        print('\033[32m' + '-----------------------------\n')
        pygame.init()
        pygame.mixer.init()
        my_sound = pygame.mixer.Sound('ta_pronto.wav')
        my_sound.play()
        while pygame.mixer.get_busy():
            pygame.time.delay(10)
            pygame.event.poll()

    else:
        pygame.init()
        print('\033[31m' + '-----------------------------')
        print('\033[31m' + 'Complete, FAIL!')
        print('\033[31m' + '-----------------------------\n')
        pygame.mixer.init()
        my_sound = pygame.mixer.Sound('ta_pronto.wav')
        my_sound.play()
        while pygame.mixer.get_busy():
            pygame.time.delay(10)
            pygame.event.poll()


    if new_EUI_burned:
        # A net EUI has been used from the certificate files. We need to track this in "calibration/EUI_convertion.csv"
        #with open('calibration/EUI_convertion.csv', 'a') as eui_convertion_file:
         #   eui_convertion_file.write(f"\n{socket_type},{controller_type},{old_device_id},{device_id}")

        print('\033[31m' + '\n----------------------------------------------------------------------------')
        print('\033[31m' + 'A New EUI has been used. Please commit "calibration/EUI_convertion.csv" file')
        print('\033[31m' + '----------------------------------------------------------------------------')


    exit()
###############################################################################################################
#                       # Code shall not reach here! TO BE REMOVED #                                          #
###############################################################################################################



    print("Resetting the device after factory Configurations have been applied! Timeout: 120s")
    g4.reset()
    g4.wait_reset(120)
    # g4.flush_input()

    if set_exedra_tst_bootstrap:

        bootstrap_server_uri = ''

        if controller_type == "LIFT":
            # Set TST Exedra Bootstrap Server for Lift Nodes (IPv4)
            bootstrap_server_uri = "coap://10.100.99.1:5683"
            # Set TST Central Bridge connection in Lift Nodes
            g4.execute_command_slow("lwm2m write 32774 0 0 10.100.193.5")

        elif controller_type == "MESH":
            # Set TST Exedra Bootstrap Server for Mesh Nodes (IPv6)
            bootstrap_server_uri = "coap://[fdf7:a36c:4641:139::f1]:5683"

        g4.execute_command_slow("lwm2m write self_test 0 self_test_counter 5")
        g4.execute_command_slow("lwm2m bootstrap " + bootstrap_server_uri)
        g4.execute_command_slow("lwm2m bootstrap")

    # Do this at the end, only when everything has been verified OK
    with open(f"calibration\\After_{operation_type}_{device_id}_complete.txt", "w") as completion_file:
        g4.flush_input()
        g4.set_global_loglevel(4)

        file_output = ''

        efr_version = g4.get_mesh_fw_version()
        # simple retry mechanism, as sometimes EFR is not yet available
        retry_counter = 0
        while "Invalid" in efr_version:
            time.sleep(2)
            efr_version = g4.get_mesh_fw_version()
            retry_counter += 1
            if retry_counter == 20:  # too many retries
                raise Exception(f"Error reading EFR32 version! EFR Not Responding. FAILED!")

        # Validate certificates in Flash
        client_cert = g4.lwm2m_read_cert('Client certifcate')
        if CFG_KEY_CLIENT_CERT not in client_cert:
            Success = False
            print("Invalid Client Certificate!!!")
        root_cert = g4.lwm2m_read_cert('Root certifcate')
        if CFG_KEY_ROOT_CERT not in root_cert:
            Success = False
            print("Invalid Root Certificate!!!")
        subca_cert = g4.lwm2m_read_cert('subCA certificate')
        if CFG_KEY_SUBCA_CERT not in subca_cert:
            Success = False
            print("Invalid SubCA Certificate!!!")
        client_pvkey = g4.lwm2m_read_cert('Client private key')
        if CFG_KEY_CLIENT_PVKEY not in client_pvkey:
            Success = False
            print("Invalid Private Key!!!")

        file_output += "Client Certificate XOR:" + client_cert
        file_output += "\nRoot Certificate XOR:" + client_cert
        file_output += "\nSubCA Certificate XOR:" + client_cert
        file_output += "\nPrivate Key XOR:" + client_cert

        file_output += "\n" + efr_version

        if controller_type == "LIFT":
            file_output += "\n" + g4.execute_command("lwm2m read 32774 0 0")  # Read Tunnel for Lift
        g4.execute_command_slow("lwm2m write self_test 0 self_test_counter 5")
        file_output += "\n" + g4.execute_command("lwm2m read self_test 0 self_test_counter")  # Read Self Test Counter
        file_output += "\n" + g4.execute_command("lwm2m read 3 0 3")  # Read FW Version

        if set_mesh_network:
            retry_counter = 0
            response = g4.execute_command('lwm2m write 3440 0 20 "HYPERION_LOAD"')
            while "Invalid" in response:
                time.sleep(5)
                response = g4.execute_command('lwm2m write 3440 0 20 "HYPERION_LOAD"')
                retry_counter += 1
                if retry_counter == 5:  # too many retries
                    raise Exception(f"Error writing WiSun network Name")
        else:
            print("Mesh Network Name Not Set!")
        file_output += "\n" + g4.execute_command('lwm2m read 3440 0 20')
        file_output += "\n" + g4.sys_info()
        file_output += "\n Operation Time: " + str(time.time() - start_time)

        completion_file.write(file_output)
        completion_file.close()

    # if operation_type == "flash":
    #     g4.fsclear(wait_end=True)

