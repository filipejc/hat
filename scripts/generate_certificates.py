"""
# Filename:     generate_certificates.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   13/10/2021
# Last Update:  13/10/2021
#
# Version:      1.0
# Filetype:     
# Description:
# STATUS:       Stable
# Limitation:   None
"""


def generate_file(intial_EUI, filename, CERT_0, CERT_1, XOR0, XOR1):
    with open(filename, "w") as f:
        for x in range(256):
            line = f"EUI:{intial_EUI}{x:02X} CERT_0:{CERT_0} CERT_2:{CERT_1} XOR0:{XOR0} XOR1:{XOR1}\n"
            # print(line)
            f.write(line)


if __name__ == "__main__":
    # variables
    confs = {
        "NEMA_LTE": {
            "initial_eui": "14EFCF20FF0001",
            "filename": "production-nema-lte.txt"
        },
        "NEMA_MESH": {
            "initial_eui": "14EFCF28FF0001",
            "filename": "production-nema-mesh.txt"
        },
        "ZHAGA_NBIOT": {
            "initial_eui": "14EFCF10FF0001",
            "filename": "production-zhaga-nb.txt"
        },
        "ZHAGA_MESH": {
            "initial_eui": "14EFCF18FF0001",
            "filename": "production-zhaga-mesh.txt"
        }
    }
    intial_EUI = "14EFCF20FF0001"
    filename = "production-nema-lte.txt"
    CERT_0 = "SsaU5b7nVcXxVcBefsCn2DgEZWNLAz2FtGP7b8zO5yhw8ZvBd30N2QRduoI/NxWRsykJWYuPQs0NR/PE+xkcRmuJgJwvJz7CBI/iiZmmDpyVleLqX1v9N2E79HZOKILy5lrm0m+V1xbWYLdfQm+10lzVufdn6du+yYh9bkT1HZHCNy556b3UCT3XQVNYaC2z8kqCtMvLPzUDx3vwPuqzvu6be5Xlo0oUYZUvP378lOUCfPew7a2GL9bNv5v/CyiQDOoiIwBNLqziSqveKnR5YWsqQlS0+ZSm5/w8GZsPihTw4w8F9SDh4w8aq7x7V9BLU7zm0TSamhZPHKcPpj/5rWz1LFVgELJIAThPkR9wY91mmO2mnjN5gR9W0z8bhbiwJotUz22YoDDt6XTKhX7ZIxp5SRZM3jJLhF07trO+rZtzZFYBKJXHy/Reox0HvwIhF1UNMIeEIixMBftYZ7/PN7J6f8d6JkNPojYolNpJ2B9XUb9kaRKECFis0eXtvGDOoAtDWYO7aNg8GTrPw2g3cSP/dPIO5gVN/JeMqi1lJrfTtMHQe8R+14eVqi7Tbv/gHeCg5jpPTy+YKDrQB1ts8fh5SPbj/As7Pa/GazPFW0mLlaWZRfZ7bqTOZZ4+tcrrHMO+Cr34wbU9eAnHO2sJ8jGD4CrMsFQ9ONfuBhOo8NnW/ml3zdIWZnmoHKZ07kL8L+SiyCWKjgMKEYkgl6NQU903+8Qs9s8eXmWLyACA7tg7DhtX0zo1jIUoRMy4+KYjkDzikhYFwzZyeOhwlovjmLl4JBHtC8AMaxbUJjU7Pabz7k7iTAmi1pl9dHO2y9xLghnc/Q0fsNnFLvCwVPoQX6rXQbqWxR17omsQwmJJgSy+vCbR1RRkx+8Ry9DOaw797yS2lr97jImHuGO8G0D0v1Hjk0hXbZZd5PQEvNvaVtYISfCzQHnH+DeHUCn98vQ3pL+YmXrAPriDbeuQ8f+WsQ+9fS/kEymLONa+0Ermh7A5PY6wyiYWmifr2ljy5xMkiH4gzSWPYBma0gipht8hbcyTwvrS+jjtluEcQcEsWupvtt3WYurGnaiVFewJQq0/wsOdmvLxfC9aguEWKx8thp+f3bpMp/W8zzillOVZGH7OOtXyxWV4pO2MdNtlONTClnVa2rgZDgYbETLHoZS+kRWr8U+wYfI+f3YfjsZHs9bpgbTov9xxonkfUs0AsWKcZvBOMQMjlNyMI0Y1FRPFlCXdtoj5lYU9zWicyiUNVXx/p0lT4o967umfS9fwLGlo8qlu8Sd7WzCV9xgrsvEN7SyFBtkd0HW606ijvaNzsuBwjy19nAipubRWrWUHg5eKt1pslKIEo5prGqpLrmq4q98uoim+VDN7+qL2fuL5YRyVdQXHssX8GZnhmw8zAvGOk/usjCSiw57vHdDKQdp6AbawZUxmAcMgPgf1hTREdN8THac91JFRWLaxfFvzw69VlA5LK/HIhXuEq2KkgGiz2o3Ues7ERBItu19pFz8z5ISZMVa6rKQ8wtQc6u1V8j7GsntUozwZFXawAOxd9+keZ+EPOw/L9Pih90sgJg2VwdrJH1qyE2D5mdTX0AI8Bs3fHxphyTbSuRhv5T/tjrkAMjxuqCBh+ygLlZpm+KoPNhc3eR9FIdi5wURCbuhS+o3eKtfquOQUw8mjxFvDTREtVbCAUgXCCP10+tlLRtradCMQAXrBn2/0H70lGvJg+54YUB/Km1R4fGIRQMS6rmN+17WLMmOKusdTZcvSUuugQM4Ztj6xg/5nK7sYoWyiRmpOvM+alubF4lDGsMIC3GUmMaFGryXzILiYFpbXmoamii7X2kZ3KzZ/tZ8Ga5WEtA/TsWg9EmvnMwmfEC8/eiwTQnh5IzjusqQr9fDv8lqSFIMlwggI5moG0GoF4Sr+a1EgmQuppL83+FOnB/dTznClZ5zeX5Sbf9hAS4atA/IYNSJZvQ8DHfN5Jlvr3Slxzgah+HTA4CqBCsSsjr3KW2jsX17Luo82k+ezCXw+hQ9eZwnoMlMSmbhi+K8f50MQOLYasqPi7lVHa8t7HyF1/NCFdNfUAPBHBRCjTh7cqeP2mR4wIgg2G8PRQog6IxNRja2hCn7o7OK13zjTtPcPuzGhnWWeGjuOjLBrNJzvkEZK7f2c9FytL1EI8D8ixgUEziOKMFnnZV0B07IYEVsKU6a2XNHxB3SY1FMrDHgUOkeGQHb71ROjjZZ8A2TdyLo/U0yY"
    CERT_1 = "SsaU5b7nVcXxVcBefsCn2DgEZWNLAz2FtGP7b8zO5yjClP3i0yGoHbRoA18fIyu5zD6cva4mEpBTITqLrWmHDDMPYkUiWKtF8KTAfsbHaeikBd/9Lu3QtAcfUzWfiqu3Chu8Z1XN0BS5cuKDkmfzgOrkxqSfFCbBvAjjJllJBR8ByWTtkfA5mJl02JnlWIzj8n2YgXM6BFDCidBM578GO4wRMhoeLkSSxfzxtWs9qOH5lYavRKhVS40XStYRpaJe5AEsZeu0APfB0dRZux5z99wrLeYcI+gaGq0MFDHKPxYH/R+7rnsSWStQqtw/vmtjGL3H0hAGG3SYoh4u52l3j7cjWCcNs4ZEkTQQnyYjSIVtKr8Uueo4kGS84Z7b9BF/p+oc+sqg1gVa6lHlXCJAbG+BVq7SDgaa5mrbzAvzYbImFIz+ocZrKoVUO7i9b2xBRe6mUd79did8wrCZOr5tlzkuXbucYpJL7F7KHxpN4dN/eYWbVQKRIgb4+BcPpC162W2BIAMF7c5ZmSB6y6UYotLMkZ3WQwF/eamYAgvm7WffSqBgeWdFOsdJmifybzjiXWMXJU7RTnJb3b8ffychTeUVhgy+BteyKdxBuGFNkgDhNPqtXgPukpHP0rYl4wq5X/bpBCb9mCDZsl2CxPTSJvJO2n/IjO1z8BbSukutn+f7X9+D2nUtYMjc0F/onebsuHVcKuksRKNQ3k34sFk4fKgCzBmoA+zWbuTaS5m1csqznO8R5Q/rBlukPVtV9uEiqsolbrvyPIEbsXsqiDdHfvSmxEcGduqqyGo0vWVDwmteqt0ZydmGgSY620nppCY6XFh9vF72oQTnQgG0lGaQ9hNVe4L9h/37+ChVXhi8PPQKmsrGNXoBr6s2QFp7Z44YAToB/bAJhRHv7FMMFBBWw1NRm+K1FzJMPQCsCMLNxoSRwGePNLVqiHt8H80kJPPfZ/6So3SrgG36JrU9N1PMsNf/JgjpYfpfMK/GnBaADolB2Kdy2cwrT5bo1QSEeMqOPPZYNm98L/6IEubQ7jYLcTsP8JVOpzRaNEOp5uuc2qjXHEz9jsOZueeIdT/qf7PNJsyKInBAfUCqnNoR6ItkSavHNBHCyG/ENHoUpG+6yLSLxjbPJngOD60prtm3z6PkHCGNOJNQmWxkRPTO0XUYhTJphYo9PEN5+YDbn8w4D4kGJcaHcyggwtLP9auHgexH5pouzTs+HX/flaqITsdOLHG89/3k7xVsKJNyTGgcVz81/B2EaQP/ZeS4W+AkOvfWmugSZo3w6jcaZJQBJj5cKOlxr8YGE7Y3UmgNiuupk6X99vDtDkEGihUmCvpErERMptqekB5pAbjJbDm/mXh9cOIoHfDpyz28mHFuTizZdeMd7nBM92H7WbgRmi/QioDtENEXEPFWuwDKf7avOpYb5TVGbYG0LpxupVN7GtOgS/NbrQrAHbbIQ/qYunm9BXuXK84sI3USvEsuDrNUTpUX8XhO9DIe+JyByFjSyRw/pKZvs90BjmN1BI9WRaoexigRoR5RDP/uOVngiFQx4lHCSOoYCZZoHDYV1gCaTDSaqreWnJNQl6x2EjXDPcSXQPzOcrlP5FbIQohq3CDxdSxCpjt72SofuAVAdWQ3/Wn5SiG2f/nisvKux89d0GXUNC0ANgfPsuny93/ndXKQY5y5uwpx4mo6xN+nlXX8USgJzfZxvteWWcEEw1AxGIKK8k4sDqayee+mxtxwB/NEQFNd7LnVC64aGeUpeQgwaG1VUc7KO0bVWSXo0J/MdhczXYKRW3eMi0XuJYJKA3bQvCNI2Bgu13PaC8nUw9JMR4wI/W39feP//46UN2lqhvmPyeCZh1JqBZEkoPoDC2whGECMofQRZvZAtFga9yF7TQRRg0zl9u2WfnElQjRqCbvFDEjkD4DzOY/CR/e6R/RzdFLBsPMBTBXDJKNxmC6AdIRGxZh3j4s4t6PwV9Gm9A7TR6dchtexlGIZsXx+Pxskd8fezLMP0aDGOZDSRIeS+wGTqFLl533hAXnwdx2RpS9TDzs6Aj8H3Rm0hyWVrbmkCyCX8yxiewb5Lsotsv8MnIxrjbp0I+V10z5RahlhVRZLinNEf6oWxVjQvPgm46ZXqHIw2fvIgsYSjhZahZC+J4t6oAAaVOyyDeSlHqJsFBOGvz6tyaEvJ69QY1xyei6SEUtdzDmjnmhk3Qw+xN7SKR8/r51Py1rYffigJptZ7Wn7to6i"
    XOR0 = 250
    XOR1 = 237

    # start of generation
    print("Start of generation")
    for key, conf in confs.items():
        print(f"Key: {key}, confs: {conf}")
        generate_file(conf["initial_eui"], conf["filename"], CERT_0, CERT_1, XOR0, XOR1)
    print("End of generation")
