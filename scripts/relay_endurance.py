import time
import asyncio
from hat.utils.utils import create_logger
from hat.utils.DataStorage import DataStorage
from hat.comms.g4_interface_cli import G4

log = create_logger(__name__)

MAXIMUM_VOLTAGE_ON = 10  # V maximum voltage across the relay when closed
MINIMUM_POWER_ON = 15  # W minimum power when relay is on

MINIMUM_VOLTAGE_OFF = 100  # V minimum voltage across the relay when opened
MAXIMUM_POWER_OFF = 5  # W Maximum power when realy is off


async def main(confs):
    log.info("Staring main function")
    loop = asyncio.get_event_loop()
    # start test
    init_tasks = []
    L = await asyncio.gather(*[initialize(label, conf["g4"]) for label, conf in confs.items()])
    print(L)

    for i in range(1000):
        log.info(f"Starting cycle: {i + 1}")
        await asyncio.gather(*[relay_cycle(label, conf["g4"], conf["data"]) for label, conf in confs.items()])

    log.info("End of main!")


async def relay_cycle(label: str, g4: G4, data: [DataStorage]):
    log.info(f"{label}: Starting relay cycle")
    data["cycle time"] = time.asctime()
    relay_on = g4.execute_command("lamp relay 1")

    # sleep to wait for relay change
    await asyncio.sleep(3)

    # # read voltage accross the relay
    voltage = g4.lwm2m_get_relay_voltage()
    data["Relay on voltage"] = voltage

    power = g4.lwm2m_get_active_power()
    data["Relay on power"] = power

    if voltage > MAXIMUM_VOLTAGE_ON:
        raise Exception(f"{label}: problem changing relay to on")

    if power < MINIMUM_POWER_ON:
        raise Exception(f"{label}: problem changing relay to on")

    # wait some time before making another change
    await asyncio.sleep(3)

    relay_off = g4.execute_command("lamp relay 0")
    # sleep to wait for relay change
    await asyncio.sleep(3)

    # check if realy voltage across the relay is good
    voltage = g4.lwm2m_get_relay_voltage()
    data["Relay off voltage"] = voltage

    power = g4.lwm2m_get_active_power()
    data["Relay off power"] = power

    if voltage < MINIMUM_VOLTAGE_OFF:
        raise Exception(f"{label}: problem changing relay to off")

    if power > MAXIMUM_POWER_OFF:
        raise Exception(f"{label}: problem changing relay to off")

    data.start_next_test()

    # sleep to wait for relay change
    await asyncio.sleep(3)


async def initialize(label: str, g4: G4):
    log.info(f"{label}: Trying to initiatlize")

    # set loglevel 4
    g4.set_global_loglevel(4)

    # set connection manager suspend
    g4.execute_command("cm-suspend 360000")  # suspends connection manaer

    # lock rules engine
    g4.execute_command("rules lock_for 36000")

    log.info(f"{label}: Initialization completed:")


if __name__ == "__main__":
    # variables
    confs = {
        "NEMA MESH": {
            "com_port": "COM226",
            "logfile": "data/%Y%m%d_%H%M_NEMA_MESH.txt"
        },
        "NEMA LTE": {
            "com_port": "COM225",
            "logfile": "data/%Y%m%d_%H%M_NEMA_LTE.txt"
        }
    }

    # multiple device suported

    for label, conf in confs.items():
        g4 = G4(conf["com_port"], "SERIAL_THREAD", log_file=conf["logfile"])
        confs[label]["g4"] = g4
        data = DataStorage()
        data.open(f"data/Relay_{label}.xlsx")
        confs[label]["data"] = data

    asyncio.run(main(confs))

    log.info("end!")
