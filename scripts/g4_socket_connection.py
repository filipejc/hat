from hat.comms import LTE_interface
import time
import random
import string

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


# test variables
com_port = "COM23"
baudrate = 115200
host_ip = "52.13.98.241"
host_port = 82

print("Start of test")

# connect to a device
print("Connecting to G4...")
dev = LTE_interface.G4(com_port, baudrate)

# print(dev.execute_command("lte-shell"))
# unclock SIM
print(dev.unlock_sim(8082))

# check network operator
print(dev.lte_network_operator())

# connect to an APN
print(dev.lte_assign_APN())

# open socket
print(dev.lte_open_socket(host_ip, host_port, "TCP"))

start = time.time()
data = randomString(10)
# send data
print(dev.lte_send_data_socket(data))

# read data
print(dev.lte_recv_data_socket(1500))
elapsed = time.time() - start
print(f"Elapsed: {elapsed}")
print("End!")