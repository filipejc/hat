from hat.utils.utils import create_logger
import requests

log = create_logger(__name__)


class Lwm2mClient:
    def __init__(self, url, dev_eui, conn_timeout=10, response_timeout=10):
        self._url = url
        self._dev_eui = dev_eui
        self.conn_timeout = 10
        self.response_timeout = 10

    def lwm2m_read(self, object_id, instance_id, resource_id):
        log.info(f"Lwm2m read: obj: {object_id}, ins: {instance_id}, res: {resource_id}")
        uri = f"{self._url}/devices/{self._dev_eui}/{object_id}/{instance_id}/{resource_id}"
        r = requests.get(url=uri, timeout=(self.conn_timeout, self.response_timeout))

        return r.json()

    def lwm2m_write(self, object_id, instance_id, resource_id, data):
        log.info(f"Lwm2m write: obj: {object_id}, ins: {instance_id}, res: {resource_id}, data: {data}")
        uri = f"{self._url}/devices/{self._dev_eui}/{object_id}/{instance_id}/{resource_id}"
        r = requests.put(url=uri, data=data, timeout=(self.conn_timeout, self.response_timeout))

        return r.json()

    def lwm2m_exec(self, object_id, instance_id, resource_id):
        log.info(f"Lwm2m exec: obj: {object_id}, ins: {instance_id}, res: {resource_id}")
        uri = f"{self._url}/devices/{self._dev_eui}/{object_id}/{instance_id}/{resource_id}"
        r = requests.post(url=uri, timeout=(self.conn_timeout, self.response_timeout))

        return r.json()

if __name__ == '__main__':
    log.info("Testing lwm2m client")
    server = "http://13.94.133.236:3000"
    dev_eui = "14efcf0111f0003a"

    client = Lwm2mClient(server, dev_eui)

    client.lwm2m_read(4, 0, 4)
