# G4-LwM2M-Objects

This is the collection of LwM2M objects and their resources that are supported by G4. Each object is represented by a .xml file.

Status of the repo: **DRAFT**

## OMA editor

To modify or create new object, please use the [OMA editor](https://devtoolkit.openmobilealliance.org/OEditor/LWMOEdit).

### Naming convention

All .xml files should be named as follow:

 * Start with the object ID using always 5 digits (add 0 if needed)
 * End with the object's name
 * Use underscores as separators

### Modify an existing object of the repo

 * Import the file you would like to modify in the OMA editor ("Import" button at the top of the page).
 * Modify the object and its resources as you wish.
 * IMPORTANT: the "object version" should always remain empty !
 * Export the object again ("Export" button at the top of the page) and you will get a .zip with the new .xml.
 * Unzip the .xml and replace the old file with the new version in the repo.
 * commit your changes to a new branch in bitbucket and send a PR 

### Add a new object to the repo

 * Open the OMA editor.
 * Create a new 1.1 object ("New 1.1" button at the top of the page).
 * Edit the object's name, its description, its definition and add its resources.
 * IMPORTANT: the "object version" should always remain empty !
 * Export the object ("Export" button at the top of the page) and you will get a .zip with the .xml.
 * Unzip the .xml file, rename it correctly and place it in the repo.
 * commit your changes to a new branch in bitbucket and send a PR 