<?xml version="1.0" encoding="UTF-8"?>

<!-- BSD-3 Clause License

Copyright 2020 uCIFI Alliance

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

-->

<LWM2M xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="http://openmobilealliance.org/tech/profiles/LWM2M.xsd">
	<Object ObjectType="MODefinition">
		<Name>LPWAN mesh statistics</Name>
		<Description1>The LPWAN Mesh Statistics object enables the client to collect statistical information specifically tailored to monitor performance of 805.15.4 mesh networks. The object encompasses resources that provide visibility over both MAC layer and network layer statistics, the applicability of which is dependent on the specific 802.15.4 technology.</Description1>
		<ObjectID>3441</ObjectID>
		<ObjectURN>urn:oma:lwm2m:ext:3441</ObjectURN>
		<LWM2MVersion>1.0</LWM2MVersion>
		<ObjectVersion>1.0</ObjectVersion>
		<MultipleInstances>Single</MultipleInstances>
		<Mandatory>Optional</Mandatory>
		<Resources>
			<Item ID="0">
				<Name>TX packets total</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>IEEE 802.15.4 macTxSuccessCount, packets transmitted and acked with no retries.</Description>
			</Item>
			<Item ID="1">
				<Name>TX success on single retry</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>IEEE 802.15.4 macRetryCount, successful transmission after one retry.</Description>
			</Item>
			<Item ID="2">
				<Name>TX success on multi retry</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>IEEE 802.15.4 macMultipleRetryCount, TX acked after more than one retry.</Description>
			</Item>
			<Item ID="3">
				<Name>TX discarded packets</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of packets that failed transmission.</Description>
			</Item>
			<Item ID="4">
				<Name>TX CCA failures</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of packets that failed to transmit due to busy channel.</Description>
			</Item>
			<Item ID="5">
				<Name>TX ACK failures</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of packets that failed to transmit due to lack of acknowledgement.</Description>
			</Item>
			<Item ID="6">
				<Name>RX packets total</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of packets received.</Description>
			</Item>
			<Item ID="7">
				<Name>RX discarded packets</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of packets discarded.</Description>
			</Item>
			<Item ID="8">
				<Name>RX malformed packets</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of packets that failed to parse due to inconsistencies on frame headers.</Description>
			</Item>
			<Item ID="9">
				<Name>RX unsecure packets</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total packets discarded due to inconsistencies on security information (802.15.4 macSecurityFailure).</Description>
			</Item>
			<Item ID="10">
				<Name>RX duplicate packets</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of duplicate packets dropped (802.15.4 macDuplicateFrameCount).</Description>
			</Item>
			<Item ID="11">
				<Name>Beacon frames sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 beacon frames transmitted by the device. These are used for mesh network management operations (advertising, discovery, etc.).</Description>
			</Item>
			<Item ID="12">
				<Name>Beacon frames received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 beacon frames received by the device. These are used for mesh network management operations (advertising, discovery, etc.).</Description>
			</Item>
			<Item ID="13">
				<Name>Ack frames sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 acknowledgment frames transmitted by the device.</Description>
			</Item>
			<Item ID="14">
				<Name>Ack frames received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 acknowledgment frames received by the device.</Description>
			</Item>
			<Item ID="15">
				<Name>Command frames sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 command frames transmitted by the device. These may be used for mesh network management operations (association, disassociation, beacon request, etc.).</Description>
			</Item>
			<Item ID="16">
				<Name>Command frames received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 command frames received by the device. These may be used for mesh network management operations (association, disassociation, beacon request, etc.).</Description>
			</Item>
			<Item ID="17">
				<Name>Data frames sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 data frames transmitted by the device.</Description>
			</Item>
			<Item ID="18">
				<Name>Data frames received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Used as a cumulative counter to report on the number of IEEE 802.15.4 data frames received by the device.</Description>
			</Item>
			<Item ID="19">
				<Name>Data bytes sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of payload data bytes sent via this interface.</Description>
			</Item>
			<Item ID="20">
				<Name>Data bytes received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Total number of payload data bytes received via this interface.</Description>
			</Item>
			<Item ID="21">
				<Name>Unicast packets sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Count of unicast packets sent.</Description>
			</Item>
			<Item ID="22">
				<Name>Unicast packets received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Count of unicast packets received.</Description>
			</Item>
			<Item ID="23">
				<Name>Multicast packets sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Count of multicast packets sent.</Description>
			</Item>
			<Item ID="24">
				<Name>Multicast packets received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Count of multicast packets received.</Description>
			</Item>
			<Item ID="25">
				<Name>Broadcast packets sent</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Count of broadcast packets sent.</Description>
			</Item>
			<Item ID="26">
				<Name>Broadcast packets received</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Count of broadcast packets received.</Description>
			</Item>
			<Item ID="27">
				<Name>Start</Name>
				<Operations>E</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Mandatory</Mandatory>
				<Type></Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Resets all resources and starts collection period.</Description>
			</Item>
			<Item ID="28">
				<Name>Stop</Name>
				<Operations>E</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Mandatory</Mandatory>
				<Type></Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Stops collecting information, but does not reset resources.</Description>
			</Item>
		</Resources>
		<Description2></Description2>
	</Object>
</LWM2M>
