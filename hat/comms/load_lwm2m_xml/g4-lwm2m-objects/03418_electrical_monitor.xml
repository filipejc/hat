<?xml version="1.0" encoding="UTF-8"?>

<!--
FILE INFORMATION

uCIFI Permanent Document
   File: uCIFI_Electrical_Monitor-v1_0
   Type: xml
   Date: 2020-08-31

Public Reachable Information
   Path: https://ucifi.com/datamodel
   Name: uCIFI_Electrical_Monitor-v1_0.xml

NORMATIVE INFORMATION

  Information about this file can be found in the latest revision of the document

    uCIFI Device Data Model-Version 1.0.pdf

  which is available at https://ucifi.com/datamodel

  Send comments using the web form availble at https://ucifi.com/contact

LEGAL DISCLAIMER

  Copyright 2020 uCIFI Alliance All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  3. Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived
  from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.

  The above license is used as a license under copyright only. 
  Please reference the uCIFI IPR Policy for patent licensing terms:
  https://ucifi.com/ipr-policy
-->

<LWM2M xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="http://openmobilealliance.org/tech/profiles/LWM2M.xsd">
	<Object ObjectType="MODefinition">
		<Name>Electrical monitor</Name>
		<Description1>The uCIFI electrical monitor object provides attributes related to the analysis of electrical consumption in an outdoor luminaire or in a streetlight cabinet. It also identifies electrical anomalies.</Description1>
		<ObjectID>3418</ObjectID>
		<ObjectURN>urn:oma:lwm2m:ext:3418</ObjectURN>
		<LWM2MVersion>1.0</LWM2MVersion>
		<ObjectVersion>1.0</ObjectVersion>
		<MultipleInstances>Multiple</MultipleInstances>
		<Mandatory>Optional</Mandatory>
		<Resources>
			<Item ID="1">
				<Name>Supply voltage</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>V</Units>
				<Description>Electrical voltage supplied to the device.</Description>
			</Item>
			<Item ID="2">
				<Name>Supply current</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>A</Units>
				<Description>Electrical current supplied to the device.</Description>
			</Item>
			<Item ID="3">
				<Name>Frequency</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>Hz</Units>
				<Description>Frequency of the supply current to the device.</Description>
			</Item>
			<Item ID="4">
				<Name>Active power</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>W</Units>
				<Description>Active power consumed by the device and its electrical load.</Description>
			</Item>
			<Item ID="5">
				<Name>Power factor</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration>-1..1</RangeEnumeration>
				<Units></Units>
				<Description>Power factor is equal to active power divided by apparent power. The value is between -1 and +1.</Description>
			</Item>
			<Item ID="6">
				<Name>Cumulated active energy</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>kWh</Units>
				<Description>Cumulated number of kWh measured by the device and its load since last energy counter reset.</Description>
			</Item>
			<Item ID="7">
				<Name>Energy reset</Name>
				<Operations>E</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type></Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Reset both cumulated active and reactive energy counter.</Description>
			</Item>
			<Item ID="8">
				<Name>Low power factor threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Threshold below which the device should trigger a low power factor event.</Description>
			</Item>
			<Item ID="9">
				<Name>Low power factor</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the power factor is below threshold. This is an absolute value threshold.</Description>
			</Item>
			<Item ID="10">
				<Name>Low power threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>W</Units>
				<Description>Threshold below which the device should trigger a low power event.</Description>
			</Item>
			<Item ID="11">
				<Name>Low power threshold at low dim level</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>W</Units>
				<Description>Threshold below which the device should trigger a low power event. This is an addition to the low power threshold to allow a separated threshold when dim level is low.</Description>
			</Item>
			<Item ID="12">
				<Name>Low power</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the power is below threshold. Vendor may consider the “Low power threshold at low dim level” to set this resource to 1 taking into account the lamp dim level.</Description>
			</Item>
			<Item ID="13">
				<Name>High power threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>W</Units>
				<Description>Threshold above which the device should trigger a high power event.</Description>
			</Item>
			<Item ID="14">
				<Name>High power threshold at low dim level</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>W</Units>
				<Description>Threshold above which the device should trigger a high power event. This is an addition to the high power threshold to allow a separated threshold when dim level is low.</Description>
			</Item>
			<Item ID="15">
				<Name>High power</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the power is above threshold. Vendor may consider the “High power threshold at low dim level” to set this resource to 1 taking into account the lamp dim level.</Description>
			</Item>
			<Item ID="16">
				<Name>Low current threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>A</Units>
				<Description>Threshold below which the device should trigger a low current event.</Description>
			</Item>
			<Item ID="17">
				<Name>Low current</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the current is below threshold.</Description>
			</Item>
			<Item ID="18">
				<Name>High current threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>A</Units>
				<Description>Threshold above which the device should trigger a high current event.</Description>
			</Item>
			<Item ID="19">
				<Name>High current</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the current is above threshold.</Description>
			</Item>
			<Item ID="20">
				<Name>Low voltage threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>V</Units>
				<Description>Threshold below which the device should trigger a low voltage event.</Description>
			</Item>
			<Item ID="21">
				<Name>Low voltage</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the voltage is below threshold.</Description>
			</Item>
			<Item ID="22">
				<Name>High voltage threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>V</Units>
				<Description>Threshold above which the device should trigger a high voltage event.</Description>
			</Item>
			<Item ID="23">
				<Name>High voltage</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the voltage is above threshold.</Description>
			</Item>
			<Item ID="24">
				<Name>Critical inrush threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>A</Units>
				<Description>Threshold above which the device should trigger a critical inrush event.</Description>
			</Item>
			<Item ID="25">
				<Name>Critical inrush current</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if the inrush current is above threshold.</Description>
			</Item>
			<Item ID="26">
				<Name>Minimum inrush Current</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>A</Units>
				<Description>Minimum inrush current since last configuration.</Description>
			</Item>
			<Item ID="27">
				<Name>Maximum inrush Current</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>A</Units>
				<Description>Maximum inrush current since last configuration.</Description>
			</Item>
			<Item ID="28">
				<Name>Latest inrush Current</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>A</Units>
				<Description>Latest inrush current measured since last time the relay switched ON.</Description>
			</Item>
			<Item ID="29">
				<Name>Reactive power</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>VAR</Units>
				<Description>Instantaneous reactive power measured by the device and its electrical load.</Description>
			</Item>
			<Item ID="30">
				<Name>Reactive energy</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>VARh</Units>
				<Description>Cumulative reactive power measured by the device and its electrical load since last energy counter reset.</Description>
			</Item>
			<!--
			New resources, added to provide more control over the STPM33 metering chip. 
			Notes: Please, read https://owlet-de.atlassian.net/wiki/spaces/OWGF/pages/628654245/Energy+metering+service for more information.
			-->
			<Item ID="26241">
				<Name>Stabilization time</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>s</Units>
				<Description>Time during which the electrical data change will not be notified to the LwM2M server after a dimming level change.</Description>
			</Item>
			<Item ID="26242">
				<Name>Phase angle</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>Degrees</Units>
				<Description>The angle between voltage and current.</Description>
			</Item>
			<Item ID="26243">
				<Name>Voltage ZCR timestamp</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>ms</Units>
				<Description>Last measured voltage zero-crossing timestamp as milliseconds since the device's last boot.</Description>
			</Item>
			<Item ID="26244">
				<Name>Current ZCR timestamp</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>ms</Units>
				<Description>Last measured current zero-crossing timestamp as milliseconds since the device's last boot.</Description>
			</Item>
			<Item ID="26245">
				<Name>Current ZCR monitoring time</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration>10..1000</RangeEnumeration>
				<Units>ms</Units>
				<Description>Time during which the current zero-crossings will be monitored if the device is toggled to do so.</Description>
			</Item>
			<Item ID="26246">
				<Name>Toggle current ZCR monitor</Name>
				<Operations>E</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type></Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Toggles the device to monitor the current zero-crossings for the duration specified in resource ID 26245.</Description>
			</Item>
			<Item ID="26249">
				<Name>SAG time threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration>0.008..131</RangeEnumeration>
				<Units>ms</Units>
				<Description>Time threshold used to trigger a SAG event used in combination with the voltage threshold resource ID 26250.</Description>
			</Item>
			<Item ID="26250">
				<Name>SAG voltage threshold</Name>
				<Operations>RW</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration>0,0.802912..822</RangeEnumeration>
				<Units>V</Units>
				<Description>Voltage threshold used to trigger a SAG event used in combination with the SAG time threshold resource ID 26249. Set to 0 to disable the SAG detection.</Description>
			</Item>
			<Item ID="26251">
				<Name>SAG</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Boolean</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Set to 1 if there was a SAG event. This can be used to check if the device is turned off. Resets every second.</Description>
			</Item>
			<Item ID="26252">
				<Name>SAG counter</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Integer</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units></Units>
				<Description>Number of SAG events. Resets every day at midnight.</Description>
			</Item>
			<Item ID="26253">
				<Name>Timestamp</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Time</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>s</Units>
				<Description>Timestamp of all the measurements as seconds since January 1, 1970, 00:00:00.</Description>
			</Item>
			<Item ID="26254">
				<Name>Relay voltage</Name>
				<Operations>R</Operations>
				<MultipleInstances>Single</MultipleInstances>
				<Mandatory>Optional</Mandatory>
				<Type>Float</Type>
				<RangeEnumeration></RangeEnumeration>
				<Units>V</Units>
				<Description>Relay voltage.</Description>
			</Item>
		</Resources>
		<Description2></Description2>
	</Object>
</LWM2M>
