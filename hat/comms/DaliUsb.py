"""
# Filename:     DaliUsb.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   10/09/2021
# Last Update:  10/09/2021
#
# Version:      1.0
# Filetype:     
# Description:  Communicate with a dali device using Tester
# STATUS:       Stable
# Limitation:   None
"""

"""
Message format:
START_BYTE (1x Byte)
LENGTH (1x Byte)
Data (LENGTHx Bytes) encoded as base 64
CRC (1x BYTE) XOR of data
END_BYTE (1x Byte)
"""

import serial
import base64
import time
import functools
from enum import Enum
from hat.utils.utils import retry
from abc import ABC, abstractmethod
from typing import List
from hat.utils.utils import create_logger, list_to_hex_string

CONFIGS = {
    "default_timeout": 5,  # default timeout in seconds
    "start_byte": 1,  # start of transmission
    "end_byte": 4,  # end of transmission
    "default_baudrate": 115200,  # default baudrate
    "loglevel": "INFO"  # level of logger to be used
}

log = create_logger(__name__, CONFIGS["loglevel"])


class ReceiveState(Enum):
    WaitStartByte = 0
    WaitLen = 1
    ReceiveData = 2
    WaitCRC = 3
    WaitEndByte = 4


class DaliUartInterface(ABC):
    @abstractmethod
    def send_data(self, data: List[int]):
        """ Write package to UART interface

        Parameters
        ----------
        data

        Returns
        -------

        """
        pass

    @abstractmethod
    def get_data(self) -> List[int]:
        """
        Get data from UART interface

        Returns
        -------

        """
        pass


class DaliUsb(DaliUartInterface):
    """
    Interface for Dali over USB for internally developed interface

    Parameters
    ----------
    port: str
        COM port where the Dali USB is identified
    baudrate: baudrate, optional
        Baudrate for communications
    timeout: int, optional
        Number of seconds to wait for a response
    """

    def __init__(self, port: str, baudrate: int = CONFIGS["default_baudrate"],
                 timeout: int = CONFIGS["default_timeout"]):

        log.debug(f"Connecting to Dali USB {port} at baudrate {baudrate} with timeout of {timeout}seconds...")
        self._intf = serial.Serial(port, baudrate=baudrate, timeout=timeout)
        self._state = ReceiveState.WaitStartByte
        self._timeout = timeout

        log.info("Connected to Dali USB interface!")

    @staticmethod
    def calculate_crc(data: bytes) -> int:
        """
        Calculate CRC checksum (XOR of all bytes)

        Parameters
        ----------
        data: bytes
            list of bytes to calculate

        Returns
        -------
        int
            Calculate checksum
        """
        return functools.reduce(lambda a, b: a ^ b, data, 0)

    def get_data(self) -> bytes:
        """
        Get data from interface

        Returns
        -------
        bytes
            Data received from device
        """
        start_time = time.time()

        self._state = ReceiveState.WaitStartByte
        expected_len = 0
        res = []

        while time.time() - start_time < self._timeout:
            data = self._intf.read(1)
            if data == b'':
                continue
            byte = ord(data)
            # print(chr(byte), end="")

            if self._state == ReceiveState.WaitStartByte:
                if byte == CONFIGS["start_byte"]:
                    self._state = ReceiveState.WaitLen
            elif self._state == ReceiveState.WaitLen:
                res = []
                expected_len = byte
                self._state = ReceiveState.ReceiveData
            elif self._state == ReceiveState.ReceiveData:
                res.append(byte)
                if len(res) == expected_len:
                    self._state = ReceiveState.WaitCRC
            elif self._state == ReceiveState.WaitCRC:
                crc = byte
                crc_calculated = DaliUsb.calculate_crc(res)
                if crc != crc_calculated:
                    # raise Exception(f"Wrong CRC, received: {crc}, calculated: {crc_calculated}")
                    log.warning("\nWrong CRC, to be implemented on dalu-usb interface side")
                # TODO: implement CRC
                self._state = ReceiveState.WaitEndByte
            elif self._state == ReceiveState.WaitEndByte:
                if byte == CONFIGS["end_byte"]:
                    message = base64.decodebytes(bytearray(res))

                    log.debug(f"Received data: {list_to_hex_string(message)}")

                    return message

                else:
                    print("Wrong data")
                    self._state = ReceiveState.WaitStartByte  # after wait for end byte always go to wait start byte

        raise Exception("Could not receive response in time")

    @retry(Exception, 4, 3, 2)
    def send_data(self, data: List[int]):
        """
        Send data to dali UART

        Parameters
        ----------
        data: List of int
            Data to send to DALI
        """
        log.debug(f"Sending data: {list_to_hex_string(data)}")
        cmd = [CONFIGS["start_byte"]]
        data_array = bytearray(data)
        data_encoded = base64.b64encode(data_array)
        cmd.append(len(data_encoded))
        cmd += data_encoded
        cmd.append(DaliUsb.calculate_crc(data_encoded))  # CRC of data
        cmd.append(CONFIGS["end_byte"])

        log.debug(f"Sending Raw packet: {list_to_hex_string(cmd)}")
        # send data
        self._intf.write(cmd)


if __name__ == "__main__":
    from hat.drivers.RelayInterface import RelayInterface

    # set relay position
    # board = RelayInterface("COM133", config_file=r"C:\PythonCode\hat\scripts\robot_lib\relay_config.json")
    # board.open()
    # board.set_position_by_label("uart")

    # connect to Dali USB
    dali = DaliUsb("COM114")
    time.sleep(2)
    dali.send_data(['\xE0', '\x02', '\x03', '\xE1'])
    for i in range(10):
        res = dali.get_data()
        print(f"Results: {res}")

    print("end!")
