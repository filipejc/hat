"""
# Filename:     __init__.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20/04/2021
# Last Update:  20/04/2021
#
# Version:      1.0
# Filetype:     
# Description:  Group all communication with Lucos and servers
# STATUS:       Stable
# Limitation:   None
"""
