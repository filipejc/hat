from hat.utils.DataStorage import DataStorage
import os
import serial
import logging
import re
import json
from hat.utils.utils import retry, create_logger
from hat.comms.P7CM_FCT import P7CM_FCT
from hat.comms.ZigbeeInterface import ZigbeeInterface
from serial.tools import list_ports
import pynmea2
import time
from typing import List

log = create_logger(__name__, "DEBUG")

SLEEP_TIME = 0.05  # time to wait between commands
nTries = 6  # number of retries


class P7CM_serial_interface:
    def __init__(self, com_port, baudrate=115200, timeout=10, cli=False):
        self._timeout = timeout
        self._interface = serial.Serial(port=com_port, baudrate=baudrate, timeout=timeout)
        self._mode = "normal"

        if cli:
            self._prompt = r"\[CLI\]>"
        else:
            self._prompt = ">"

        self._prompt_re = re.compile(self._prompt, re.MULTILINE)

    def set_timeout(self, timeout: int):
        """

        Parameters
        ----------
        timeout: set timeout in seconds

        Returns
        -------
            None
        """
        self._interface.timeout = timeout

    def get_timeout(self) -> int:
        """
        Get interface timeout

        Returns
        -------
        timeout: in seconds
        """
        return self._interface.timeout

    def reconnect(self):
        # if not self._interface.is_open:
        self._interface.close()
        time.sleep(2)
        self._interface.open()

    def set_prompt(self, prompt: str):
        self._prompt = prompt

    def _read_until(self, prompt=None):
        if prompt is None:
            prompt = self._prompt
        data = self._interface.read_until(prompt.encode("ascii")).decode("utf-8")
        log.debug(f"Receiving: {data}")
        return data

    def write(self, command, flush=True):
        log.debug(f"Sending: {command}")
        if not command[-1] == "\r":
            command += "\r"

        command = command.encode("ascii")
        self._interface.write(command)

        if flush:
            self._interface.flushOutput()

    def read(self) -> str:
        """
        Read all bytes available

        Returns
        -------
            string : all bytes available
        """
        return self._interface.read_all().deencode("ascii")

    def read_all(self, prompt=None):
        if prompt is None:
            prompt = self._prompt
        data = self._read_until(prompt)
        return data

    # def execute_command(self, command, sleep_time=SLEEP_TIME, check_errors=False, timeout=None):
    #     if timeout is not None:
    #         prev_timeout = self.get_timeout()
    #         self.set_timeout(timeout)
    #
    #     self._interface.flush()
    #     self.write(command)
    #     time.sleep(sleep_time)
    #     data = self.read_all()
    #
    #     data = data.replace(command, "")  # FIXMe: this is a bit dangerous
    #     data = data.replace(self._prompt, "")
    #
    #     if timeout is not None:
    #         self.set_timeout(prev_timeout)
    #
    #     return data

    @retry(Exception, 4, 2, 1)
    def execute_command(self, command, timeout=10):
        log.info(f"Execute command: {command}")
        if not self.is_open():
            self.open()

        res = self.write(command)

        starttime = time.time()
        data_raw = ""
        remaining_time = timeout
        try:
            while remaining_time > 0:
                data_raw += self._interface.read_all().decode("ASCII")
                if self._prompt_re.search(data_raw):
                    data = self._prompt_re.sub("", data_raw)
                    log.debug(f"Message received: {data}")

                    return data

                remaining_time = timeout - (time.time() - starttime)
        except Exception as e:
            print(f"Could not read all information, command {command}, data {data_raw}")

        raise Exception(f"Command: {command}, Could not get all data: {data_raw}")


COPS_status = {
    0: "Registered",
    2: "Not registered"
}
COPS_technology = {
    0: "GSM",
    2: "UTRAM",
    3: "GSM w/EGPRS",
    4: "UTRAN w/HSDPA",
    5: "UTRAN w/HSUPA",
    6: "UTRAN w/HSDPA and w/HSUPA"
}

CREG = {
    0: "Not registered",
    1: "Registered",
    2: "Not registered, but searching",
    3: "Registration denied",
    4: "Unknown (not used)",
    5: "Registered, roaming"
}


class P7CM:
    ROBOT_LIBRARY_SCOPE = 'SUITE'

    # list of commands for FCT
    CMD_DALI_REINIT = 0x30  # Dali reinit
    CMD_DALI_FCT_MODE = 0xD1  # Dali mode
    CMD_XBEE_FCT_MODE = 0xD0  # XBEE FCT MODE
    CMD_DALI_DIMMING = 0x31
    CMD_1_10V_DIMMING = 0x20
    CMD_1_10V_FEEDBACK = 0x21

    def __init__(self, address, interface_type="SERIAL", eui=None, cli=True):
        log.info("Creating P7CM object")
        interface_type = interface_type.upper()
        # TODO: better implementation to support multiple interfaces.
        if interface_type == "SERIAL" or interface_type == "WEBSOCKET":
            pass
        elif interface_type == "ZIGBEE":
            if eui is None:
                raise Exception("For zigbee is necessary to provide eui")
        else:
            raise ValueError(f"Not recognized interface type {interface_type}")

        self._interface_type = interface_type
        self._address = address
        self._eui = eui
        self._open = False
        self._cli = cli

    def is_open(self):
        return self._open

    def open(self, force=False):
        if not force and self.is_open():
            log.warning("P7CM interface already openned!")
            return
        log.debug("Opening P7CM device connection")
        if self._interface_type == "SERIAL":
            self._intf = P7CM_serial_interface(self._address, cli=self._cli)
        elif self._interface_type == "WEBSOCKET":
            self._intf = WebsocketInterface(self._address)
        elif self._interface_type == "ZIGBEE":
            self._intf = ZigbeeInterface(self._address)
            self._intf.set_remote(self._eui)
        else:
            raise ValueError(f"Not recognized interface type {self._interface_type}")

        self._open = True

    def close(self):
        log.info("Closing interface")
        self._intf.close()

    def is_usb_connected(self):
        """
        Function to check if Gemalto is connected through USB

        Note: More than one Gemalto is connected through USB is not supported
        """
        ports = list_ports.comports()
        gemalto_ports = 0
        for port in ports:
            if "Cinterion" in port.description:
                gemalto_ports += 1

        return True if gemalto_ports > 1 else False

    def wait_usb_connection(self, timeout=360):
        """
        Wait until the USB connection is available

        Return:
            bool
                True if sucessfully USB connected
                False if timeout is elapsed
        """
        start_time = time.time()
        while time.time() - start_time < timeout:
            connected = self.is_usb_connected()
            if connected:
                return True
            time.sleep(1)
        return False

    def discover_node(self, eui: str, timeout: int = 20):
        if self._interface_type != "ZIGBEE":
            raise Exception("Only available in Zigbee mode")
        return self._intf.discover_node(eui, timeout)

    def get_at_parameter(self, at: str, convert: str = "hex"):
        if self._interface_type != "ZIGBEE":
            raise Exception("Only available in Zigbee mode")
        return self._intf.get_at_parameter(at, convert)

    def execute_command(self, command):
        if not self.is_open():
            self.open()

        res = self._intf.execute_command(command)
        return res

    def get_version(self):
        res = self._intf.execute_command("version")
        return res

    def read_lux(self):
        lux = self._intf.execute_command("PHC01")
        res = re.search(r"Avg Light Value\s+:\s(?P<lux>\d+)\n\[FPS\]Photocell Control Status\s+:\s(?P<status>\d)\n",
                        lux)
        if res:  # if something was found
            avg_lux = int(res.group("lux"))
            status = bool(res.group("status"))
            return avg_lux, status

        raise ValueError("Could not read lux and status")

    def set_relay(self, status):
        log.info(f"Set relay to {status}")
        self._intf.execute_command(f"rel {1 if status else 0}")

    def get_relay(self):
        relay = self._intf.execute_command("POS01")
        res = re.search(
            r"\[FRS\]Relay status\s+:\s(?P<status>\w+)\n\[REP\]Relay expected status\s+:\s(?P<required>\w+)", relay)
        if res:
            status = True if res.group("status") == "true" else False
            required = True if res.group("required") == "true" else False
            return status, required
        raise ValueError("Could not read relay status")

    @retry(Exception, nTries, 2, 1)
    def get_ac_metering(self, raise_exception=True):
        res = self._intf.execute_command("SYS03")

        res_dict = {}

        match = re.search(
            r"\[FPF\]Power factor\s+:\s(?P<PF>.+)\s\[FCC\]Current Current\s+:\s(?P<curent>.+)\sA\s\[FCV]Current Voltage\s+:\s(?P<volt>.+)\sV\s\[FEC\]Feedback Energy Consumption:\s(?P<energy>.+)\sKWh\s\[FCP\]Sys Feedback Current Power\s:\s(?P<power>.+)\sW\s\[FET\]Last FEC Updated TimeStamp\s:\s(?P<time>.+)",
            res, re.MULTILINE)
        if match:
            res_dict = match.groupdict()
            for key, item in res_dict.items():
                if key != "time":
                    res_dict[key] = float(item)
            return res_dict
        else:
            if raise_exception:
                raise Exception(f"Could not find data in response, {res}")
            else:
                log.warning("Not possible to get data")
                res_dict["volt"] = 0
                res_dict["power"] = 0
                return res_dict

    def get_gps(self):
        gps = self._intf.execute_command("GPS")
        regex = r"^([\w\s-]+):\s+(.+)$"
        res = re.findall(regex, gps, re.MULTILINE)
        res = map(lambda x: (x[0].strip(), x[1].strip()), res)
        res_dict = dict(res)

        return res_dict

    @retry(Exception, nTries, 2, 1)
    def get_sys03(self):
        sys03 = self._intf.execute_command("SYS03")
        regex = r"^([\w \-\[\]]+):\s+(.+)$"
        res = re.findall(regex, sys03, re.MULTILINE)
        res = map(lambda x: (x[0].strip(), x[1].strip()), res)
        res_dict = dict(res)

        return res_dict

    @retry(Exception, nTries, 2, 1)
    def execute_at(self, command):
        res = self.execute_command(command)
        if self._interface_type == "ZIGBEE":
            match_re = rf"\[ATC\]\s+{re.escape(command)}\s+(.+)\s+OK"
            match = re.search(match_re, res, re.DOTALL)
            if match:
                res = match.group(1).strip()
            else:
                raise Exception(f"Could not make match str: {match_re}, string: {res}")

        return res

    def get_gemalto_at_scfg(self):
        log.debug("Getting AT configs")

        res = self.execute_at("AT^SCFG?")
        match_re = r'\^SCFG: "(.*)","+(.*)"+'
        matches = re.findall(match_re, res)
        if matches:
            result = []
            for match in matches:
                result.append({
                    "Description": match[0],
                    "Value": match[1]
                })
            return result
        else:
            log.warning(f"Could not make match str: {match_re}, string: {res}")
            return []

    def get_at_functionality(self) -> str:
        res = self._intf.execute_command('AT+CFUN?')
        match = re.search(r"\+CFUN: (\d+),(\d+)", res)
        if match:
            mode = int(match.group(1))
            stk = int(match.group(2))

            if mode == 0:
                mode_str = "Switch_off"
            elif mode == 1:
                mode_str = "Full"
            elif mode == 4:
                mode_str = "Airplane"
            else:
                raise ValueError(f"unknown mode {mode}")
            return mode_str
        raise Exception(f"Could not get mode: response: {res}")

    def set_at_full(self):
        print("Set to full funcionality mode")
        res = self._intf.execute_command("AT+CFUN=1")

    def set_at_airplace(self):
        print("Set to airplane mode")
        res = self._intf.execute_command("AT+CFUN=4")

    def at_check(self):
        res = self._intf.execute_command("ATI")
        return True if "Cinterion" in res else False

    def ate(self):
        """
        Cancel the command echo

        Returns
        -------
            None
        """
        self._intf.execute_command("ATE")

    def at_reset(self, wait_reset=True, timeout=15):
        print("Reset gemalto with AT commands")
        self._intf.execute_command("AT+CFUN=1,1")

        time.sleep(40)  # wait for the device to reset

        # connect again
        print("Open again the interface")
        self._intf.reconnect()

        if wait_reset:
            startime = time.time()
            while time.time() - startime < timeout:
                if self.at_check():
                    self.ate()
                    return
            raise Exception("Timeout for reset exceeded")

    def force_network_search(self):
        log.info("Force network search")
        res = self._intf.execute_command("AT+COPS=0,2", timeout=120)
        log.debug(f"Result of network search: {res}")
        # necessary after at+cops=0
        time.sleep(5)
        self.at_check()

    def get_at_network(self):
        res = self._intf.execute_command("AT+COPS?")
        match = re.search(r"\+COPS: (\d+),?(\d*),?\"*(\w*)\"*,?(\d*)", res)

        if match:
            res_dict = {
                "mode": int(match.group(1)),
                # "status": COPS_status[int(match.group(2))],
                "mode": match.group(2),
                "network": match.group(3),
                "technology": COPS_technology[int(match.group(4))],
                "raw": res
            }
            return res_dict

        raise Exception(f"Could not execute COPS: {res}")

    @retry(Exception, nTries, 2, 1)
    def get_global_storage_stats(self):
        res = self._intf.execute_command('AT^SFSA="gstat","A:/"')
        data = re.findall(r"\^SFSA: (\d+)", res)
        res_dict = {
            "storage": int(data[0]),
            "freespace": int(data[1])
        }

        return res_dict

    @retry(Exception, nTries, 2, 1)
    def get_file_stats(self, filename):
        res = self._intf.execute_command(f'AT^SFSA="stat","{filename}"')
        data = re.findall(r"\^SFSA: (.+)\r", res)
        data_dict = {
            "filename": filename,
            "size": int(data[0]),
            "date_last_access": data[1],
            "date_last_modified": data[2],
            "date_last_file_stat_change": data[3],
            "stat_attribute": data[4]
        }
        return data_dict

    def copy_files(self, copy_dir, stats_list=None, base_dir="A:/", exclude_extensions=(".jad", ".jar")):
        if stats_list is None:
            stats_list = []
        print(f"Copying dir {copy_dir}, base_dir {base_dir}")
        if not os.path.exists(copy_dir):
            print(f"Creating dir {copy_dir}")
            os.mkdir(copy_dir)

        files = self.list_directory(base_dir)
        for filename in files:
            if filename.endswith("/"):  # is a dir
                print(f"copying dir {filename}")
                self.copy_files(os.path.join(copy_dir, filename), stats_list, os.path.join(base_dir, filename),
                                exclude_extensions)
            else:  # create filename
                complete_filename = os.path.join(base_dir, filename)
                stats = self.get_file_stats(complete_filename)
                print(f"Filename {complete_filename}, stats: {stats}")
                stats_list.append(stats)

                with open(os.path.join(copy_dir, filename), "w") as f:
                    print(f"Creating file {filename}")

                    if any([filename.endswith(ext) for ext in exclude_extensions]):
                        print("Not copying, just create file place holder")
                    else:
                        f.write(self.read_file(complete_filename))

        return stats_list

    def get_at_creg(self):
        res = self._intf.execute_command("AT+CREG?")
        match = re.search(r"\+CREG: (\d+),(\d+)", res)
        if match:
            res_dict = {
                "urc": match.group(1),
                "status": CREG[int(match.group(2))],
                "registered": CREG[int(match.group(2))].startswith("Registered")
            }
            return res_dict
        raise Exception(f"Could not execute creg: {res}")

    def get_at_greg(self):
        res = self._intf.execute_command("AT+CGREG?")
        match = re.search(r"\+CGREG: (\d+),(\d+)", res)
        if match:
            res_dict = {
                "urc": match.group(1),
                "status": CREG[int(match.group(2))],
                "registered": CREG[int(match.group(2))].startswith("Registered")
            }
            return res_dict
        raise Exception(f"Could not execute creg: {res}")

    def get_at_cell_information(self):
        res = self._intf.execute_command("AT^SMONI?")
        match = re.search(r"\+CGREG: (\d+),(\d+)", res)
        if match:
            res_dict = {
                "urc": match.group(1),
                "status": CREG[int(match.group(2))]
            }
            return res_dict
        raise Exception(f"Could not execute creg: {res}")

    def get_at_imsi(self):
        res = self._intf.execute_command("AT+CIMI")
        return res.strip()

    def get_at_imei(self):
        res = self._intf.execute_command("AT+CGSN")
        return res.strip()

    def get_at_manufacture(self):
        res = self._intf.execute_command("AT+CGMI")
        return res.strip()

    def get_at_model(self):
        res = self._intf.execute_command("AT+CGMM")
        return res.strip()

    def get_at_software(self):
        res = self._intf.execute_command("AT+CGMR")
        return res.strip()

    def get_identification(self):
        res = [{
            "Parameter": "Manufacture",
            "Value": self.get_at_manufacture()
        }, {
            "Parameter": "Model",
            "Value": self.get_at_model()
        }, {
            "Parameter": "Revision",
            "Value": self.get_at_model()
        }, {
            "Parameter": "IMSI",
            "Value": self.get_at_imsi()
        }, {
            "Parameter": "IMEI",
            "Value": self.get_at_imei()
        }
        ]

        return res

    def at_list_running_apps(self):
        res = self._intf.execute_command("AT^SJAM=5")
        match = re.findall(r"\^SJAM: (.+)\r", res)
        if match:
            return match
        raise ValueError(f"Could not get running apps: {res}")

    def at_list_installed_apps(self):
        res = self._intf.execute_command("AT^SJAM=4")
        match = re.findall(r"\^SJAM: (.+)\r", res)
        if match:
            return match
        raise ValueError(f"Could not get running apps: {res}")

    def list_directory(self, directory="") -> List[str]:
        res = self._intf.execute_command(f'AT^SFSA="ls","{os.path.join("A:/", directory)}"')
        files = re.findall(r"\^SFSA: \"(.+)\"", res)
        return files

    def read_file(self, filename: str) -> str:
        # self._intf.execute_command('at^sfsa="close",0')
        res = self._intf.execute_command(f'AT^SFSA="open","{filename}",2')
        if "^SFSA: 0,0" not in res:
            self._intf.execute_command('at^sfsa="close",0')
            res = self._intf.execute_command(f'AT^SFSA="open","{filename}",2')
            if "^SFSA: 0,0" not in res:
                raise Exception(f"Could not open file {filename}")
        data = ""
        size = 1024
        while True:
            res = self._intf.execute_command(f'AT^SFSA="read",0,{size}')
            if not res or "^SFSA: 0,0" in res:
                break
            data_raw = res.split("\r\n", 2)[-1]
            if self._cli:
                data += data_raw[:-7]
            else:
                data += data_raw

        self._intf.execute_command('at^sfsa="close",0')
        time.sleep(2)  # time indicate on document after closing
        return data

    def execute_pre_dali(self):
        res = self._intf.execute_fct_command([0x83, 0x04, 0x81, 0x01, 0x17, 0x10])
        return res

    def perform_dali_reinit(self):
        log.debug("Start dali reinit")
        data = P7CM_FCT.make_package(P7CM.CMD_DALI_REINIT)
        response_data = self._intf.execute_fct_command(data["raw"])
        n_drivers = int.from_bytes(response_data[3:5], byteorder='big', signed=False)
        log.debug(f"Number of devices detected: {n_drivers}")
        return n_drivers

    def set_dali_fct_mode(self, state: bool):
        log.info(f"Set Dali FCT mode: {state}")
        data = P7CM_FCT.make_package(P7CM.CMD_DALI_FCT_MODE, [int(bool(state))])
        response_data = self._intf.execute_fct_command(data["raw"])
        log.debug(f"Dali FCT mode set: {response_data}")
        return response_data

    def set_xbee_fct_mode(self, state: bool):
        log.info(f"Set XBEE FCT mode: {state}")
        data = P7CM_FCT.make_package(P7CM.CMD_XBEE_FCT_MODE, [int(bool(state))])
        response_data = self._intf.execute_fct_command(data["raw"])
        log.debug(f"Xbee FCT mode set: {response_data}")
        return response_data

    def set_dali_dimming_fct(self, short_address, dimming):
        log.info(f"Setting Dali address: {short_address} with dimming {dimming}")
        data = P7CM_FCT.make_package(P7CM.CMD_DALI_DIMMING, [int(short_address), int(dimming)])
        response_data = self._intf.execute_fct_command(data["raw"])
        log.debug(f"Dali dimming set: {response_data}")
        return response_data

    def set_1_10v_dimming_fct(self, dimming):
        log.info(f"Set 1_10V dimming to {dimming}")
        data = P7CM_FCT.make_package(P7CM.CMD_1_10V_DIMMING, [int(dimming)])
        response_data = self._intf.execute_fct_command(data["raw"])
        log.debug("1_10V dimming set: {response_data}")
        return response_data

    def get_1_10v_feedback_fct(self):
        log.debug("Get 1-10V feedback")
        data = P7CM_FCT.make_package(P7CM.CMD_1_10V_FEEDBACK)
        response_data = self._intf.execute_fct_command(data["raw"])
        feedback = int.from_bytes(response_data[3:4], byteorder='big', signed=False) * 0.1
        log.info(f"1-10V Feedback: {feedback}")
        return feedback

    def get_gps_position_fct(self, comport, timeout=60):
        # set usb port
        res = self.execute_at('AT^SGPSC="Nmea/Interface","usb5"')
        log.info(f"Set gps nema port: {res}")
        # enable port
        interface = serial.Serial(comport, 115200, timeout=timeout)

        res = self.execute_at('AT^SGPSC="engine","3"')
        log.info(f"Set gps enable: {res}")

        starttime = time.time()

        result = {
            "fix": False
        }

        data = ""
        while time.time() - starttime < timeout:
            new_data = interface.readline().decode("ascii").strip()
            log.debug(new_data)
            if "$GPGGA" in new_data:
                log.debug(f"GPS data: {new_data}")
                pos = pynmea2.parse(new_data)
                if pos.timestamp is not None:
                    log.debug("Position found")
                    result["fix"] = True
                    result["time"] = pos.timestamp
                    result["latitude"] = float(pos.lat)
                    result["longitude"] = float(pos.lon)
                    result["num_satellites"] = int(pos.num_sats)
                    break
            time.sleep(0.1)
            data += new_data

        return result

    def get_gps_configs_at(self):
        data = self.execute_at("AT^SGPSC?")
        return data


def relay_test():
    from hat.drivers.AcSourceInstrument import AcSourceInstrument

    # wait time
    wait_time = 5  # seconds sleep

    # connect AC source
    COM_port = "ASRL40::INSTR"
    dev = AcSourceInstrument(COM_port, reset=False)
    dev.clear()

    log.info("Staring test script")
    serial_port = "COM9"

    g3 = P7CM(serial_port)
    version = g3.execute_command("version")
    log.info(f"Version: {version}")

    data = DataStorage("data/results_%Y%m%d_%H%M%S.xlsx")

    index = 1

    while True:
        try:
            data["index"] = index
            data["Time"] = time.asctime()
            lux, status = g3.read_lux()
            log.info(f"Lux: {lux}, status {status}")
            data["lux"] = lux
            data["status"] = status

            status, required = g3.get_relay()
            data["Relay actual"] = status
            data["Relay required"] = required

            # get data from source
            power = dev.get_power_real()
            data["Power"] = power

            print(data.last_meas_str())
            index += 1
            data.start_next_test()
            log.info(f"Spleeping {wait_time} seconds")
            time.sleep(wait_time)
            if index == 3000:
                break
        except Exception as e:
            log.exception("Error")

    dev.set_output_state(False)
    data.close()


def gps_test():
    COM_port = "COM96"

    p7cm = P7CM(COM_port)
    for i in range(100):
        gps = p7cm.get_gps()
        print(f"GPS: {gps}")
        time.sleep(1)

    print("End!")


def fct_test():
    # relay_test()
    # data = [0xE0, 0x02, 0x04, 0xE6]
    data = 0x4
    p7cm = P7CM_FCT("192.168.1.96", 10)
    for i in range(20):
        data = p7cm.get_mcu_version()
        time.sleep(1)

    data = p7cm.get_rfid_data()

    p7cm.set_rfid_switch(True)
    p7cm.set_rfid_switch(False)

    data = p7cm.get_photocell()

    data = p7cm.get_i2c_devices()

    no_load, voltage_ms = p7cm.get_no_load_bit()
    print(f"No load bit: {no_load}, voltage: {voltage_ms}")

    p7cm.switch_relay(True, False)
    time.sleep(1)
    p7cm.switch_relay(False, False)
    time.sleep(1)
    p7cm.switch_relay(True, True)
    time.sleep(1)
    p7cm.switch_relay(False, True)
    time.sleep(1)
    version = p7cm.get_mcu_bootloader_version()
    print(f"Bootloader {version}")

    version = p7cm.get_mcu_version()
    print(f"Response: {version}")
    product_key = p7cm.get_product_key()
    print(f"Product key {product_key}")

    serial_number = p7cm.get_production_serial_number()
    print(f"Serial number {serial_number}")

    version = p7cm.get_mpu_version()
    print(f"Response: {version}")

    # version = p7cm.send_cmd(0x03)
    # print(f"Response: {version}")

    version = p7cm.execute_fct_cmd(0x10, [0x03])
    print(f"Response: {version}")

    photo = p7cm.execute_fct_cmd(0x40, [])

    rfid = p7cm.execute_fct_cmd(0x50, [1])

    eui = p7cm.execute_fct_cmd(0x91)
    # rfid_data = p7cm.send_cmd(0x51)


def main():
    import time
    import pandas as pd

    network_registration = True
    get_datapoints = True
    file_copy = False

    dirname = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\07_Returned_Devices\0013A200041BCBF5D\files"

    data_point_template = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet IOT\Project Folder\07_Returned_Devices\LuCo IOT datapoint commands template.xlsx"

    p7cm = P7CM("COM73")
    p7cm._intf.set_prompt("\r\nOK\r\n")

    id = p7cm.get_identification()
    print(f"ID: {id}")
    print("Create file directory")

    if not os.path.exists(dirname):
        os.mkdir(dirname)

    if network_registration:
        apps = p7cm.at_list_running_apps()

        print(f"Running {apps}")

        apps = p7cm.at_list_installed_apps()

        to_file(dirname, "app_running.json", apps)

        print(f"Installed {apps}")

        creg = p7cm.get_at_creg()
        print(f"creg {creg}")

        cgreg = p7cm.get_at_greg()
        print(f"cgreg {cgreg}")

        p7cm.at_reset()

        res = p7cm.get_at_functionality()
        print(f"Functionality {res}")

        p7cm.set_at_full()
        time.sleep(4)

        res = p7cm.get_at_functionality()
        print(f"Functionality {res}")
        time.sleep(10)
        p7cm.at_check()
        time.sleep(1)
        p7cm.force_network_search()
        time.sleep(1)

        res = p7cm.get_at_network()
        while res["status"] != "Registered":
            print(res)
            res = p7cm.get_at_network()

        network = p7cm.get_at_network()
        to_file(dirname, "network.json", network)

    if get_datapoints:
        data = pd.read_excel(data_point_template)
        for idx, row in data.iterrows():
            pass

    if file_copy:
        res = p7cm.get_global_storage_stats()
        with open(os.path.join(dirname, "global_stats.json"), "w") as f:
            json.dump(res, f)

        stats_dict = p7cm.copy_files(dirname)
        with open(os.path.join(dirname, "stats.json"), "w") as f:
            json.dump(stats_dict, f)

        print(f"Complete stats: {stats_dict}")

        # read file
        # p7cm.read_file("A:/Configuration/JsonDBBackup")
        #
        directories = p7cm.list_directory()
        print(f"directories: {directories}")
        directories = p7cm.list_directory("Configuration/")
        print(f"directories: {directories}")

    print("End!")


log.info("P7CM library loaded")

if __name__ == "__main__":
    eui = "0013A20041B472A5"
    serial_port = "COM2"
    p7cm = P7CM(serial_port, interface_type="ZIGBEE", eui=eui)

    version = p7cm.execute_command("version")
    print(f"Version: {version}")

    print("end!")
