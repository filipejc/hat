from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

TRIES = 3


class InfluxInterface:
    def __init__(self, host, port, token, org, buck_name):
        self._db = InfluxDBClient(f"http://{host}:{port}", token, buck_name)
        self._write_api = self._db.write_api(write_options=SYNCHRONOUS)
        self._org = org
        self._buck_name = buck_name

    def insert_point(self, measurement, tags, fields):
        point = Point(measurement)
        for tag_name, tag_value in tags.items():
            point.tag(tag_name, tag_value)

        for field_name, field_value in fields.items():
            point.field(field_name, field_value)

        point.time(datetime.utcnow(), WritePrecision.NS)

        self._write_api.write(self._buck_name, self._org, point)

    def query(self, measurement, tags={}):
        data = self._db.query_api(f"select * from {measurement}")
        points = list(data.get_points(measurement=measurement, tags=tags))
        return points


if __name__ == "__main__":
    import random

    print(f"Staring influx db test")
    token = "q0Iq2FYw72ZFd7vywRBlxh3jvutb2AL-1f0eGQJc0jRwkZTuThxMXg8yoAzc3Zu5VFjACVr53yfTZJnRx9BTNw=="
    db = InfluxInterface('127.0.0.1', 8086, token, "hyperion", "data")
    # for i in range(10):
        #     db.insert_point("data", {"device": "meter", "location": "home"}, {"luminance": random.randint(0, 100)})

    data = db.query("data")
    print(data)
    print("end!")
