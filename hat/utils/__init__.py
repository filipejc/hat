"""
# Filename:     __init__.py
# Author:       Filipe Coelho
# E-mail:       fcoelho@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20/04/2021
# Last Update:  20/04/2021
#
# Version:      1.0
# Filetype:     
# Description:  Module to store utils function and classes used by other parts of the framework
# STATUS:       Stable
# Limitation:   None
"""
