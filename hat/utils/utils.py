import socket
from functools import wraps

import logging
import time
import os
import pandas as pd
import json
from typing import List

import smtplib, ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

LOG_FILENAME = 'logs/utils.txt'
CONFIG_FILENAME = os.path.join(os.path.dirname(__file__), 'config.json')

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")


def create_logger(logger_name, logger_level=logging.INFO):
    """
    Create logger using standard lib logger

    Parameters
    ----------
    logger_name: str
        Name of the logger to be created
    logger_level: logging.level, default=INFO
        level of the logger

    Returns
    -------
    logging.logger
        logger to be used in the module
    """
    logger = logging.getLogger(logger_name)

    # add a stream logger
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logger_level)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to stream handler
    stream_handler.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(stream_handler)

    logger.setLevel(logger_level)
    return logger


def list_to_hex_string(data: List[int]) -> str:
    """
    Convert a list of int to a hex string

    Parameters
    ----------
    data: List of int
        data to convert to hex represention

    Returns
    -------
    str
        Hex string representation
    """
    return " ".join(["{:02X}".format(value) for value in data])


def dict_to_file(dirname, filename, obj, save_excel=True):
    """
    object should be list of dictionaries with same keys

    Parameters
    ----------
    dirname
    filename
    obj

    Returns
    -------

    """
    # create dirname
    if not os.path.exists(dirname):
        os.mkdir(dirname)

    # filename for json
    json_filename = filename + ".json"
    with open(os.path.join(dirname, json_filename), "w") as f:
        json.dump(obj, f)

    if save_excel:
        # filename for xlsx
        xlsx_filename = filename + ".xlsx"
        data = pd.DataFrame(obj)
        data.to_excel(os.path.join(dirname, xlsx_filename))


def get_ip_from_hostname(hostname):
    try:
        host_ip = socket.gethostbyname(hostname)
        print("Hostname :  ", hostname)
        print("IP : ", host_ip)
        return host_ip
    except:
        print("Unable to get Hostname and IP")


def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
    """Retry calling the decorated function using an exponential backoff.

    http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry

    :param ExceptionToCheck: the exception to check. may be a tuple of
        exceptions to check
    :type ExceptionToCheck: Exception or tuple
    :param tries: number of times to try (not retry) before giving up
    :type tries: int
    :param delay: initial delay between retries in seconds
    :type delay: int
    :param backoff: backoff multiplier e.g. value of 2 will double the delay
        each retry
    :type backoff: int
    :param logger: logger to use. If None, print
    :type logger: logging.Logger instance
    """

    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print(msg)
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry


def send_email(subject: str, receiver_email: List[str], body: str, attachments: List[str] = None):
    """
    Function that send an email using schreder.hyperion.automation@gmail.com email.
    Can be used to inform people about the status of a log automated test or unexpected errors during a script.

    Parameters
    ----------
    subject : str
        Subject of email to send
    receiver_email : List[str]
        List of email addresses that should receive email message
    body : str
        Message to send in the body of email
    attachments : List[str], optional
        list of filenames to attached to the email

    Returns
    -------
        None
    """
    log.info(f"Creating email with subject: {subject} for receivers: {receiver_email}")
    if attachments is None:
        attachments = []

    with open(CONFIG_FILENAME) as f:
        email_confs = json.load(f)

    # email
    sender_email = email_confs["email_from"]
    port = email_confs["email_port"]  # For SSL
    password = email_confs["email_password"]
    server_link = email_confs["email_server"]

    # Create a secure SSL context
    context = ssl.create_default_context()

    # constract email
    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    message["From"] = sender_email
    message["To"] = receiver_email

    message.attach(MIMEText(body, "plain"))

    for attachment_name in attachments:
        log.debug(f"Adding file {attachment_name} to the email.")
        # The email client will try to render the last part first
        # Open PDF file in binary mode
        with open(attachment_name, "rb") as attachment:
            # Add file as application/octet-stream
            # Email client can usually download this automatically as attachment
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())

        # Encode file in ASCII characters to send by email
        encoders.encode_base64(part)

        # Add header as key/value pair to attachment part
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {attachment_name}",
        )

        # Add attachment to message and convert message to string
        message.attach(part)

    # composer from
    text = message.as_string()

    with smtplib.SMTP_SSL(server_link, port, context=context) as server:
        log.debug("Connecting to email server.")
        server.ehlo()
        server.login(sender_email, password)

        log.debug("Sending email...")
        server.sendmail(sender_email, receiver_email, text)

        log.debug("Email send completed!")

# TODO: Create a file manager to control and facilitate control of all files
"""
 features:
 * generate automatically filenames in a certain test directory with possible time formating
 * can generate DataStorage at request
 * create a zip file with all the files that were created
"""
