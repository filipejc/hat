import os
import subprocess
import shlex


def install_stm32_fw(stm_exe, stm_hex, options=""):
    print(f"Installing STM firmware {stm_hex}")
    if "erase" in options:
        paramters = shlex.split(f'"{stm_exe}" -c port=SWD mode=UR {options} -w "{stm_hex}"')
    else:
        paramters = shlex.split(f'"{stm_exe}" -c port=SWD mode=UR -w "{stm_hex}" {options}')
    process = subprocess.run(paramters,
                             stdout=subprocess.PIPE,
                             universal_newlines=True)

    if not "File download complete" in process.stdout:
        raise Exception(f"Problem programming {process.stdout}")
    print(f"Installation ok")

    paramters = shlex.split(f'"{stm_exe}" -c port=SWD mode=UR -hardRst')
    process = subprocess.run(paramters,
                             stdout=subprocess.PIPE,
                             universal_newlines=True)
    if not "Hard reset is performed" in process.stdout:
        raise Exception(f"Problem programming {process.stdout}")


def install_efr32_fw(efr_exe, efr_hex):
    print(f"Installing EFR32 firmware {efr_hex}")
    parameters = shlex.split(f'"{efr_exe}" device info --tif SWD --speed 1000 --device EFR32MG13P632F512GM32')
    process = subprocess.run(parameters,
                             stdout=subprocess.PIPE,
                             universal_newlines=True)

    if "ERROR" in process.stdout:
        raise Exception(f"Problem programming {process.stdout}")

    parameters = shlex.split(f'"{efr_exe}" flash "{efr_hex}" --tif SWD --speed 1000 --device EFR32MG13P632F512GM32')

    process = subprocess.run(parameters,
                             stdout=subprocess.PIPE,
                             universal_newlines=True)

    if not "DONE" in process.stdout:
        raise Exception(f"Problem programming {process.stdout}")

    print(f"Installation ok")


if __name__ == "__main__":
    # variables
    stm_exe = r"C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"
    stm_hex = r"C:\Users\fcoelho\Downloads\20210105_complete.hex"

    efr_exe = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\01_HW\03_Calculations\Silabs_Flash_support\Zigbee firmware flash-selected\commander\commander\commander.exe"
    efr_hex = r"C:\Users\fcoelho\Schréder SA\HYP - Products - Documents\Owlet FOUR\Project Folder\02_FW\01_Binaries\EFR32_bins\22-12-2020\g4-rail-firmware.s37"

    # process stm32
    # install_stm32_fw(stm_exe, stm_hex)

    # process efr32
    install_efr32_fw(efr_exe, efr_hex)

    print("end!")
