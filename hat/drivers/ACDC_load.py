import logging
from logging.handlers import RotatingFileHandler
from hat.drivers.Instruments import Instrument

LOG_FILENAME = 'ACDC_load.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("DEBUG")

DEFAULT_BAUDRATE = 57600

LOAD_MODES = {"CURR", "POW", "VOLT", "RES", "RLC", "RLCP", "INRUSH", "RECT"}


class ACDCLoad(Instrument):
    def __init__(self, address, baud_rate=DEFAULT_BAUDRATE, write_termination="\n", execute_idn=True, **kwargs):
        super(ACDCLoad, self).__init__(address, write_termination=write_termination, execute_idn=execute_idn,
                                       baud_rate=baud_rate, **kwargs)

    def reset(self):
        sleep_time = 20
        log.debug(f"Doing reset, waiting {sleep_time} seconds before continuing")
        cmd = "*RST"
        self._intf.write(cmd)

        self.check_errors(cmd, sleep_time, True)  # user manual referes waiting 7 seconds

    def get_current_AC(self):
        current = float(self.query("measure:current:ac?"))
        return current

    def get_power_real(self):
        power = float(self.query("measure:power:real?"))
        return power

    def get_voltage_rms(self):
        voltage = float(self.query("measure:voltage?"))
        return voltage

    # def set_voltage_DC(self, voltage):
    #     self.write("voltage:dc %.1f" % voltage)
    #
    # def set_frequency(self, frequency):
    #     log.debug(f"Set frequency to {frequency}")
    #     self.write(f"frequency {frequency:.1f}")
    #
    # def set_voltage_AC(self, voltage):
    #     self.write("voltage:ac %.1f" % voltage)

    def get_output_state(self):
        state = True if "0" in self.query("load?") else False
        return state

    def set_output_state(self, state):
        self.write(f"LOAD {'ON' if state else 'OFF'}")
        # self.write(f"output:relay {'ON' if state else 'OFF'}")

    def get_load_mode(self):
        mode = self.query("load:mode?").strip()
        return mode

    def set_load_mode(self, mode):
        log.debug(f"Configure mode to {mode}")
        if mode.upper() not in LOAD_MODES:
            raise ValueError(f"Value mode {mode} not in valid modes: {LOAD_MODES}")
        self.write(f"LOAD:MODE {mode}")

    # def set_pulse_AC_voltage(self, volt_AC):
    #     self.write("pulse:voltage:ac %.1f" % volt_AC)
    #
    # def set_pulse_DC_voltage(self, volt_DC):
    #     self.write("pulse:voltage:dc %.1f" % volt_DC)
    #
    # def set_pulse_freq(self, frequency):
    #     self.write("pulse:frequency %.1f" % frequency)
    #
    # def set_pulse_count(self, count):
    #     self.write("pulse:count %d" % count)
    #
    # def set_pulse_duty_cycle(self, duty_cycle):
    #     self.write("pulse:DCYCle %.1f" % duty_cycle)
    #
    # def set_pulse_period_ms(self, period_ms):
    #     self.write("pulse:period %.1f" % period_ms)
    #
    # def config_pulse_mode(self, volt_AC, volt_DC, frequency, count, duty_cycle, period_ms):
    #     log.info("Configuring pulse mode")
    #     # set trigger OFF
    #     # self.set_trigger(False)
    #
    #     # set mode fixed in order to change the parameter
    #     self.set_source_mode("FIXED")
    #
    #     log.debug(f"Set AC voltage to {volt_AC}")
    #     self.set_pulse_AC_voltage(volt_AC)
    #
    #     log.debug(f"Set DC voltage to {volt_DC}")
    #     self.set_pulse_DC_voltage(volt_DC)
    #
    #     log.debug(f"Set frequency {frequency}")
    #     self.set_pulse_DC_voltage(volt_DC)
    #
    #     log.debug(f"Set count {count}")
    #     self.set_pulse_count(count)
    #
    #     log.debug(f"Set duty cycle {duty_cycle}")
    #     self.set_pulse_duty_cycle(duty_cycle)
    #
    #     log.debug(f"Set period {period_ms}")
    #     self.set_pulse_period_ms(period_ms)
    #
    #     self.set_source_mode("PULSE")

    def get_protection_status(self):
        status = int(self.query("LOAD:PROTECT?"))
        log.info(f"Protection status: {status}")
        return status

    def clear_protection_status(self):
        log.info("clear protect status")
        self.write("LOAD:PROTECT:CLE")

    def set_trigger(self, state):
        log.info(f"Setting trigger state: {state}")
        self.write(f"trig {'ON' if state else 'OFF'}")

    def get_trigger(self):
        state = False if "OFF" in self.query("trig:state?").strip() else True
        return state

    def set_current_AC(self, current):
        log.info(f"Set current to {current}A")
        self.write(f"LOAD:CURRENT:LEVEL:AMPLITUDE:AC {current}")

    def get_target_current_AC(self):
        current = self.query("LOAD:CURRENT:LEVEL:AMPLITUDE:AC?")
        log.info(f"Target Current: {current}A")
        return current

    def set_peak_current_AC(self, current):
        log.info(f"Set peak current to {current}")
        self.write(f"LOAD:CURRENT:PEAK:MAX {current}")

    def set_power_factor(self, pfactor):
        log.info(f"Set power factor to {pfactor}")
        self.write(f"LOAD:PFACTOR:LEVEL:AMPLITUDE:AC {pfactor}")

    def get_power_factor(self):
        pfactor = self.query("LOAD:PFACTOR:LEVEL:AMPLITUDE:AC?")
        log.info(f"Target Power Factor: {pfactor}")
        return pfactor        

    def set_crest_factor(self, cfactor):
        log.info(f"Set crest factor to {cfactor}")
        self.write(f"LOAD:CFAC:LEVEL:AMPLITUDE:AC {cfactor}")

    def get_crest_factor(self):
        cfactor = self.query("LOAD:CFAC:LEVEL:AMPLITUDE:AC?")
        log.info(f"Target Crest Factor: {cfactor}")
        return cfactor        


if __name__ == "__main__":
    log.info("AC source interface")

    COM_port = "ASRL7::INSTR"
    dev = ACDCLoad(COM_port, reset=False)
    # dev.clear()
    #
    dev.get_current_AC()
    dev.get_power_real()
    dev.get_voltage_rms()

    dev.set_load_mode("CURR")
    dev.set_current_AC(0.1)
    dev.get_target_current_AC()

    dev.get_protection_status()

    dev.set_output_state(True)
    #
    # dev.get_output_state()
    # dev.set_output_state(False)
    # log.info(f"State should be true {dev.get_output_state()}")
    # dev.set_output_state(True)
    # log.info(f"State should be false {dev.get_output_state()}")
    #
    # mode = dev.get_source_mode()
    # log.info(f"Mode {mode}")
    # dev.set_source_mode("STEP")
    # mode = dev.get_source_mode()
    # log.info(f"mode should be STEP: {mode}")
    #
    # dev.config_pulse_mode(100, 0, 50, 10, 50, 2000)
    #
    # error = dev.get_system_error()
    # log.info(f"Error {error}")
    #
    # dev.set_trigger(True)
    # trig = dev.get_trigger()
    # log.info(f"Trigger {trig}")

    log.info("End!")
