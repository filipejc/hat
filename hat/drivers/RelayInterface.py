import json
import time
try:
    from  pyfirmata import Arduino, util
except ImportError:
    print("Will not able to use Relay board based on Arduino")

import logging
import os, sys

# from Automation.BDaq import *
from Automation.BDaq.InstantDoCtrl import InstantDoCtrl
from Automation.BDaq.BDaqApi import AdxEnumToString, BioFailed

log = logging.getLogger("Relay interface")
handler = logging.StreamHandler()
handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
handler.setFormatter(formatter)

# add ch to logger
log.addHandler(handler)

log.setLevel("DEBUG")


class RelayInterface:
    ROBOT_LIBRARY_SCOPE = 'SUITE'

    def __init__(self, address, config_file):
        self._intf = None
        self._config = {}
        self._labels = {}
        self._open = False
        self._address = address
        self._config_file = config_file

    def is_open(self):
        return self._open

    def open(self, address=None):
        if self.is_open():
            log.warning("Already opened")
            return False

        if address is not None:
            self._address = address

        log.info(f"Opening Relay Interface on address {self._address}")
        self._intf = Arduino(self._address)
        if self._config_file is not None:
            self.load_config(self._config_file)
        self._open = True

    def config_relay(self, relay_number, arduino_pin):
        log.debug(f"Config relay: {relay_number} to pin {arduino_pin}")
        self._config[relay_number] = arduino_pin

    def set_relay(self, relay_number, status):
        if not self.is_open():
            self.open()

        log.debug(f"Set relay {relay_number} to status {status}")
        if relay_number not in self._config:
            raise Exception(f"Relay number {relay_number} not yet configured")

        self._intf.digital[self._config[relay_number]].write(not status)

    def config_label(self, label, positions):
        self._labels[label] = []
        for position in positions:
            self._labels[label].append(position)

    def load_config(self, config_file):
        with open(config_file) as f:
            confs = json.load(f)

        # channels
        for conf in confs["channels"]:
            self.config_relay(conf["index"], conf["pin"])

        # positions
        for conf in confs["labels"]:
            self.config_label(conf["name"], conf["positions"])

    def set_position_by_label(self, label):
        log.info(f"Set relay to position {label}")
        if not self.is_open():
            self.open()

        if label not in self._labels:
            raise Exception(f"Label {label} not present")

        positions = self._labels[label]
        for position in positions:
            if "index" in position:
                self.set_relay(position["index"], position["state"])
            elif "label" in position:
                self.set_position_by_label(position["label"])
            else:
                raise ValueError(f"Unknown position type: {position}")

    def close(self):
        if self.is_open():
            self._intf.exit()
            self._open = False

    def __del__(self):
        self.close()


class AdvantechInterface(RelayInterface):
    def __init__(self, address, config_file=None):
        super().__init__(address, config_file)
        self._profilePath = u"..\\..\\profile\\DemoDevice.xml"
        self._inst = None

    def open(self, address=None):
        if self.is_open():
            log.warning("Already opened")
            return False

        if address is not None:
            self._address = address

        log.info(f"Opening Relay Interface on address {self._address}")

        # connecting to device
        self._inst = InstantDoCtrl(self._address)
        self._inst.loadProfile = self._profilePath

        if self._config_file is not None:
            self.load_config(self._config_file)
        self._open = True

    def set_relay(self, relay_number, status):
        if not self.is_open():
            self.open()

        log.debug(f"Set relay {relay_number} to status {status}")

        # set relay

        ret = self._inst.writeBit(0, relay_number, status)
        # ret = self._inst.writeAny(0, 1, [1 << relay_number] if status else [0])
        if BioFailed(ret):
            raise Exception(f"Could not change relay")

    def close(self):
        if self._inst is not None:
            self._inst.dispose()


if __name__ == "__main__":
    def relay_test():
        board = RelayInterface("COM3")

        board.read_config("relay_config.json")

        # for label in board._labels.keys():
        #     board.set_position_by_label(label)

        board.set_position_by_label("uart")
        board.set_position_by_label("lsi_inactive")

    def advantech_test():
        address = "USB-4761, BID#0"
        rel = AdvantechInterface(address)
#        for idx in range(7):
        rel.set_relay(0, False)
        time.sleep(0.1)

    advantech_test()

    print("end!")
