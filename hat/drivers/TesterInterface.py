import requests

n_tries = 3  # number of tries


class TesterInterface:
    def __init__(self, timeout=10):
        self._ip = None
        self._timeout = timeout

    def open(self, address):
        self._ip = address

    def get_request(self, path, conv=float):
        comp_path = f'http://{self._ip}/{path}'
        for i in range(n_tries):
            try:
                r = requests.get(comp_path, timeout=self._timeout)
                break
            except requests.exceptions.ReadTimeout as e:
                print(f"Read time: {e.msg}")

        if r.status_code != 200:
            raise Exception(f"Could not get request: {comp_path}")
        value = conv(r.text)
        return value

    def get_1_10v_feedback(self):
        value = self.get_request("1_10v_value")
        return value

    def get_light(self):
        value = self.get_request("light")
        return value


if __name__ == "__main__":
    print("Tester interface")

    test = TesterInterface("192.168.1.83")

    value_1_10v = test.get_1_10v_feedback()
    print(f"1_10V: {value_1_10v}")

    light = test.get_light()
    print(f"Light {light}")

    print("end!")
