import logging
from logging.handlers import RotatingFileHandler
from hat.drivers.Instruments import Instrument

LOG_FILENAME = 'PM_1000.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("DEBUG")

PM1000_MODES = {
    0: "NORMAL",
    1: "BALLAST",
    2: "INRUSH",
    3: "STANDBY",
    4: "INTEGRATOR"
}


class AcPowerMeterInstrument(Instrument):
    ROBOT_LIBRARY_SCOPE = 'SUITE'

    def __init__(self, address, write_termination="\n", *args, **kwargs):
        super(AcPowerMeterInstrument, self).__init__(address, write_termination=write_termination, check_errors=False, *args, **kwargs)
        self._meas_names = []

    def clear_measurements(self):
        # log.info("Clear measurements")
        self.write(":SEL:CLR")

    def configured_measurements(self):
        meas = self.query(":FRF?")
        # log.info(f"Current measurements are {meas}")
        return meas

    def add_meas_voltage_rms(self):
        # log.info("Add measurement VRMS")
        self.write(":SEL:VLT")
        self._synchronize_measurement_names()

    def get_voltage_rms(self) -> float:
        if "Vrms" not in self._meas_names:
            self.add_meas_voltage_rms()
        meas = self.get_measurement()
        return meas["Vrms"]

    def add_meas_current_rms(self):
        # log.info("Add measurement IRMS")
        self.write(":SEL:AMP")
        self._synchronize_measurement_names()

    def add_meas_active_power(self):
        self.write(":SEL:WAT")
        self._synchronize_measurement_names()

    def add_meas_frequency(self):
        self.write(":SEL:FRQ")
        self._synchronize_measurement_names()

    def get_frequency(self) -> float:
        if "FRQ" in self._meas_names:
            self.add_meas_current_rms()
        meas = self.get_measurement()
        return meas["Freq"]

    def get_active_power(self):
        meas = self.get_measurement()
        return meas["Watt"]

    def get_current_rms(self) -> float:
        if "IRMS" in self._meas_names:
            self.add_meas_current_rms()
        meas = self.get_measurement()
        return meas["IRMS"]

    def get_measurement(self, auto_synchronize: bool = True):
        measurements = self.query(":FRD?").strip().split(",")
        results = {}
        if len(measurements) != self._meas_names:
            if auto_synchronize:
                self._synchronize_measurement_names()
                names = self._meas_names
            else:
                names = [f"meas_{x}" for x in range(len(measurements))]
        else:
            names = self._meas_names

        for name, measurement in zip(names, measurements):
            results[name] = float(measurement)

        # log.info(f"Current values: {results}")
        return results

    def _synchronize_measurement_names(self):
        meas = self.configured_measurements()
        self._meas_names = meas.strip().split(",")

    def set_integrator_mode(self):
        # log.info("Set integrator mode")
        self.write(":MOD:INT")

    def set_normal_mode(self):
        # log.info("Set normal mode")
        self.write(":MOD:NOR")

    def get_current_mode(self) -> str:
        mode = int(self.query(":MOD?"))
        mode_str = PM1000_MODES[mode]
        # log.info(f"Current mode {mode_str}")
        return mode_str

    def set_display_mode_large(self, large: bool):
        # log.info(f"Set display mode {'Large' if large else 'small'}")
        self.write(":DSP:Z04" if large else ":DSP:Z14")

    def add_meas_WHR(self):
        # log.info("Add measurement WHR")
        self.write(":SEL:WHR")
        self._synchronize_measurement_names()

    def add_meas_integration_time(self):
        # log.info("Add integration time")
        self.write(":SEL:HRS")

    def conf_integrator_manual_mode(self):
        # log.info("Configure integrator manual mode")
        self.write(":INT:START 0    ")

    def start_integrator(self):
        # log.info("Start integrator")
        self.write(":INT:MAN:RUN")

    def stop_integrator(self):
        # log.info("Stop integrator")
        self.write(":INT:MAN:STOP")

    def reset_integrator(self):
        # log.info("Reset integrator")
        self.stop_integrator()
        self.write(":INT:RESET")


if __name__ == "__main__":
    import time

    log.info("Created Driver for PM1000+")

    USB_connection_string = "USB0::0xABCD::0x03E8::100008201733::INSTR"

    pm = AcPowerMeterInstrument(USB_connection_string)

    pm.reset()

    pm.clear_measurements()

    pm.get_voltage_rms()

    pm.add_meas_voltage_rms()
    pm.add_meas_current_rms()

    pm.configured_measurements()
    pm.get_measurement(False)
    pm.get_measurement()

    pm._synchronize_measurement_names()

    pm.get_measurement()

    pm.get_current_mode()

    pm.set_integrator_mode()

    pm.get_current_mode()

    pm.set_display_mode_large(False)

    pm.add_meas_integration_time()

    pm.conf_integrator_manual_mode()

    # start integrator
    pm.start_integrator()

    time.sleep(5)

    pm.stop_integrator()

    pm.get_measurement()

    log.info("END!")
