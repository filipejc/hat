import time
import logging
from logging.handlers import RotatingFileHandler
from hat.drivers.Instruments import Instrument

LOG_FILENAME = 'AC_source.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
file_handler = RotatingFileHandler(
    LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

log.setLevel("DEBUG")

DEFAULT_BAUDRATE = 19200

AC_SOURCE_MODES = {"FIXED", "LIST", "PULSE", "STEP", "SYNTH", "INTERHAR"}


class AcSourceInstrument(Instrument):
    ROBOT_LIBRARY_SCOPE = 'SUITE'

    def __init__(self, address, baud_rate=DEFAULT_BAUDRATE, write_termination="\r", execute_idn=True, **kwargs):
        super(AcSourceInstrument, self).__init__(address, write_termination=write_termination, execute_idn=execute_idn,
                                                 baud_rate=baud_rate, **kwargs)

    def reset(self):
        sleep_time = 20
        log.debug(f"Doing reset, waiting {sleep_time} seconds before continuing")
        cmd = "*RST"
        self._intf.write(cmd)

        self.check_errors(cmd, sleep_time, True)  # user manual referes waiting 7 seconds

    def set_ac_configuration(self, voltage, frequency, state):
        """
        Function to set AC configuation in a single line.
        Set voltage, frequency and state

        """
        self.set_voltage_AC(voltage)
        self.set_frequency(frequency)
        self.set_output_state(state)

    def get_phase_on(self) -> float:
        angle = float(self.query("phase:on?"))
        log.debug(f"Phase On: {angle} Degree")
        return angle

    def get_phase_off(self) -> float:
        angle = float(self.query("phase:off?"))
        log.debug(f"Phase Off: {angle} Degree")
        return angle

    def set_phase_on(self, angle: float):
        log.debug(f"Set Phase On: {angle} Degree")
        self.write(f"phase:on {angle}")

    def set_phase_off(self, angle: float):
        log.debug(f"Set Phase Off: {angle} Degree")
        self.write(f"phase:off {angle}")

    def get_current_AC(self) -> float:
        current = float(self.query("measure:current:ac?"))
        log.debug(f"Get AC current: {current}A")
        return current

    def get_power_real(self) -> float:
        power = float(self.query("measure:power:ac:real?"))
        log.debug(f"Get AC current: {power}W")
        return power

    def get_voltage_rms(self) -> float:
        voltage = float(self.query("measure:voltage:ACDC?"))
        log.debug(f"Get voltage: {voltage}V")
        return voltage

    def set_voltage_AC(self, voltage: float) -> None:
        voltage = float(voltage)
        log.debug("Set AC voltage to: %.1f" % voltage)
        self.write("voltage:ac %.1f" % voltage)

    def set_voltage_slew_rate_AC(self, slew_rate_V_ms: float):
        if not 0 <= slew_rate_V_ms <= 1200:
            raise ValueError(f"Slew rate {slew_rate_V_ms}V/ms invalid!")
        cmd = f"OUTPut:SLEW:VOLTage:AC {slew_rate_V_ms}"
        self.write(cmd)

    def get_voltage_slew_rate_AC(self) -> float:
        cmd = "OUTPut:SLEW:VOLTage:AC?"
        slew_rate = float(self.query(cmd))
        return slew_rate

    def set_voltage_DC(self, voltage: float) -> float:
        log.debug("Set DC voltage to: %.1f" % voltage)
        self.write("voltage:dc %.1f" % voltage)

    def set_frequency(self, frequency: float) -> float:
        frequency = float(frequency)
        log.debug(f"Set frequency to {frequency}")
        self.write(f"frequency {frequency:.1f}")

    def get_output_state(self) -> bool:
        state = True if "ON" in self.query("OUTPut?") else False
        log.debug("Output state to: %s" % state)
        return state

    def set_output_state(self, state: bool):
        log.debug("Set Output state to: %s" % state)
        self.write(f"output {'ON' if state else 'OFF'}")
        # self.write(f"output:relay {'ON' if state else 'OFF'}")

    def get_source_mode(self) -> str:
        mode = self.query("output:mode?").strip()
        return mode

    def set_source_mode(self, mode: str):
        log.debug(f"Configure mode to {mode}")
        if mode.upper() not in AC_SOURCE_MODES:
            raise ValueError(f"Value mode {mode} not in valid modes: {AC_SOURCE_MODES}")
        self.write(f"OUTPUT:MODE {mode}")

    def set_pulse_AC_voltage(self, volt_AC: float):
        self.write("pulse:voltage:ac %.1f" % volt_AC)

    def set_pulse_DC_voltage(self, volt_DC: float):
        self.write("pulse:voltage:dc %.1f" % volt_DC)

    def set_pulse_freq(self, frequency: float):
        self.write("pulse:frequency %.1f" % frequency)

    def set_pulse_count(self, count: int):
        self.write("pulse:count %d" % count)

    def set_pulse_duty_cycle(self, duty_cycle: float):
        self.write("pulse:DCYCle %.1f" % duty_cycle)

    def set_pulse_period_ms(self, period_ms: float):
        self.write("pulse:period %.1f" % period_ms)

    def config_pulse_mode(self, volt_AC: float, volt_DC: float, frequency: float, count: int, duty_cycle: float,
                          period_ms: float):
        log.info("Configuring pulse mode")
        # set trigger OFF
        # self.set_trigger(False)

        # set mode fixed in order to change the parameter
        self.set_source_mode("FIXED")

        log.debug(f"Set AC voltage to {volt_AC}")
        self.set_pulse_AC_voltage(volt_AC)

        log.debug(f"Set DC voltage to {volt_DC}")
        self.set_pulse_DC_voltage(volt_DC)

        log.debug(f"Set frequency {frequency}")
        self.set_pulse_DC_voltage(volt_DC)

        log.debug(f"Set count {count}")
        self.set_pulse_count(count)

        log.debug(f"Set duty cycle {duty_cycle}")
        self.set_pulse_duty_cycle(duty_cycle)

        log.debug(f"Set period {period_ms}")
        self.set_pulse_period_ms(period_ms)

        self.set_source_mode("PULSE")

    def set_trigger(self, state: bool):
        log.info(f"Setting trigger state: {state}")
        self.write(f"trig {'ON' if state else 'OFF'}")

    def get_trigger(self) -> bool:
        state = False if "OFF" in self.query("trig:state?").strip() else True
        return state

    @staticmethod
    def _validate_buffer(buffer: str) -> str:
        buffer = buffer.upper()
        if buffer not in ["A", "B"]:
            raise ValueError(f"waveform buffer {buffer} not ok")
        return buffer

    def set_source_waveform(self, buffer: str, waveform: str = "SINE"):
        log.info(f"Set buffer {buffer} to waveform: {waveform}")
        buffer = AcSourceInstrument._validate_buffer(buffer)
        self.write(f"FUNCTION:SHAPE:{buffer} {waveform}")

    def get_source_waveform(self, buffer: str) -> str:
        buffer = AcSourceInstrument._validate_buffer(buffer)
        shape = self.query(f"FUNCtion:SHAPe:{buffer}?")
        log.info(f"Waveform buffer {buffer}, shape: {shape}")
        return shape


if __name__ == "__main__":
    log.info("AC source interface")

    COM_port = "ASRL51::INSTR"
    dev = AcSourceInstrument(COM_port, reset=False)
    dev.clear()
    buffer = "A"
    print(f"voltage: {dev.get_voltage_rms()}")

    dev.set_source_waveform(buffer, "SQUA")
    dev.get_source_waveform(buffer)
    time.sleep(1)
    dev.set_source_waveform(buffer, "SINE")
    dev.get_source_waveform(buffer)
    # dev.get_current_AC()
    # dev.get_power_real()
    # dev.get_voltage_rms()
    #
    # dev.get_output_state()
    # dev.set_output_state(False)
    # log.info(f"State should be true {dev.get_output_state()}")
    # dev.set_output_state(True)
    # log.info(f"State should be false {dev.get_output_state()}")
    #
    # mode = dev.get_source_mode()
    # log.info(f"Mode {mode}")
    # dev.set_source_mode("STEP")
    # mode = dev.get_source_mode()
    # log.info(f"mode should be STEP: {mode}")
    #
    # dev.config_pulse_mode(100, 0, 50, 10, 50, 2000)
    #
    # error = dev.get_system_error()
    # log.info(f"Error {error}")
    #
    # dev.set_trigger(True)
    # trig = dev.get_trigger()
    # log.info(f"Trigger {trig}")

    log.info("End!")
