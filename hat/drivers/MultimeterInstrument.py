import time
import logging
from logging.handlers import RotatingFileHandler
from hat.drivers.Instruments import Instrument
import json

# LOG_FILENAME = 'multimeter.txt'

log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)
# add ch to logger
log.addHandler(stream_handler)

# create handler for file logging
# Add the log message handler to the logger
# file_handler = RotatingFileHandler(
#     LOG_FILENAME, maxBytes=10000000, backupCount=5)

# set formatter for file_handler
# file_handler.setFormatter(formatter)
# log.addHandler(file_handler)

log.setLevel("DEBUG")


class MultimeterInstrument(Instrument):
    ROBOT_LIBRARY_SCOPE = 'SUITE'

    def __init__(self, address, config_file, write_termination="\n", execute_IDN=True, **kwargs):
        super(MultimeterInstrument, self).__init__(address, config_file=config_file,
                                                   write_termination=write_termination,
                                                   **kwargs)
        # store configurations
        self._conv_dict = {}
        self._config_file = config_file
        log.info(f"Multimenter on address {address}, config {config_file}")

        # hold channel configuration
        self._channels = []

    def load_config(self, json_filename: str) -> bool:
        with open(json_filename) as f:
            data = json.load(f)

        if not data or "channels" not in data:
            raise Exception(f"Could not load conf file {json_filename}")

        self._conv_dict = {}

        channels = data["channels"]
        for channel in channels:
            self._conv_dict[channel["name"]] = channel["number"]
        return True

    def get_voltage_dc(self):
        data = self.query(":measure:voltage:dc?")
        return float(data)

    def get_voltage_ac(self) -> float:
        cmd = ":measure:voltage:ac?"
        data = self.query(cmd)
        return float(data)

    # TODO: merge to next method to reduce repetiontion
    def get_voltage_ac_by_name(self, name: str) -> float:
        self.set_channel_by_name(name)
        self.set_function_by_name(name, "VOLTAGE:AC")
        resistance = float(self.query("READ?").strip())
        return resistance

    def get_voltage_dc_by_name(self, name: str) -> float:
        self.set_channel_by_name(name)
        self.set_function_by_name(name, "VOLTAGE")
        resistance = float(self.query("READ?").strip())
        return resistance

    def get_current_dc(self):
        data = self.query(":measure:current:dc?")
        return float(data)

    def get_current_dc_by_name(self, name: str) -> float:
        self.set_channel_by_name(name)
        self.set_function_by_name(name, "VOLTAGE")
        resistance = float(self.query("READ?").strip())
        return resistance

    def get_resistance(self):
        data = self.query(":measure:resistance?")
        return float(data)

    def set_function_by_name(self, name: str, function: str):
        channel = self.get_channel_number_by_name(name)
        self.write(f'SENSE:FUNCTION "{function}", (@{channel})')

    def set_function_range(self, function: str, channel: int, range: float):
        cmd = f'SENSE:{function}:RANGE {range}, (@{channel})'
        self.write(cmd)

    def set_function_range_by_name(self, name: str, function: str, range: float):
        channel = self.get_channel_number_by_name(name)
        self.set_function_range(function, channel, range)

    def get_resistance_by_name(self, name: str) -> float:
        self.set_channel_by_name(name)
        self.set_function_by_name(name, "RESISTANCE")
        resistance = float(self.query("READ?").strip())
        return resistance

    def get_channel_number_by_name(self, name: str) -> int:
        if not self.is_open():
            print(f"Multimenter open config: {self._config_file}")
            self.open()
        if name not in self._conv_dict:
            raise ValueError(f"Name {name} not in list of known channels: {self._conv_dict.keys()}")
        return self._conv_dict[name]

    def set_channel_by_name(self, name: str):
        channel = self.get_channel_number_by_name(name)
        self.set_channel_by_number(channel)

    def open_all_channels(self):
        self.write("ROUT:OPEN:ALL")

    def set_channel_by_number(self, channel_number: int):
        self.open_all_channels()
        self.write(f"ROUT:CLOS (@{channel_number})")

    def set_channel_by_slot_number(self, slot: int, channel: int):
        self.open_all_channels()
        self.write(f"ROUT:CLOS (@{slot:1d}{channel:02d})")

    def get_channel_list(self):
        channel_list = self.query("ROUT:CLOS?")
        return channel_list

    def make_measurement_scan(self):
        if not self._channels:
            raise Exception("Channel list not configured!")

        cmd_trace_clear = "TRAC:CLE"
        cmd_trace_lenght = f'TRACe:POINts 1000, "defbuffer1"'

        cmd_init = "INIT"
        cmd_wait = "*WAI"
        cmd_read_trace = f'TRAC:DATA? 1, {len(self._channels)}, "defbuffer1", READ'

        self.write(cmd_trace_clear)
        self.write(cmd_trace_lenght)
        self.write(cmd_init)
        self.write(cmd_wait)
        data_trace = list(map(float, self.query(cmd_read_trace).split(",")))
        print(f"{data_trace}")
        return data_trace

    def make_measurement_scan_using_conf(self):
        data = self.make_measurement_scan()
        data_dict = dict(zip(self._channels, data))
        res = {key: data_dict[val] for key, val in self._conv_dict.items()}
        return res

    def configure_measurement_scan_using_conf(self, function="Volt", range=10, speed_nplc=0.1):
        channels = [val for _, val in self._conv_dict.items()]
        self.configure_measurement_scan(channels, function, range, speed_nplc)

    def configure_measurement_scan(self, channels, function="Volt", range=10, speed_nplc=0.1):
        self._channels = channels
        # configure multimeter
        channel_list = f"(@{', '.join(map(str, channels))})"
        cmd_function_channel = f":SENS:FUNC '{function}', {channel_list}"
        cmd_voltage_range = f":SENS:VOLT:RANG {range}, {channel_list}"
        cmd_speed = f":SENS:VOLT:NPLC {speed_nplc}, {channel_list}"
        cmd_route_channel = f"ROUT:SCAN:CRE {channel_list}"

        # send commands

        self.write(cmd_function_channel)
        if function.lower() == "volt":
            self.write(cmd_voltage_range)
            self.write(cmd_speed)
        self.write(cmd_route_channel)

        # mult.query(cmd_meas)
        # data = mult.query(cmd_read)


if __name__ == "__main__":
    log.info("Multimeter interface")

    address = "USB0::0x05E6::0x6510::04472337::INSTR"
    mult = MultimeterClass(address, reset=False)

    mult.load_config(
        r"C:\Users\fcoelho\OneDrive - Schréder SA\Documents\Schreder\Automation\hatplug\mult_config.json")

    print(f'Line IN to Neutral: {mult.get_resistance_by_name("LineInToNeutral")}')
    print(f'Line OUT to Neutral: {mult.get_resistance_by_name("LineOutToNeutral")}')
    print(f'Line IN to Line OUT: {mult.get_resistance_by_name("LineInToLineOut")}')
    mult.set_channel_by_slot_number(1, 1)
    channel = mult.get_channel_list()

    volt = mult.get_voltage_dc()
    print(f"Voltage: {volt}V")

    res = mult.get_resistance()
    print(f"Resistance: {res}ohm")

    log.info("End!")
