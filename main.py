import logging
import os
import time
from genericpath import exists
from datetime import datetime
from scripts import measure_energy_monitoring as energy
from scripts.ANSI_C136_50 import measure_ansi_c136_50_t7_voltage_int
from scripts.ANSI_C136_50 import measure_ansi_c136_50_t6_temperature


log = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
stream_handler.setLevel("DEBUG")
if not exists("./log"):
    os.mkdir("./log")
logging.basicConfig(filename = './log/{:%Y-%m-%d}.log'.format(datetime.now()),
                    filemode = "w+",
                    format = '%(asctime)s %(levelname)-8s %(message)s', 
                    level = logging.ERROR)
# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')

# add formatter to ch
stream_handler.setFormatter(formatter)

# add ch to logger
log.addHandler(stream_handler)

log.setLevel("DEBUG")

while True:
    try:
        log.info("################################")
        log.info("Welcome to Hyperion Lab Software")
        log.info("################################")
        log.info("Please, select the test:")
        log.info("1) Energy")
        log.info("2) ANSI C136.50 - T#6 - Effect of Variation of Ambient Temperature")
        log.info("3) ANSI C136.50 - T#7 - Voltage Interruptions")
        test_case = input()
        log.info(f'Option selected: {test_case}')

        if test_case == '1':
            energy.test_method()
        elif test_case == '2':
            log.info("Please, specify the nominal voltage (ex. 110 for 110 VAC):")
            voltage = input()
            log.info(f'Voltage selected: {voltage}')
            measure_ansi_c136_50_t6_temperature.test_method(voltage)
        elif test_case == '3':
            measure_ansi_c136_50_t7_voltage_int.test_method()


        time.sleep(0.1)
    except KeyboardInterrupt:
        break


